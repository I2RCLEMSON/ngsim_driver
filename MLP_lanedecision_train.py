from __future__ import print_function, division
import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import transforms, utils

import torch.nn as nn
import torch.nn.functional as F

# number of subprocesses to use for data loading
num_workers = 0
# how many samples per batch to load
batch_size = 20
# percentage of training set to use as validation
valid_size = 0.2

# ================================================================
# define datasets and dataloaders
# =================================================================


class LaneDecisionDataSet(Dataset):
    def __init__(self, csv_file, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        features = self.data.iloc[idx, 2:9]
        features = np.array(features)
        features = torch.from_numpy(features).type(torch.float32)

        decision = self.data.iloc[idx, 9]
        decision = np.array([decision])
        decision = torch.from_numpy(decision).type(torch.float32)

        sample = {'features': features, 'decision': decision}

        if self.transform:
            sample = self.transform(sample)

        return sample


# choose the training and testing datasets
train_data = LaneDecisionDataSet(csv_file='./Data/MLP_traj_train_all.csv')
# test_data = LaneDecisionDataSet( csv_file='./Data/MLP_traj_test_all.csv' )

# obtain training indices that will be used for validation
num_train = len(train_data)
indices = list(range(num_train))
np.random.shuffle(indices)
split = int(np.floor(valid_size * num_train))
train_index, valid_index = indices[split:], indices[:split]

# define samplers for obtaining training and validation batches
train_sampler = SubsetRandomSampler(train_index)
valid_sampler = SubsetRandomSampler(valid_index)

# prepare data loaders
train_loader = torch.utils.data.DataLoader(train_data, batch_size = batch_size, sampler = train_sampler, num_workers = num_workers)
valid_loader = torch.utils.data.DataLoader(train_data, batch_size = batch_size, sampler = valid_sampler, num_workers = num_workers)

# ================================================================
# define NN architecture
# =================================================================


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        # number of hidden nodes in the hidden layer
        hidden_1 = 4
        hidden_2 = 2
        # linear layer (7 -> hidden_1)
        self.fc1 = nn.Linear(7, hidden_1)
        ## linear layer (hidden_1 -> hidden_2)
        # self.fc2 = nn.Linear(hidden_1, hidden_2)
        # linear layer (hidden_2 -> 1)
        self.fc3 = nn.Linear(hidden_1, 1)

    def forward(self, x):
        # add hidden layer, with sigmoid activation function
        x = torch.sigmoid(self.fc1(x))
        ## add hidden layer, with sigmoid activation function
        # x = torch.sigmoid(self.fc2(x))
        # add hidden layer, with sigmoid activation function
        x = torch.sigmoid(self.fc3(x))

        return x

# initialize the NN
model = Net()
print(model)

# specify loss function (categorical cross-entropy)
criterion = nn.MSELoss()

# specify optimizer (stochastic gradient descent and learning rate = 0.01)
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

# number of epochs to train the model
n_epochs = 2000

# initialize tracker for minimum validation loss
valid_loss_min = np.Inf    # set initial "min" to infinity
# initialize a model index for saving various versions of the model
model_i = 10

for epoch in range(n_epochs):
    # Monitor losses
    train_loss = 0
    valid_loss = 0

    ######
    # Train the model
    model.train()    # prep model for training
    for mybatch in train_loader:
        # Get the feature data and label in the mybatch
        data = mybatch['features']
        label = mybatch['decision']
        # Clear the gradients of all optimized variables
        optimizer.zero_grad()
        # forward pass: compute the predicted outputs by passing inputs to th model
        output = model(data)
        # calculate the loss
        loss = criterion(output, label)
        # backward pass: compute gradient of the loss with respect to model parameters
        loss.backward()
        # perform a single optimization step (parameter update)
        optimizer.step()
        # update running training loss
        train_loss += loss.item() * data.size(0)

    #################
    # Validate the model
    model.eval()    # prep model for evaluation
    for mybatch in valid_loader:
        # Get the feature data and label in the mybatch
        data = mybatch['features']
        label = mybatch['decision']
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)
        # calculate the loss
        loss = criterion(output, label)
        valid_loss = loss.item() * data.size(0)

    # print training/validation statistics
    # calculate average loss over an epoch
    train_loss = train_loss / len(train_loader.sampler)
    valid_loss = valid_loss / len(valid_loader.sampler)

    print('Epoch: {} \tTrain Loss: {:.6f} \tValidation Loss: {:.6f}'.format(epoch+1, train_loss, valid_loss))

    # save model if validation loss has decreased
    if valid_loss <= valid_loss_min:
        print('Validation loss decreased ({:.6f} --> {:.6f}). Saving model ...'.format(valid_loss_min, valid_loss))
        torch.save(model.state_dict(), 'model'+str(model_i)+'.pt')
        valid_loss_min = valid_loss
        # model_i += 1