include("./Supplement/core.jl");


# """
# Make aninimations
# To do: Change render function so veh2 and veh3 cannot be the same one.
# """
function make_animation()
    # Define the range of frames to analyze.
    AFTER_MARGIN = 50
    BEFORE_MARGIN = 50

    # Choose with decision to analyze
    category_name = ask_which_lane_decision();
    # test code{
    println("hello")
    # test code}

    # Loading the saved data. This requires package JLD.
    whichdataset = ask_use_which_dataset();

    if category_name == "change"

        filepath = "./Data/lanedecision_cases_"*whichdataset*".jld";
        println("filepath: ", filepath);
        (change_cases, keep_cases, abort_cases) = JLD.load(filepath, "lanedecision_cases");

        making_films = change_cases;

    elseif category_name == "keep"

        filepath = "./Data/lanedecision_cases_"*whichdataset*".jld";
        println("filepath: ", filepath);
        (change_cases, keep_cases, abort_cases) = JLD.load(filepath, "lanedecision_cases");

        making_films = keep_cases;

    elseif category_name == "abort"

        filepath = "./Data/lanedecision_cases_"*whichdataset*".jld";
        println("filepath: ", filepath);
        (change_cases, keep_cases, abort_cases) = JLD.load(filepath, "lanedecision_cases");

        making_films = abort_cases;

    elseif category_name == "merge"

        filepath = "./Data/merge_cases_"*whichdataset*".jld";
        println("filepath: ", filepath);
        merge_cases = JLD.load(filepath, "merge_cases");

        film_ranges = merge_cases;

    else
        error("Please use the corret category names: change, keep, or abort");
    end
    # test code{
    # making_films = Dict{Int, Vector{Int}}()
    # making_films[2680] = [8025]
    # test code}



    # For merging cases,
    # This is the special case,
    if category_name == "merge"

        for carid in keys(film_ranges)

            # Retrieve the film range of this car.
            film_range = film_ranges[carid];
            # We get the initial lane id of the car
            ego_laneid_ini = get_laneid_from_trajdata_i101(carid, film_range[1]);

            # Start to prepare each slide of the animation.
            filmframes = Frames(MIME("image/png"), fps=10);
            for i = film_range[1]:film_range[3]
                scene = get!(Scene(500), trajdata_my, i);
                carcolors = Dict{Int, Colorant}()
                carcolors[carid] = colorant"green";
                ego_ind = get_scene_id(scene, carid);
                # We also want to show lane id according to trajdata_my (Not the original NGSIM lane labelling!)
                ego_laneid = get_laneid_from_trajdata_i101(carid, i);

                # Get the raw lane id
                ego_start_row = tdraw_my.car2start[carid];
                ego_start_frame = tdraw_my.df[ego_start_row, :frame];
                ego_current_row = ego_start_row + i - ego_start_frame;
                ego_raw_laneid = tdraw_my.df[ego_current_row, :lane];
                # We want to highlight the trajectory after the event-trigginger frame by displaying a red font color.
                if i >= film_range[2]
                    frame_txt_color =  colorant"red";
                else
                    frame_txt_color =  colorant"white";
                end
                # We want to highlight the trajectory after the lane is changed.
                if ego_laneid != ego_laneid_ini
                    laneid_txt_color = colorant"red";
                else
                    laneid_txt_color = colorant"white";
                end
                # If you want to show additional information in the video, it is handy to use TextOverlay.
                slide = render(scene, ROADWAY_my,
                              [NeighborsOverlay(carid),
                              TextOverlay(text=[@sprintf("ego id: %d", carid)], font_size= 20, pos=VecE2(250,30)),
                              TextOverlay(text=[@sprintf("lane id: %d raw lane id:  %d", ego_laneid, ego_raw_laneid)], color = laneid_txt_color, font_size= 20, pos=VecE2(250,60)),
                              TextOverlay(text=[@sprintf("frame: %d", i)], color = frame_txt_color, font_size= 20, pos=VecE2(250,90))],
                              cam=CarFollowCamera{Int}(carid, 8.0, 2.0/9.0*pi),
                              car_colors=carcolors);
                push!(filmframes, slide)
                println("frame: ", i)
            end
            filmframes;
            println("Write to mp4")
            tic;
            println("Save to ./Animation/"*category_name*"_"*string(carid)*"_"*string(film_range[2])*".mp4");
            write("./Animation/"*category_name*"_"*string(carid)*"_"*string(film_range[2])*".mp4", filmframes);
            toc;
        end

    # For lane change, lane keep cases.
    else

        for carid in keys(making_films)

            range_margins = (BEFORE_MARGIN, AFTER_MARGIN);
            for frame in making_films[carid]
                # We make sure the car is present in the range defined by range_margin and the frame.
                (b_margin, a_margin) = proper_frame_range(range_margins, carid, frame);
                # We get the initial lane id of the car
                ego_laneid_ini = get_laneid_from_trajdata_i101(carid, frame-b_margin);
                # Start to prepare each slide of the animation.
                filmframes = Frames(MIME("image/png"), fps=10);
                for i = -b_margin:a_margin
                    scene = get!(Scene(500), trajdata_my, frame + i);
                    carcolors = Dict{Int, Colorant}()
                    carcolors[carid] = colorant"green";
                    ego_ind = get_scene_id(scene, carid);
                    # We also want to show lane id according to trajdata_my (Not the original NGSIM lane labelling!)
                    ego_laneid = get_laneid_from_trajdata_i101(carid, frame+i);
                    # We want to highlight the trajectory after the event-trigginger frame by displaying a red font color.
                    if i >= 0
                        frame_txt_color =  colorant"red";
                    else
                        frame_txt_color =  colorant"white";
                    end
                    # We want to highlight the trajectory after the lane is changed.
                    if ego_laneid != ego_laneid_ini
                        laneid_txt_color = colorant"red";
                    else
                        laneid_txt_color = colorant"white";
                    end
                    # If you want to show additional information in the video, it is handy to use TextOverlay.
                    slide = render(scene, ROADWAY_my,
                                  [NeighborsOverlay(carid),
                                  TextOverlay(text=[@sprintf("ego id: %d", carid)], font_size= 20, pos=VecE2(250,30)),
                                  TextOverlay(text=[@sprintf("lane id: %d", ego_laneid)], color = laneid_txt_color, font_size= 20, pos=VecE2(250,60)),
                                  TextOverlay(text=[@sprintf("frame: %d", frame+i)], color = frame_txt_color, font_size= 20, pos=VecE2(250,90))],
                                  cam=CarFollowCamera{Int}(carid, 8.0, 2.0/9.0*pi),
                                  car_colors=carcolors);
                    push!(filmframes, slide)
                end
                filmframes;
                write("./Animation/"*category_name*"_"*string(carid)*"_"*string(frame)*".mp4", filmframes);
            end
        end
    end
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    make_animation()
end
