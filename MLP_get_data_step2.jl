# This file is used to get label vehicle trajectories for training a multi-layer
# perception network.

# Loading libraries
include("./Supplement/core.jl");

using DataFrames
using CSV
using StatsBase
using PyPlot
using PyCall
using JLD

function retrieve_trajectories(; τ::Int64 = 30)
    #
    # τ: the frame window for retrieving vehicle trajectories
    #

    # Get to know which decision to work with and which dataset is in use.
    println("Which is the lane decision to be analyzed? 1 = change or 2 = keep");
    lanedecision_number = readline();
    println("Which is the dataset used? 1 = MyNGSIM1, 2 = MyNGSIM2, or 3 = MyNGSIM3");
    myngsim_number = readline();
    println("What is the purpose of the data? 1 = training or 2 = testing");
    for_training = readline();

    # ==========================================================================
    # Loading the required files
    # ==========================================================================
    # If the lane decision is lane_keep,
    if lanedecision_number == "2"
        println("Loading ", "MLP_keep.csv");
        data_heading = DataFrame( CSV.read( "./Data/MLP_keep.csv" ) );

    # If the lane decision is lane_change,
    else
        println("Loading ", "MLP_change_"* string(myngsim_number) * ".csv");
        data_heading = DataFrame( CSV.read( "./Data/MLP_change_" * string(myngsim_number) * ".csv" ) );
    end

    # ==========================================================================
    # Load the trajectories of the vehicle in data_heading
    # ==========================================================================
    ## Initialize data containers here
    feature_mat = Matrix{Union{Int64, Float64}}(0, 10);
    train_or_test = "";

    ## Loop through each row in the data heading
    for i = 1:size(data_heading, 1)

        # Retrieve information of the ego vehicle
        egoid = Int(data_heading[i, :id]);   # convert to Int64
        re_frame = Int(data_heading[i, :re_frame]);
        de_frame = Int(data_heading[i, :de_frame]);
        decision = Int(data_heading[i, :decision]);

        # If lane-changing, determine the turning direction, left or right.
        if lanedecision_number == 1

            # Searh from reaction frame forward to see the lateral movement of the ego.
            ego_lat = 0.0;  # Initilize the lateral position of the ego
            ego_forward = re_frame; # Initialize the ego_frame
            forward_stepsize = 3;
            while abs(ego_lat) < 0.8

                ego_forward += forward_stepsize;
                ego_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_forward,
                                                      longitudinal_direc="self",
                                                      lateral_direc = "middle");
                ego_lat = ego_traffic_info[:lat_pos];

            end
            # Determine if the lane change is to the left or not.
            if ego_lat > 0
                isleft = true;
            elseif ego_lat < 0
                isleft = false;
            else
                error("The lane-change vehicle does not change lanes.")
            end

        # If lane-keeping, choose whichever side contain a vehicle
        else

            # Check the left vehicles. Vehicle_id == 0 means there is no vehicle
            veh2_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, re_frame,
                                                   longitudinal_direc="fore",
                                                   lateral_direc = "left" );
            veh2_id = veh2_traffic_info[:vehicle_id];

            veh3_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, re_frame,
                                                   longitudinal_direc="rear",
                                                   lateral_direc = "left" );
            veh3_id = veh3_traffic_info[:vehicle_id];

            # Check the right vehicles. Vehicle_id == 0 means there is no vehicle
            veh22_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, re_frame,
                                                   longitudinal_direc="fore",
                                                   lateral_direc = "right" );
            veh22_id = veh22_traffic_info[:vehicle_id];

            veh32_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, re_frame,
                                                   longitudinal_direc="rear",
                                                   lateral_direc = "right" );
            veh32_id = veh32_traffic_info[:vehicle_id];

            # If the left side has no vehicle, then consider the right side.
            if veh2_id == 0 && veh3_id == 0
                isleft = false;
            # If the right side has no vehicle, then consider the left side.
            elseif veh22_id == 0 && veh32_id == 0
                isleft = true;
            # Otherwise, randomly select a side
            else
                isleft = rand( [true, false] );
            end

        end
        # By now "isleft" determines the turning direction.

        ## Distinguish if it is training data or testing data.
        # If the data is for training, the data in the τ-window except at "de_frame" is collected.
        if for_training == "1"
            backward_range = vcat( collect( re_frame : -1 : de_frame+1 ), collect( de_frame-1: -1 : re_frame - τ ) );
            train_or_test = "train";

        # Otherwise, the data is for testing; the data at "de_frame" is collected.
        else
            backward_range = [ de_frame ];
            train_or_test = "test";
        end

        # Loop through the frame window one by one backwards
        for ego_backward in backward_range

            # Depending on the Boolean variable "isleft", get the left or right traffic info
            if isleft   # Left side traffic
                lateral_direc = "left";
            else        # Right side traffic
                lateral_direc = "right";
            end

            ego_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_backward,
                                                  longitudinal_direc="self",
                                                  lateral_direc = "middle");
            # Speed
            v0 = ego_traffic_info[:speed];

            veh1_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_backward,
                                                   longitudinal_direc="fore",
                                                   lateral_direc = "middle" );
            # Relative distance
            d01 = veh1_traffic_info[:distance];
            # Speed
            v1 = veh1_traffic_info[:speed];
            # Relative speed
            v01 = v1 - v0;

            veh2_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_backward,
                                                   longitudinal_direc="fore",
                                                   lateral_direc = lateral_direc);
            # Relative distance
            d02 = veh2_traffic_info[:distance];
            # Speed
            v2 = veh2_traffic_info[:speed];
            # Relative speed
            v02 = v2 - v0;

            veh3_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_backward,
                                                   longitudinal_direc="rear",
                                                   lateral_direc = lateral_direc );
            # Relative distance
            d03 = veh3_traffic_info[:distance];
            # Speed
            v3 = veh3_traffic_info[:speed];
            # Relative speed
            v03 = v3 - v0;

            # Lane_id of the ego, since the lane id does not change, let it be 1
            l0 = 1;

            # Pack the features together with the label. The last item, "decision", is the label.
            feature_vec = [egoid, ego_backward, d01, d02, d03, v01, v02, v03, l0, decision];
            # Pack the feature vector into a matrix
            feature_mat = vcat(feature_mat, feature_vec');

        end


    end

    # Write the data into a CSV file
    names = [:ego_id, :ego_backward, :d01, :d02, :d03, :v01, :v02, :v03, :l0, :decision];
    feature_df = DataFrame( feature_mat, names );


    if lanedecision_number == "2"
        filename = "./Data/MLP_traj_keep_" * myngsim_number * "_" *train_or_test *".csv";
    # If the lane decision is lane_change,
    else
        filename = "./Data/MLP_traj_change_" * myngsim_number* "_" * train_or_test*".csv";
    end
    println("Write the extracted trajectory data to " * filename);
    CSV.write(filename, feature_df);


end

"Combine different trajectory data files into a single one"
function combine_trajectories()

    # Loading data
    for train_or_test in ["train", "test"]
        println("Loading ", "MLP_traj_change_1_"* train_or_test *".csv");
        traj_data = DataFrame( CSV.read( "./Data/MLP_traj_change_1_"* train_or_test *".csv" ) );

        println("Loading ", "MLP_traj_change_2_"* train_or_test *".csv");
        traj_data = DataFrame( CSV.read( "./Data/MLP_traj_change_2_"* train_or_test *".csv", traj_data, append=true ) );

        println("Loading ", "MLP_traj_change_1_"* train_or_test *".csv");
        traj_data = DataFrame( CSV.read( "./Data/MLP_traj_change_3_"* train_or_test *".csv", traj_data, append=true ) );

        println("Loading ", "MLP_traj_keep_1.csv");
        traj_data = DataFrame( CSV.read("./Data/MLP_traj_keep_1_"* train_or_test *".csv", traj_data, append=true) );

        println("Finish loading");

        # Write data
        filename = "./Data/MLP_traj_"* train_or_test *"_all.csv"
        println("Write the combined trajectory data to " * filename);
        CSV.write(filename, traj_data);

    end


end
