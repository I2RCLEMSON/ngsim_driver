# This file analyzes the testing performance of a trained MLP.

# Include a small library
include("./Supplement/analysis.jl")

"Main file for MLP_data_analysis"
function MLP_data_analysis( ;num_model::Int64=10)

    # Initialize some data containers.
    confusion_df_mlp_orig = DataFrame();
    MLP_roc_curves_orig = zeros(2, 100, num_model );

    # Iterate through the different copies of the performance data
    for i = 1:num_model

        # Load data
        println("Loading ", "MLP_test_performance"*string(i)*".csv");
        test = DataFrame( CSV.read( "./Data/MLP_test_performance"*string(i)*".csv" ) );


        p = Array{Float64}(test[:prediction]);
        y = Array{Float64}(test[:label]);

        performance = MLP_metrics(p, y);
        # Get the points on the ROC curve.
        roc = get_roc_curve( y, p );
        # Compute the area under curve of ROC.
        roc_auc = get_roc_auc( roc );
        # Add the ROC_AUC to the performance dataframe.
        performance[:roc_auc] = [ roc_auc ];

        # SAVE ANALYZED DATA
        #   save the confusion matrix data
        if i == 1
            confusion_df_mlp_orig = performance;
        else
            confusion_df_mlp_orig = vcat(confusion_df_mlp_orig, performance);
        end
        #   save the roc curve
        MLP_roc_curves_orig[ :, :, i ] = roc;

    end

    # WRITE DATA TO FILES
    #   write the ROC curves now.
    JLD.save( "./Results/MLP_roc_orig.jld", "MLP_roc_curves", MLP_roc_curves_orig );
    #   compute the mean and stds
    stat_df_mlp_orig = compute_mean_std(confusion_df_mlp_orig);
    #   write this confusion matrix data into a csv file.
    CSV.write("./Results/MLP_performance_dataset_orig.csv", confusion_df_mlp_orig);
    CSV.write("./Results/MLP_performance_stats_orig.csv", stat_df_mlp_orig);

end


function MLP_metrics(p, y; ϵ = 0.5)
    # p: the predicted decision
    # y: the true decision
    # ϵ: the decision threshold

    ( tp, fn, tn, fp ) = MLP_confusionmatrix( p, y, ϵ);

    # Compute the metrics
    sensitivity = tp / (tp + fn);
    precision = tp/ (tp + fp);
    specificity = tn / (tn + fp);

    accuracy = (tp + tn) / (tp + tn + fp + fn);
    prevalence = ( tp + fn ) / (tp + tn + fp + fn);

    f1_score = 2 * precision * sensitivity / ( precision + sensitivity );

    # Return
    metric = DataFrame( :sensitivity => sensitivity,
                        :specificity => specificity,
                        :precision => precision,
                        :accuracy => accuracy,
                        :prevalence => prevalence,
                        :f1_score => f1_score)

end

function MLP_confusionmatrix(p, y, ϵ)
    # p: the predicted decision
    # y: the true decision
    # ϵ: the decision threshold

    truechange_inds = find(d -> d >= 0.9, y);
    tp = length( find( i -> p[i] >= ϵ, truechange_inds ) );
    # false negative
    fn = length( find( i -> p[i] < ϵ, truechange_inds ) );

    truekeep_inds = find(d -> d <= 0.1, y);
    # true negative
    tn = length( find( i -> p[i] < ϵ, truekeep_inds ) );
    # false positive
    fp = length( find( i -> p[i] >= ϵ, truekeep_inds ) );

    println(" tp= ", tp)
    println(" fn=", fn)
    println(" tn=", tn)
    println(" fp=", fp)
    # Return
    (tp, fn, tn, fp)

end


# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    MLP_data_analysis()
end
