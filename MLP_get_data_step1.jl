# This file is used to prepare the data for training and testing
# a Multi-Layer-Perception for lane-change decision-making. Its aim
# is to separate the training data from the testing data.

# Loading libraries
using DataFrames
using CSV
using StatsBase
using PyPlot
using PyCall
using JLD

# Adding plotting utilities
pyplt = pyimport("matplotlib.pyplot");
pyimport("mpl_toolkits.mplot3d");

# Here we start the main function of this file
function MLP_rebalance_data(; prob_upper::Float64 = 0.90, prob_lower::Float64 = 0.1, test_ratio::Float64 = 0.2)

    # ==========================================================================
    # Loading data
    # ==========================================================================
    # Read data

    println("Loading ", "i101_0750_0805_change.csv");
    data_c1 = DataFrame( CSV.read( "./Data/i101_0750_0805_change.csv" ) );
    println("Loading ", "i101_0805_0820_change.csv");
    data_c2 = DataFrame( CSV.read( "./Data/i101_0805_0820_change.csv" ) );
    println("Loading ", "i101_0820_0835_change.csv");
    data_c3 = DataFrame( CSV.read( "./Data/i101_0820_0835_change.csv") );

    println("Loading ", "i101_0750_0805_keep.csv");
    data_k = DataFrame( CSV.read("./Data/i101_0750_0805_keep.csv") );
    println("Finish loading")


    # ==========================================================================
    # Rebalancing data
    # ==========================================================================
    ## Preprocessing the lane-change data
    # Initialize a matrix for containing lane-change data
    lc_mat_reba_size = 0;

    for (i, data_c) in enumerate( [data_c1, data_c2, data_c3] )
        # Load the required information in the lane_change data
        id_lc = Array{Int64, 1}( data_c[:id] ); # vehicle id
        re_lc = Array{Int64, 1}( data_c[:re_frame] );  # reaction moment
        de_lc = Array{Int64, 1}( data_c[:de_frame] );   # decision moment
        p_safe_lc = data_c[:p_safe]; # We bound the probability to [0.01, 0.99]
        p_safe_lc = [ minimum( [ maximum([ p_safe_lc[i], 0.01 ]), 0.99 ] ) for i = 1:length(p_safe_lc) ];
        p_safe_lc = Array{Float64, 1}(p_safe_lc);
        # Label data, this is a vector.
        y_lc = data_c[:decision];
        y_lc = Array{Int64, 1}(y_lc);
        # Pack the required information in a matrix
        lc_mat = hcat(id_lc, re_lc, de_lc, y_lc, p_safe_lc);

        ## Rebalance the lane-change data
        # Find risk-seeking items
        rsk_inds = find( x-> x < prob_upper && x > prob_lower, p_safe_lc );
        lc_mat_rsk = lc_mat[ rsk_inds, : ];
        # Match non-risk-seeking items with the risk-seeking ones through random undersampling
        nrsk_inds = setdiff( 1:size(lc_mat, 1 ), rsk_inds );
        lc_mat_nrsk = lc_mat[ nrsk_inds, : ];
        # random undersampling
        ratio = size(lc_mat_rsk, 1 ) / size(lc_mat_nrsk, 1);
        ( lc_mat_nrsk_undersam, _ ) = random_undersample(lc_mat_nrsk, target_ratio=ratio);
        # Assemble the rebalanced lane-change data.
        lc_mat_rebalance = vcat( lc_mat_rsk, lc_mat_nrsk_undersam );

        # Save the data
        names = [:id, :re_frame, :de_frame, :decision, :p_safe];
        # Pack the data into a dataframe
        lc_mat_rebalance_df = DataFrame(lc_mat_rebalance, names);
        # Write this dataframe into a csv file.
        println("Write the rebalanced lane changing data to csv files.");
        CSV.write("./Data/MLP_change_" * string(i) * ".csv", lc_mat_rebalance_df);

        # Store the size of the rebalanced data
        lc_mat_reba_size += size(lc_mat_rebalance, 1);

    end


    ## Preprocessing the lane-keep data
    # Load the required information in the lane_keep data
    id_lk = Array{Int64, 1}( data_k[:id] ); # vehicle id
    re_lk = Array{Int64, 1}( data_k[:re_frame] );  # reaction moment
    de_lk = Array{Int64, 1}( data_k[:de_frame] );   # decision moment
    p_safe_lk = data_k[:p_safe]; # We bound the probability to [0.01, 0.99]
    p_safe_lk = [ minimum( [ maximum([ p_safe_lk[i], 0.01 ]), 0.99 ] ) for i = 1:length(p_safe_lk) ];
    p_safe_lk = Array{Float64, 1}(p_safe_lk);
    # Label data, this is a vector.
    y_lk = data_k[:decision];
    y_lk = Array{Int64, 1}(y_lk);
    # Pack the required information in a matrix
    lk_mat = hcat(id_lk, re_lk, de_lk, y_lk, p_safe_lk);

    # Rebalance the lane-keep data with the lane-change data
    ratio = lc_mat_reba_size / size( lk_mat, 1 );
    ( lk_mat_rebalance, _ ) = random_undersample(lk_mat, target_ratio=ratio);
    # Pack the data into a dataframe
    names = [:id, :re_frame, :de_frame, :decision, :p_safe];
    lk_mat_rebalance_df = DataFrame(lk_mat_rebalance, names);
    # Write this dataframe into a csv file.
    println("Write the rebalanced lane keeping data to csv files.");
    CSV.write("./Data/MLP_keep.csv", lk_mat_rebalance_df);

end

"Random undersampling"
function random_undersample(dataset::Matrix{Float64}; target_ratio::Float64=0.8)

    # We randomly select a row of the dataset and exclude this row.
    n_org_rows = size(dataset, 1);
    n_target_rows = floor( Int, n_org_rows * target_ratio );
    n_deleterows = n_org_rows - n_target_rows;

    # Randomly select the distinct indices of the rows to be deleted.
    deleterow_inds = sample( 1:n_org_rows, n_deleterows, replace=false );
    # The new matrix excludes the selected rows.
    targetrow_inds = setdiff(1:n_org_rows, deleterow_inds);
    new_dataset = dataset[ targetrow_inds, : ];

    # Return
    (new_dataset, deleterow_inds)

end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    MLP_rebalance_data()
end
