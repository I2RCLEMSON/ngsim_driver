# For each vehicle, we want determine if it made a lane keep decision.
# The main things we are looking into are
#   1) whether this vehicle maintained the same lane at the four check points.
#   2) whether this vehicle significantly slowed down its speed.
# We add a function to detect lane keeping.
# --Get raw data frames from raw trajectory data.
df = tdraw_101.df;
# --Get the starting index to the first row for each vehicle.
car_start_rows = tdraw_101.car2start;
# --For each vehicle, we want determine if it made a lane change.
refined_lane_keep_cars = Dict{Int, RefinedLaneKeepInfo}()
for carid in keys(car_start_rows)
    # We retrieve the start row of this vehicle.
    start_row = car_start_rows[carid]
    # We need to know the first camera frame which contains this vehicle.
    start_camframe = df[start_row, :frame]
    # Here, we only want to investigate the camera frame from 1000 to 9000.
    if start_camframe >= 1000 && start_camframe <= 9000
        # Get the initial laneID of this vehicle.
        inital_laneID = df[start_row, :lane]
        # We want to know how many frames in total contains this vehicle.
        total_num_frames = df[start_row, :n_frames_in_dataset]
        # Get the last laneID of this vehicle recorded.
        end_row = start_row + total_num_frames -1
        final_laneID = df[end_row, :lane]
        # We only want to investigate vehicles in lane 1--5
        if inital_laneID <= 5 && final_laneID <= 5
            # We want to know how many frames in total contains this vehicle.
            total_num_frames = df[start_row, :n_frames_in_dataset]
            # We now divide the total camera frames into four shares.
            quater_num_frames = total_num_frames ÷ 4
            # We now sample the laneID in one camera frame from each of the four shares.
            checkpoint_1 = start_row + quater_num_frames ÷ 2
            checkpoint_2 = start_row + quater_num_frames + quater_num_frames ÷ 2
            checkpoint_3 = start_row + 2*quater_num_frames + quater_num_frames ÷ 2
            checkpoint_4 = start_row + 3*quater_num_frames + quater_num_frames ÷ 2
            # We now need to investigate the laneID of this vehicle at the four check points.
            laneID_1 = df[checkpoint_1, :lane]
            laneID_2 = df[checkpoint_2, :lane]
            laneID_3 = df[checkpoint_3, :lane]
            laneID_4 = df[checkpoint_4, :lane]
            # If the laneIDs at the check points are the same, it means the vehicle might have slowed down for keeping lane.
            # Also, we only want to investigate the vehicles which has been recorded for at least 40s.
            if laneID_1 == laneID_2 && laneID_2 == laneID_3 && laneID_3 == laneID_4 && total_num_frames >= 400
                # We first get the fore neighbor in the same lane. To do so, we need to build the traffic scenes at the
                # four check points.
                # Hence, we would like to know the camera frame index at various check points.
                checkpoints = collect(start_row:10:end_row)
                # Check each point.
                for checkpoint in checkpoints
                    cameraframe = df[checkpoint, :frame]
                    # We create a scene for this camera frame.
                    scene = get!(Scene(500), trajdata_101, cameraframe);
                    # We obtain the scene index for the ego vehicle.
                    ego_ind = MyNGSIM.get_scene_id(scene, carid)
                    # We get the velocity of the ego vehicle in unit [m/s].
                    ego_v = scene[ego_ind].state.v
                    # Now let us get the lead vehicle in the scene.
                    lead = get_neighbor_fore_along_lane_NGSIM(scene, carid, ROADWAY_101)
                    # Now let us get the front vehicle on the left lane in the scene.
                    leftfront = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, ROADWAY_101)
                    # Now let us get the front vehicle on the right lane in the scene.
                    rightfront = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, ROADWAY_101)
                    # The following condition make sure that the ego vehicle has the front vehicles.
                    if lead.ind != 0
                        # Obtain the lead vehicle velocity [m/s]
                        lead_v = scene[lead.ind].state.v
                        # If the ego is in the left most lane,
                        if laneID_1 == 1
                            # further if there is a right front vehicle
                            if rightfront.ind != 0
                                # Obtain the lead vehicle velocity [m/s]
                                rightfront_v = scene[rightfront.ind].state.v
                            # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                            else
                                # Obtain the lead vehicle velocity [m/s]
                                rightfront_v = 25
                            end
                            # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                            # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                            if lead_v + 2 < ego_v && rightfront_v > ego_v + 1
                                refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                                println("ego_v: " *string(ego_v) )
                                println("lead_v: " *string(lead_v) )
                                println("rightfront_v: " *string(rightfront_v) )
                                println(refined_lane_keep_cars[carid])
                                println("\n")
                                break
                            end
                        # If the ego is in the right most lane,
                    elseif laneID_1 == 5
                            # further if there is a left front vehicle
                            if leftfront.ind != 0
                                # Obtain the lead vehicle velocity [m/s]
                                leftfront_v = scene[leftfront.ind].state.v
                            # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                            else
                                # Obtain the lead vehicle velocity [m/s]
                                leftfront_v = 25
                            end
                            # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                            # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                            if lead_v + 2 < ego_v && leftfront_v > ego_v + 1
                                refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                                println("ego_v: " *string(ego_v) )
                                println("lead_v: " *string(lead_v) )
                                println("leftfront_v: " *string(leftfront_v) )
                                println(refined_lane_keep_cars[carid])
                                println("\n")
                                break
                            end
                        # Otherwise, the ego is not in the edge lanes.
                        else
                                # further if there is a left front vehicle
                                if leftfront.ind != 0
                                    # Obtain the lead vehicle velocity [m/s]
                                    leftfront_v = scene[leftfront.ind].state.v
                                # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                                else
                                    # Obtain the lead vehicle velocity [m/s]
                                    leftfront_v = 25
                                end
                                # If there is a right front vehicle,
                                if rightfront.ind != 0
                                    # Obtain the lead vehicle velocity [m/s]
                                    rightfront_v = scene[rightfront.ind].state.v
                                # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                                else
                                    # Obtain the lead vehicle velocity [m/s]
                                    rightfront_v = 25
                                end
                                # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                                # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                                if lead_v + 2< ego_v && (leftfront_v > ego_v + 1 || rightfront_v > ego_v + 1)
                                    refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                                    println("ego_v: " *string(ego_v) )
                                    println("lead_v: " *string(lead_v) )
                                    println("leftfront_v: " *string(leftfront_v) )
                                    println("rightfront_v: " *string(rightfront_v) )
                                    println(refined_lane_keep_cars[carid])
                                    println("\n")
                                    break
                                end

                        end
                    end
                end
            end
        end
    end
end
