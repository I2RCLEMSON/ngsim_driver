# We add a function to detect lane change.
# --Get raw data frames from raw trajectory data.
df = tdraw_101.df;
# --Get the starting index to the first row for each vehicle.
car_start_rows = tdraw_101.car2start;
# --For each vehicle, we want determine if it made a lane change.
lane_change_cars = Dict{Int, LaneChangeInfo}()
for carid in keys(car_start_rows)
    # We retrieve the start row of this vehicle.
    start_row = car_start_rows[carid]
    # We need to know the first camera frame which contains this vehicle.
    start_camframe = df[start_row, :frame]
    # Here, we only want to investigate the camera frame from 1000 to 9000.
    if start_camframe >= 1000 && start_camframe <= 9000
        # Get the initial laneID of this vehicle.
        inital_laneID = df[start_row, :lane]
        # We want to know how many frames in total contains this vehicle.
        total_num_frames = df[start_row, :n_frames_in_dataset]
        # Get the last laneID of this vehicle recorded.
        end_row = start_row + total_num_frames -1
        final_laneID = df[end_row, :lane]
        # We only want to investigate vehicles in lane 1--5
        if inital_laneID <= 5 && final_laneID <= 5
            # We want to know how many frames in total contains this vehicle.
            total_num_frames = df[start_row, :n_frames_in_dataset]
            # We now divide the total camera frames into four shares.
            quater_num_frames = total_num_frames ÷ 4
            # We now sample the laneID in one camera frame from each of the four shares.
            checkpoint_1 = start_row + quater_num_frames ÷ 2
            checkpoint_2 = start_row + quater_num_frames + quater_num_frames ÷ 2
            checkpoint_3 = start_row + 2*quater_num_frames + quater_num_frames ÷ 2
            checkpoint_4 = start_row + 3*quater_num_frames + quater_num_frames ÷ 2
            # We now need to investigate the laneID of this vehicle at the four check points.
            laneID_1 = df[checkpoint_1, :lane]
            laneID_2 = df[checkpoint_2, :lane]
            laneID_3 = df[checkpoint_3, :lane]
            laneID_4 = df[checkpoint_4, :lane]
            # If the laneIDs at the check points are not the same, it means the vehicle did
            # lane changes. Also, we only want to investigate the vehicles which has been recorded for at least 40s.
            if !(laneID_1 == laneID_2 && laneID_2 == laneID_3 && laneID_3 == laneID_4) && total_num_frames >= 400
                # Now we want to know when the lane change happened and to which direction.
                # --Lane change happened between check point 1 and check point 2
                if laneID_1 != laneID_2
                     if laneID_1 < laneID_2
                         # The vehicle changed to the left lane.
                         lane_change_cars[carid] = LaneChangeInfo("right", [laneID_1, laneID_2], [df[checkpoint_1, :frame], df[checkpoint_2, :frame]], [checkpoint_1, checkpoint_2]);
                     else
                         # The vehicle changed to the right lane.
                         lane_change_cars[carid] = LaneChangeInfo("left", [laneID_1, laneID_2], [df[checkpoint_1, :frame], df[checkpoint_2, :frame]], [checkpoint_1, checkpoint_2]);
                     end
                 # --Lane change happened between check point 2 and check point 3
             elseif laneID_2 != laneID_3
                      if laneID_2 < laneID_3
                          # The vehicle changed to the left lane.
                          lane_change_cars[carid] = LaneChangeInfo("right", [laneID_2, laneID_3], [df[checkpoint_2, :frame], df[checkpoint_3, :frame]], [checkpoint_2, checkpoint_3]);
                      else
                          # The vehicle changed to the right lane.
                          lane_change_cars[carid] = LaneChangeInfo("left", [laneID_2, laneID_3], [df[checkpoint_2, :frame], df[checkpoint_3, :frame]], [checkpoint_2, checkpoint_3]);
                      end
              # --Lane change happened between check point 3 and check point 4
                else
                   if laneID_3 < laneID_4
                       # The vehicle changed to the left lane.
                       lane_change_cars[carid] = LaneChangeInfo("right", [laneID_3, laneID_4], [df[checkpoint_3, :frame], df[checkpoint_4, :frame]], [checkpoint_3, checkpoint_4]);
                   else
                       # The vehicle changed to the right lane.
                       lane_change_cars[carid] = LaneChangeInfo("left", [laneID_3, laneID_4], [df[checkpoint_3, :frame], df[checkpoint_4, :frame]], [checkpoint_3, checkpoint_4]);
                   end

                end
            end
        end
    end
end

# Now we want to refine the lane change info to find where exactly the ego vehicle across the lane.
refined_lane_change_cars = Dict{Int, RefinedLaneChangeInfo}()
# For each vehicle in the lane_change_cars,
for carid in keys(lane_change_cars)
    # Get the orignial laneID.
    original_lane = lane_change_cars[carid].from_to[1]
    changeto_lane = lane_change_cars[carid].from_to[2]
    # Get the lower bound of the dataframe range.
    low_bound = lane_change_cars[carid].df_frames[1]
    # Get the upper bound of the dataframe range.
    up_bound = lane_change_cars[carid].df_frames[2]
    # Initialize the data frame where the lane change happened
    df_frame = 0
    df_frame_consrv = 0
    for dfind = low_bound:up_bound
        # Get the laneID in the dataframe indexed by dfind.
        current_lane = df[dfind, :lane]
        # If the current lane is not the original lane and it is the new lane,
        if current_lane != original_lane && current_lane == changeto_lane
            # We save this dataframe index.
            df_frame = dfind - 20
            df_frame_consrv = dfind - 1
            break
        end
    end
    # Double check if df_frame is in the frame range.
    if !( df_frame>=low_bound-20 && df_frame<=up_bound)
        error("df_frame is not correct!")
    end
    # Retrieve the correspondind camera frame.
    cameraframe = df[df_frame, :frame]
    cameraframe_consrv = df[df_frame_consrv, :frame]
    # We create a scene for this camera frame.
    scene = get!(Scene(500), trajdata_101, cameraframe);
    # We obtain the scene index for the ego vehicle.
    ego_ind = MyNGSIM.get_scene_id(scene, carid)
    # We get the velocity of the ego vehicle in unit [m/s].
    ego_v = scene[ego_ind].state.v
    # Now let us get the lead vehicle in the scene.
    lead = get_neighbor_fore_along_lane_NGSIM(scene, carid, ROADWAY_101)
    # We make sure there is a lead vehicle.
    if lead.ind != 0
        # Obtain the lead vehicle velocity [m/s]
        lead_v = scene[lead.ind].state.v
        # Now let us get the front vehicle on the left lane in the scene.
        leftfront = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, ROADWAY_101)
        # Now let us get the front vehicle on the right lane in the scene.
        rightfront = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, ROADWAY_101)
        # It is possible that the ego vehicle is in the edge lane thus does not have left
        # or right front vehicle.
        # If the ego is in the left most lane,
        if lane_change_cars[carid].from_to[1] == 1
            # further if there is a right front vehicle
            if rightfront.ind != 0
                # Obtain the lead vehicle velocity [m/s]
                rightfront_v = scene[rightfront.ind].state.v
            # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
            else
                # Obtain the lead vehicle velocity [m/s]
                rightfront_v = 25
            end

           # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
           # while the leftfront or rightfront vehicle is faster than the ego vehicle.
           if lead_v< ego_v && rightfront_v > ego_v
               refined_lane_change_cars[carid] = RefinedLaneChangeInfo(lane_change_cars[carid].changto,
                                                                       lane_change_cars[carid].from_to,
                                                                       cameraframe_consrv,
                                                                       df_frame_consrv)
               println("ego_v: " *string(ego_v) )
               println("lead_v: " *string(lead_v) )
               println("rightfront_v: " *string(rightfront_v) )
               println(refined_lane_change_cars[carid])
               println("\n")
            end
        # If the ego is in the right most lane,
    elseif lane_change_cars[carid].from_to[1] == 5
            # further if there is a left front vehicle
            if leftfront.ind != 0
                # Obtain the lead vehicle velocity [m/s]
                leftfront_v = scene[leftfront.ind].state.v
            # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
            else
                # Obtain the lead vehicle velocity [m/s]
                leftfront_v = 25
            end
            # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
            # while the leftfront or rightfront vehicle is faster than the ego vehicle.
            if lead_v< ego_v && leftfront_v > ego_v
                refined_lane_change_cars[carid] = RefinedLaneChangeInfo(lane_change_cars[carid].changto,
                                                                        lane_change_cars[carid].from_to,
                                                                        cameraframe_consrv,
                                                                        df_frame_consrv)
                println("ego_v: " *string(ego_v) )
                println("lead_v: " *string(lead_v) )
                println("leftfront_v: " *string(leftfront_v) )
                println(refined_lane_change_cars[carid])
                println("\n")
            end
    # Otherwise, the ego is not in the edge lanes.
    else
            # further if there is a left front vehicle
            if leftfront.ind != 0
                # Obtain the lead vehicle velocity [m/s]
                leftfront_v = scene[leftfront.ind].state.v
            # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
            else
                # Obtain the lead vehicle velocity [m/s]
                leftfront_v = 25
            end
            # If there is a right front vehicle,
            if rightfront.ind != 0
                # Obtain the lead vehicle velocity [m/s]
                rightfront_v = scene[rightfront.ind].state.v
            # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
            else
                # Obtain the lead vehicle velocity [m/s]
                rightfront_v = 25
            end
            # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
            # while the leftfront or rightfront vehicle is faster than the ego vehicle.
            if lead_v< ego_v && (leftfront_v > ego_v || rightfront_v > ego_v)
                refined_lane_change_cars[carid] = RefinedLaneChangeInfo(lane_change_cars[carid].changto,
                                                                        lane_change_cars[carid].from_to,
                                                                        cameraframe_consrv,
                                                                        df_frame_consrv)
                println("ego_v: " *string(ego_v) )
                println("lead_v: " *string(lead_v) )
                println("leftfront_v: " *string(leftfront_v) )
                println("rightfront_v: " *string(rightfront_v) )
                println(refined_lane_change_cars[carid])
                println("\n")
            end
        end
    end
end

# Now we want to estimate when the lane change decision-making happened (not when the vehicle crossed lanes).
# We start from the frame when the vehicle cross lanes and roll back frame by frame to find in which frame
# the relative heading of the vehicle starts to deviate from 0 radius.
carid = 2220
# Get the frame in which the vehicle cross lanes?
cross_frame = refined_lane_change_cars[carid].cam_frame
# Set the total number of frames to roll back.
num_frames = 100
# Start rolling
for frame = cross_frame:-1:(cross_frame - num_frames)
    if frame == cross_frame
        println("crossing lane")
    end
    scene = get!(Scene(500), trajdata_101, frame);
    relative_heading = scene[get_scene_id(scene, carid)].state.posF.ϕ
    println("frame: "*string(frame)*"  ϕ: "*string(relative_heading))
    if abs(relative_heading) < 0.015
        break
    end
end
