# For each vehicle, we want determine if it made a lane keep decision.
# The main things we are looking into are
#   1) whether this vehicle maintained the same lane at the four check points.
#   2) whether this vehicle significantly slowed down its speed.
# We add a function to detect lane change.
# --Get raw data frames from raw trajectory data.
df = tdraw_101.df;
# --Get the starting index to the first row for each vehicle.
car_start_rows = tdraw_101.car2start;
# --For each vehicle, we want determine if it made a lane change.
lane_keep_cars = Dict{Int, LaneKeepInfo}();
for carid in keys(car_start_rows)
    # We retrieve the start row of this vehicle.
    start_row = car_start_rows[carid]
    # We need to know the first camera frame which contains this vehicle.
    start_camframe = df[start_row, :frame]
    # Here, we only want to investigate the camera frame from 1000 to 9000.
    if start_camframe >= 1000 && start_camframe <= 9000
        # Get the initial laneID of this vehicle.
        inital_laneID = df[start_row, :lane]
        # We want to know how many frames in total contains this vehicle.
        total_num_frames = df[start_row, :n_frames_in_dataset]
        # Get the last laneID of this vehicle recorded.
        end_row = start_row + total_num_frames -1
        final_laneID = df[end_row, :lane]
        # We only want to investigate vehicles in lane 1--5
        if inital_laneID <= 5 && final_laneID <= 5
            # We want to know how many frames in total contains this vehicle.
            total_num_frames = df[start_row, :n_frames_in_dataset]
            # We now divide the total camera frames into four shares.
            quater_num_frames = total_num_frames ÷ 4
            # We now sample the laneID in one camera frame from each of the four shares.
            checkpoint_1 = start_row + quater_num_frames ÷ 2
            checkpoint_2 = start_row + quater_num_frames + quater_num_frames ÷ 2
            checkpoint_3 = start_row + 2*quater_num_frames + quater_num_frames ÷ 2
            checkpoint_4 = start_row + 3*quater_num_frames + quater_num_frames ÷ 2
            # We now need to investigate the laneID of this vehicle at the four check points.
            laneID_1 = df[checkpoint_1, :lane]
            laneID_2 = df[checkpoint_2, :lane]
            laneID_3 = df[checkpoint_3, :lane]
            laneID_4 = df[checkpoint_4, :lane]
            # If the laneIDs at the check points are the same, it means the vehicle might have slowed down for keeping lane.
            # Also, we only want to investigate the vehicles which has been recorded for at least 40s.
            if laneID_1 == laneID_2 && laneID_2 == laneID_3 && laneID_3 == laneID_4 && total_num_frames >= 400
                # We first get the fore neighbor in the same lane. To do so, we need to build the traffic scenes at the
                # four check points.
                # Hence, we would like to know the camera frame index at the four check points.
                camframe_1 = df[checkpoint_1, :frame]
                camframe_2 = df[checkpoint_2, :frame]
                camframe_3 = df[checkpoint_3, :frame]
                camframe_4 = df[checkpoint_4, :frame]
                # Now retrieve the speed of the ego vehicle at the check points.
                v_duration = 19
                ego_v_1 = mean(df[checkpoint_1:checkpoint_1+v_duration, :speed])
                ego_v_2 = mean(df[checkpoint_2:checkpoint_2+v_duration, :speed])
                ego_v_3 = mean(df[checkpoint_3:checkpoint_3+v_duration, :speed])
                ego_v_4 = mean(df[checkpoint_4:checkpoint_4+v_duration, :speed])
                # Define a threshould to represent significant speed reduction.
                v_sig_redu = 10
                # If ego_v_2 is significantly less than ego_v_1, one lane keep decision happened between check points 1 and 2.
                if ego_v_1 > ego_v_2 + v_sig_redu
                    lane_keep_cars[carid] = LaneKeepInfo([camframe_1, camframe_2], [checkpoint_1, checkpoint_2])
                    # println("id: " * string(carid))
                    # println("checking segment: 1" )
                    # println("original speed: " * string(ego_v_1))
                    # println("new speed: " * string(ego_v_2))
                # If ego_v_3 is significantly less than ego_v_2, one lane keep decision happened between check points 2 and 3.
            elseif ego_v_2 > ego_v_3 + v_sig_redu
                    lane_keep_cars[carid] = LaneKeepInfo([camframe_2, camframe_3], [checkpoint_2, checkpoint_3])
                    # println("id: " * string(carid))
                    # println("checking segment: 2" )
                    # println("original speed: " * string(ego_v_2))
                    # println("new speed: " * string(ego_v_3))
                # If ego_v_4 is significantly less than ego_v_3, one lane keep decision happened between check points 3 and 4.
            elseif ego_v_3 > ego_v_4 + v_sig_redu
                    lane_keep_cars[carid] = LaneKeepInfo([camframe_3, camframe_4], [checkpoint_3, checkpoint_4])
                    # println("id: " * string(carid))
                    # println("checking segment: 3" )
                    # println("original speed: " * string(ego_v_3))
                    # println("new speed: " * string(ego_v_4))
                end
            end
        end
    end
end

# Now we want to refine the lane keep info to find where exactly the ego vehicle started slowing down.
refined_lane_keep_cars = Dict{Int, RefinedLaneKeepInfo}()
# For each vehicle in the lane_change_cars,
for carid in keys(lane_keep_cars)
    # Get the lower bound of the dataframe range.
    low_bound = lane_keep_cars[carid].df_frames[1]
    # Get the upper bound of the dataframe range.
    up_bound = lane_keep_cars[carid].df_frames[2]
    # Initialize the data frame where the lane keep happened
    checkpoints = collect(low_bound:1:up_bound)
    # Get the lane ID of this vehicle.
    laneID_1 = df[low_bound, :lane]
    # Check each point.
    for checkpoint in checkpoints
        cameraframe = df[checkpoint, :frame]
        # We create a scene for this camera frame.
        scene = get!(Scene(500), trajdata_101, cameraframe);
        # We obtain the scene index for the ego vehicle.
        ego_ind = MyNGSIM.get_scene_id(scene, carid)
        # We get the velocity of the ego vehicle in unit [m/s].
        ego_v = scene[ego_ind].state.v
        # Now let us get the lead vehicle in the scene.
        lead = get_neighbor_fore_along_lane_NGSIM(scene, carid, ROADWAY_101)
        # Now let us get the front vehicle on the left lane in the scene.
        leftfront = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, ROADWAY_101)
        # Now let us get the front vehicle on the right lane in the scene.
        rightfront = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, ROADWAY_101)
        # The following condition make sure that the ego vehicle has the front vehicles.
        if lead.ind != 0
            # Obtain the lead vehicle velocity [m/s]
            lead_v = scene[lead.ind].state.v
            # If the ego is in the left most lane,
            if laneID_1 == 1
                # further if there is a right front vehicle
                if rightfront.ind != 0
                    # Obtain the lead vehicle velocity [m/s]
                    rightfront_v = scene[rightfront.ind].state.v
                # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                else
                    # Obtain the lead vehicle velocity [m/s]
                    rightfront_v = 25
                end
                # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                if lead_v + 2 < ego_v && rightfront_v > ego_v + 0
                    refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                    # println("ego_v: " *string(ego_v) )
                    # println("lead_v: " *string(lead_v) )
                    # println("rightfront_v: " *string(rightfront_v) )
                    # println(refined_lane_keep_cars[carid])
                    # println("\n")
                    break
                end
            # If the ego is in the right most lane,
        elseif laneID_1 == 5
                # further if there is a left front vehicle
                if leftfront.ind != 0
                    # Obtain the lead vehicle velocity [m/s]
                    leftfront_v = scene[leftfront.ind].state.v
                # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                else
                    # Obtain the lead vehicle velocity [m/s]
                    leftfront_v = 25
                end
                # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                if lead_v + 2 < ego_v && leftfront_v > ego_v + 0
                    refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                    # println("ego_v: " *string(ego_v) )
                    # println("lead_v: " *string(lead_v) )
                    # println("leftfront_v: " *string(leftfront_v) )
                    # println(refined_lane_keep_cars[carid])
                    # println("\n")
                    break
                end
            # Otherwise, the ego is not in the edge lanes.
            else
                    # further if there is a left front vehicle
                    if leftfront.ind != 0
                        # Obtain the lead vehicle velocity [m/s]
                        leftfront_v = scene[leftfront.ind].state.v
                    # otherwise, there is no left front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                    else
                        # Obtain the lead vehicle velocity [m/s]
                        leftfront_v = 25
                    end
                    # If there is a right front vehicle,
                    if rightfront.ind != 0
                        # Obtain the lead vehicle velocity [m/s]
                        rightfront_v = scene[rightfront.ind].state.v
                    # otherwise, there is no right front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
                    else
                        # Obtain the lead vehicle velocity [m/s]
                        rightfront_v = 25
                    end
                    # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                    # while the leftfront or rightfront vehicle is faster than the ego vehicle.
                    if lead_v + 2< ego_v && (leftfront_v > ego_v + 0 || rightfront_v > ego_v + 0)
                        refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                        # println("ego_v: " *string(ego_v) )
                        # println("lead_v: " *string(lead_v) )
                        # println("leftfront_v: " *string(leftfront_v) )
                        # println("rightfront_v: " *string(rightfront_v) )
                        # println(refined_lane_keep_cars[carid])
                        # println("\n")
                        break
                    end

            end
        end
    end
end

# ==============================================================================
# Now we retrieve the required data for regret decision model.
# ==============================================================================
# Create a vector to store the required data
# total_entries = length(refined_lane_keep_cars)
total_entries = 100
required_lane_keep_data = Matrix{Float64}(total_entries, 6)
# Initialize a counter
count = 0
# For each vehicle in the refined_lane_keep_cars, we do the following.
for carid in keys(refined_lane_keep_cars)
    count += 1
    if count > total_entries
        break
    end
    # We retrieve the camera frame recorded in the refined_lane_keep_cars.
    cam_frame = refined_lane_keep_cars[carid].cam_frame
    # With the camera frame index, we can rebuild the traffic scene.
    scene = get!(Scene(500), trajdata_101, cam_frame);
    # Get the index of the current vehicle in the scene.
    ego_ind = get_scene_id(scene, carid)
    # Data 1: the velocity of the ego vehicle in unit [m/s].
    ego_v = scene[ego_ind].state.v
    # We want to get the slow velocity of the lead vehicle in the current lane.
    lead = get_neighbor_fore_along_lane(scene, ego_ind, ROADWAY_101)
    # Data 2: the lead vehicle velocity [m/s]
    lead_v = scene[lead.ind].state.v
    # We want to know to which adjacent lane the driver has considered to change.
    # Hence, we first get the info of front vehicle in both the left and right lane.
    leftfront = get_neighbor_fore_along_left_lane(scene, ego_ind, ROADWAY_101)
    rightfront = get_neighbor_fore_along_right_lane(scene, ego_ind, ROADWAY_101)
    # We now get the velocity of both the left and right front vehicle.
    # Left:
    if leftfront.ind != 0
        # Obtain the vehicle velocity [m/s]
        leftfront_v = scene[leftfront.ind].state.v
    # otherwise, there is no adjacent front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
    else
        # Obtain tht vehicle velocity [m/s]
        leftfront_v = 25
    end
    # Right:
    if rightfront.ind != 0
        # Obtain the vehicle velocity [m/s]
        rightfront_v = scene[rightfront.ind].state.v
    # otherwise, there is no adjacent front vehicle, set the speed to be the speed limit 55 mph == 25 m/s
    else
        # Obtain tht vehicle velocity [m/s]
        rightfront_v = 25
    end
    # If the ego vehicle is in the left most or right most lane, then the adjacent
    # lane should be in the opposite direction.
    # To check this, we also need to know the current lane of the ego vehicle.
    # --Get the dataframe index for the lane ID retrieving.
    ego_df_frame = refined_lane_keep_cars[carid].df_frame
    # --Retrieve the current lane ID
    ego_lane_ID = df[ego_df_frame, :lane]
    println("laneID: "*string(ego_lane_ID))
    # If the current lane is the left most lane, a.k.a., laneID == 1
    if ego_lane_ID == 1
        adjacent = "right"
        # Data 3: velocity of the adjacent front vehicle
        adjacentfront_v = rightfront_v
    # If the current lane is the right most lane, a.k.a., laneID == 5
    elseif ego_lane_ID == 5
        adjacent = "left"
        # Data 3: velocity of the adjacent front vehicle
        adjacentfront_v = leftfront_v
    # If the current lane is not on the edge of the road, a.k.a, it has both left
    # and right adjacent lanes, we think the driver wanted to merge into the lane
    # with faster front vehicle.
    else
        # We know determine which adjacent lane the driver has considered.
        if leftfront_v >= rightfront_v
            adjacent = "left"
            # Data 3: velocity of the adjacent front vehicle
            adjacentfront_v = leftfront_v
        else
            adjacent = "right"
            # Data 3: velocity of the adjacent front vehicle
            adjacentfront_v = rightfront_v
        end
    end
    # If the direction is the to the left, we get the vehicle info in the left lane.
    if adjacent == "left"
        adjacentrear = get_neighbor_rear_along_left_lane(scene, ego_ind, ROADWAY_101)
    # If the direction is the to the right, we get the vehicle info in the right lane.
    else
        adjacentrear = get_neighbor_rear_along_right_lane(scene, ego_ind, ROADWAY_101)
    end
    # If there is an adjacent rear vehicle
    if adjacentrear.ind != 0
        # Data 4: velocity of the rear approaching vehicle
        # Obtain the rear vehicle velocity [m/s]
        adjacentrear_v = scene[adjacentrear.ind].state.v
        # Data 5: the distance between the ego vehicle and the rear approaching vehicle.
        adjacentrear_dist = adjacentrear.Δs
        # Data 6: the volume of the adjacent rear vehicle
        # Get the vehicle length
        vehicle_len = scene[adjacentrear.ind].def.length
        # Get the vehicle width
        vehicle_wid = scene[adjacentrear.ind].def.width
        # Depending on the classes of the vehicle, the height is different.
        # If the vehicle is a truck, the normal height is 4 meter
        if scene[adjacentrear.ind].def.class == 3
            vehicle_heig = 4
        # Otherwise, either the vehicle is a sedan or a motorcycle, we let the height
        # be 1.5 meter
        else
            vehicle_heig = 1.5
        end
        # Calculate the volume
        adjacentrear_vol = vehicle_len * vehicle_wid * vehicle_heig
    # otherwise, there is no adjacent rear vehicle, set the speed to be 0 m
    else
        # Data 4: velocity of the rear approaching vehicle
        # Obtain the rear vehicle velocity [m/s]
        adjacentrear_v = 0.1
        # Data 5: the distance between the ego vehicle and the rear approaching vehicle.
        adjacentrear_dist = adjacentrear.Δs
        # Data 6: the volume of the adjacent rear vehicle
        adjacentrear_vol=0.1
    end
    println("carid:  "*string(carid))
    println("ego_v:  "*string(ego_v))
    println("lead_v:  "*string(lead_v))
    println("adjacentfront_v:  "*string(adjacentfront_v))
    println("adjacentrear_v:  "*string(adjacentrear_v))
    println("adjacentrear_dist:  "*string(adjacentrear_dist))
    println("adjacentrear_vol:  "*string(adjacentrear_vol))
    println("\n")
    # Now we store the data as a row in the matrix. The order is
    #   [ego_v, lead_v, adjacentfront_v, adjacentrear_v, adjacentrear_dist, adjacentrear_vol]
    required_lane_keep_data[count,:] = [ego_v, lead_v, adjacentfront_v, adjacentrear_v, adjacentrear_dist, adjacentrear_vol]
end
# Write to a csv file
writecsv( "lane_keep_data.csv",  required_lane_keep_data);
