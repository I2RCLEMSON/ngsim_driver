# # Here we define a function for retrieving velocities distances from NGSIM
# function traffic_info_NGSIM(trajdata::Records.ListRecord{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64},
#                             roadway::AutomotiveDrivingModels.Roadway,
#                             carid::Int, cameraframe::Int,
#                             longitudinal_direc::String, lateral_direc::String, sample_duration::Int = 5)
#     # Define a dictionary for traffic info
#     traffic_info = Dict{Symbol, Any}()
#     # Define a speed cache and a distance chance here
#     speed_vec = Vector{Float64}(sample_duration)
#     speed_mean = 0
#     distance_vec = Vector{Float64}(sample_duration)
#     distance_min = 0
#     # This record the type and dimension of the vehicle.
#     vehicle_def = AutomotiveDrivingModels.VehicleDef()
#
#     # If the vehicle to be observed is the ego self,
#     if longitudinal_direc == "self"
#         for i = 1:sample_duration
#             scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
#                                                                                        # This time constant is the same as in the rear left/right case.
#             # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#             ego_ind = MyNGSIM.get_scene_id(scene, carid)
#             # We get the velocity of the ego vehicle in unit [m/s].
#             speed_vec[i] = scene[ego_ind].state.v
#             # The vehicle definition.
#             vehicle_def = scene[ego_ind].def
#         end
#         # Getting mean values
#         speed_mean = mean(speed_vec)
#     # If the vehicle to be observed is in front,
#     elseif longitudinal_direc == "fore"
#         # further if the vehicle to be observed is on left
#         if lateral_direc == "middle"
#             for i = 1:sample_duration
#                 scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 24 is from ACT-R driver model, means -2.4 second.
#                 neighbor = get_neighbor_fore_along_lane_NGSIM(scene, carid, roadway)
#                 # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#                 distance_vec[i] = neighbor.Δs
#                 speed_vec[i] = get_neighbor_speed(scene, neighbor)
#                 # The vehicle definition.
#                 if neighbor.ind != 0
#                     vehicle_def = scene[neighbor.ind].def
#                 end
#             end
#             # Getting mean values
#             distance_min = minimum(distance_vec)
#             speed_mean = mean(speed_vec)
#         # If the vehicle to be observed is in middle,
#         elseif lateral_direc == "left"
#             for i = 1:sample_duration
#                 scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 15 is from ACT-R driver model, means -1.5 second.
#                 neighbor = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, roadway)
#                 # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#                 distance_vec[i] = neighbor.Δs
#                 speed_vec[i] = get_neighbor_speed(scene, neighbor)
#                 # The vehicle definition.
#                 if neighbor.ind != 0
#                     vehicle_def = scene[neighbor.ind].def
#                 end
#             end
#             # Getting mean values
#             distance_min = minimum(distance_vec)
#             speed_mean = mean(speed_vec)
#         # If the vehicle to be observed is on right,
#         elseif lateral_direc == "right"
#             for i = 1:sample_duration
#                 scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 15 is from ACT-R driver model, means -1.5 second.
#                 neighbor = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, roadway)
#                 # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#                 distance_vec[i] = neighbor.Δs
#                 speed_vec[i] = get_neighbor_speed(scene, neighbor)
#                 # The vehicle definition.
#                 if neighbor.ind != 0
#                     vehicle_def = scene[neighbor.ind].def
#                 end
#             end
#             # Getting mean values
#             distance_min = minimum(distance_vec)
#             speed_mean = mean(speed_vec)
#         else
#             error("When getting front neighbor, the lateral direction is not valid.")
#         end
#     # If the vehicle to be observed is at rear,
#     elseif longitudinal_direc == "rear"
#         # further if the vehicle to be observed is on left
#         if lateral_direc == "left"
#             for i = 1:sample_duration
#                 scene = get!(Scene(500), trajdata, cameraframe - 0 -sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
#                 neighbor = get_neighbor_rear_along_left_lane_NGSIM(scene, carid, roadway)
#                 # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#                 distance_vec[i] = neighbor.Δs
#                 speed_vec[i] = get_neighbor_speed(scene, neighbor, 0.0)
#                 # The vehicle definition.
#                 if neighbor.ind != 0
#                     vehicle_def = scene[neighbor.ind].def
#                 end
#             end
#             # Getting mean values
#             distance_min = minimum(distance_vec)
#             speed_mean = mean(speed_vec)
#         # If the vehicle to be observed is in middle,
#         elseif lateral_direc == "right"
#             for i = 1:sample_duration
#                 scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
#                 neighbor = get_neighbor_rear_along_right_lane_NGSIM(scene, carid, roadway)
#                 # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
#                 distance_vec[i] = neighbor.Δs
#                 speed_vec[i] = get_neighbor_speed(scene, neighbor, 0.0)
#                 # The vehicle definition.
#                 if neighbor.ind != 0
#                     vehicle_def = scene[neighbor.ind].def
#                 end
#             end
#             # Getting mean values
#             distance_min = minimum(distance_vec)
#             speed_mean = mean(speed_vec)
#         # If the vehicle to be observed is on right,
#         else
#             error("When getting rear neighbor, the lateral direction is not valid.")
#         end # if end
#     # If the vehicle to be observed is in the blind spot region.
#     elseif longitudinal_direc == "blindspot"
#         # further if the vehicle to be observed is on left
#         if lateral_direc == "left"
#             scene = get!(Scene(500), trajdata, cameraframe);
#             # We retrieve both the fore neighbor and the rear neighbor.
#             neighbor_fore = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, roadway)
#             neighbor_rear = get_neighbor_rear_along_left_lane_NGSIM(scene, carid, roadway)
#             distance_min = min(neighbor_fore.Δs, neighbor_rear.Δs)
#         # If the vehicle to be observed is in middle,
#         elseif lateral_direc == "right"
#             scene = get!(Scene(500), trajdata, cameraframe);
#             # We retrieve both the fore neighbor and the rear neighbor.
#             neighbor_fore = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, roadway)
#             neighbor_rear = get_neighbor_rear_along_right_lane_NGSIM(scene, carid, roadway)
#             distance_min = min(neighbor_fore.Δs, neighbor_rear.Δs)
#         # If the vehicle to be observed is on right,
#         else
#             error("When checking blindspot, the lateral direction is not valid.")
#         end
#     else
#         error("The longitudinal direction is not valid.")
#     end # if end
# traffic_info[:speed] = speed_mean
# traffic_info[:distance] = distance_min
# traffic_info[:vehicle_def] = vehicle_def
# traffic_info
# end # function end
#
# # We define a function to get the speed of a neighbor in the scene.
# function get_neighbor_speed(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
#                             neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult,
#                             alternativespeed::Float64 = 25)
#     # We now compute the speed. If there is a neighbor
#     if neighbor.ind != 0
#         # Obtain the lead vehicle velocity [m/s]
#         speed = scene[neighbor.ind].state.v
#     # otherwise, set the speed to be the speed limit 55 mph == 25 m/s
#     else
#         # Obtain the lead vehicle velocity [m/s]
#         speed = alternativespeed
#     end
#     speed
# end

# Here we define a function for retrieving velocities distances from NGSIM
function traffic_info_NGSIM(trajdata::Records.ListRecord{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64},
                            roadway::AutomotiveDrivingModels.Roadway,
                            carid::Int, cameraframe::Int,
                            longitudinal_direc::String, lateral_direc::String, sample_duration::Int = 5)
    # Define a dictionary for traffic info
    traffic_info = Dict{Symbol, Any}()
    # Define a speed cache and a distance chance here
    speed_vec = Vector{Float64}(sample_duration)
    speed_mean = 0
    distance_vec = Vector{Float64}(sample_duration)
    distance_min = 0
    # This record the type and dimension of the vehicle.
    vehicle_def = AutomotiveDrivingModels.VehicleDef()

    # If the vehicle to be observed is the ego self,
    if longitudinal_direc == "self"
        for i = 1:sample_duration
            scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
                                                                                       # This time constant is the same as in the rear left/right case.
            # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
            ego_ind = MyNGSIM.get_scene_id(scene, carid)
            # We get the velocity of the ego vehicle in unit [m/s].
            speed_vec[i] = scene[ego_ind].state.v
            # The vehicle definition.
            vehicle_def = scene[ego_ind].def
        end
        # Getting mean values
        speed_mean = mean(speed_vec)
    # If the vehicle to be observed is in front,
    elseif longitudinal_direc == "fore"
        # further if the vehicle to be observed is on left
        if lateral_direc == "middle"
            for i = 1:sample_duration
                scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 24 is from ACT-R driver model, means -2.4 second.
                neighbor = get_neighbor_fore_along_lane_NGSIM(scene, carid, roadway)
                # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
                distance_vec[i] = neighbor.Δs
                speed_vec[i] = get_neighbor_speed(scene, neighbor)
                # The vehicle definition.
                if neighbor.ind != 0
                    vehicle_def = scene[neighbor.ind].def
                end
            end
            # Getting mean values
            distance_min = minimum(distance_vec)
            speed_mean = mean(speed_vec)
        # If the vehicle to be observed is in middle,
        elseif lateral_direc == "left"
            for i = 1:sample_duration
                scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 15 is from ACT-R driver model, means -1.5 second.
                neighbor = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, roadway)
                # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
                distance_vec[i] = neighbor.Δs
                speed_vec[i] = get_neighbor_speed(scene, neighbor)
                # The vehicle definition.
                if neighbor.ind != 0
                    vehicle_def = scene[neighbor.ind].def
                end
            end
            # Getting mean values
            distance_min = minimum(distance_vec)
            speed_mean = mean(speed_vec)
        # If the vehicle to be observed is on right,
        elseif lateral_direc == "right"
            for i = 1:sample_duration
                scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 15 is from ACT-R driver model, means -1.5 second.
                neighbor = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, roadway)
                # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
                distance_vec[i] = neighbor.Δs
                speed_vec[i] = get_neighbor_speed(scene, neighbor)
                # The vehicle definition.
                if neighbor.ind != 0
                    vehicle_def = scene[neighbor.ind].def
                end
            end
            # Getting mean values
            distance_min = minimum(distance_vec)
            speed_mean = mean(speed_vec)
        else
            error("When getting front neighbor, the lateral direction is not valid.")
        end
    # If the vehicle to be observed is at rear,
    elseif longitudinal_direc == "rear"
        # further if the vehicle to be observed is on left
        if lateral_direc == "left"
            for i = 1:sample_duration
                scene = get!(Scene(500), trajdata, cameraframe - 0 -sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
                neighbor = get_neighbor_rear_along_left_lane_NGSIM(scene, carid, roadway)
                # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
                distance_vec[i] = neighbor.Δs
                speed_vec[i] = get_neighbor_speed(scene, neighbor, 0.0)
                # The vehicle definition.
                if neighbor.ind != 0
                    vehicle_def = scene[neighbor.ind].def
                end
            end
            # Getting mean values
            distance_min = minimum(distance_vec)
            speed_mean = mean(speed_vec)
        # If the vehicle to be observed is in middle,
        elseif lateral_direc == "right"
            for i = 1:sample_duration
                scene = get!(Scene(500), trajdata, cameraframe - 0 - sample_duration + i); # The 8 is from ACT-R driver model, means -0.8 second.
                neighbor = get_neighbor_rear_along_right_lane_NGSIM(scene, carid, roadway)
                # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
                distance_vec[i] = neighbor.Δs
                speed_vec[i] = get_neighbor_speed(scene, neighbor, 0.0)
                # The vehicle definition.
                if neighbor.ind != 0
                    vehicle_def = scene[neighbor.ind].def
                end
            end
            # Getting mean values
            distance_min = minimum(distance_vec)
            speed_mean = mean(speed_vec)
        # If the vehicle to be observed is on right,
        else
            error("When getting rear neighbor, the lateral direction is not valid.")
        end # if end
    # If the vehicle to be observed is in the blind spot region.
    elseif longitudinal_direc == "blindspot"
        # further if the vehicle to be observed is on left
        if lateral_direc == "left"
            scene = get!(Scene(500), trajdata, cameraframe);
            # We retrieve both the fore neighbor and the rear neighbor.
            neighbor_fore = get_neighbor_fore_along_left_lane_NGSIM(scene, carid, roadway)
            neighbor_rear = get_neighbor_rear_along_left_lane_NGSIM(scene, carid, roadway)
            distance_min = min(neighbor_fore.Δs, neighbor_rear.Δs)
        # If the vehicle to be observed is in middle,
        elseif lateral_direc == "right"
            scene = get!(Scene(500), trajdata, cameraframe);
            # We retrieve both the fore neighbor and the rear neighbor.
            neighbor_fore = get_neighbor_fore_along_right_lane_NGSIM(scene, carid, roadway)
            neighbor_rear = get_neighbor_rear_along_right_lane_NGSIM(scene, carid, roadway)
            distance_min = min(neighbor_fore.Δs, neighbor_rear.Δs)
        # If the vehicle to be observed is on right,
        else
            error("When checking blindspot, the lateral direction is not valid.")
        end
    else
        error("The longitudinal direction is not valid.")
    end # if end
traffic_info[:speed] = speed_mean
traffic_info[:distance] = distance_min
traffic_info[:vehicle_def] = vehicle_def
traffic_info
end # function end

# We define a function to get the speed of a neighbor in the scene.
function get_neighbor_speed(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                            neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult,
                            alternative_speed::Float64 = 25.0)
    # We now compute the speed. If there is a neighbor
    if neighbor.ind != 0
        # Obtain the lead vehicle velocity [m/s]
        speed = scene[neighbor.ind].state.v
    # otherwise, set the speed to be the speed limit 55 mph == 25 m/s
    else
        # Obtain the lead vehicle velocity [m/s]
        speed = alternative_speed
    end
    speed
end


# For each vehicle, we want determine if it made a lane keep decision.
# The main things we are looking into are
#   1) whether this vehicle maintained the same lane at the four check points.
#   2) whether this vehicle significantly slowed down its speed.
# We add a function to detect lane keep decision.
function lane_keep_scenarios()
    # --Get raw data frames from raw trajectory data.
    df = tdraw_101.df;
    # --Get the starting index to the first row for each vehicle.
    car_start_rows = tdraw_101.car2start;
    # --For each vehicle, we want determine if it made a lane change.
    lane_keep_cars = Dict{Int, LaneKeepInfo}();
    for carid in keys(car_start_rows)
        # We retrieve the start row of this vehicle.
        start_row = car_start_rows[carid]
        # We want to know how many frames in total contains this vehicle.
        total_num_frames = df[start_row, :n_frames_in_dataset]
        end_row = start_row + total_num_frames -1
        # We need to know the first camera frame which contains this vehicle.
        start_camframe = df[start_row, :frame]
        end_camframe = start_camframe + total_num_frames -1
        # Here, we only want to investigate the camera frame from 1000 to 9000.
        if start_camframe >= 1000 && end_camframe <= 9000
            # Get the initial laneID of this vehicle.
            inital_laneID = df[start_row, :lane]
            # We want to know how many frames in total contains this vehicle.
            final_laneID = df[end_row, :lane]
            # We only want to investigate vehicles in lane 1--5
            if inital_laneID <= 5 && final_laneID <= 5
                # We now divide the total camera frames into four shares.
                quater_num_frames = total_num_frames ÷ 4
                # We now sample the laneID in one camera frame from each of the four shares.
                checkpoint_1 = start_row + quater_num_frames ÷ 2
                checkpoint_2 = start_row + quater_num_frames + quater_num_frames ÷ 2
                checkpoint_3 = start_row + 2*quater_num_frames + quater_num_frames ÷ 2
                checkpoint_4 = start_row + 3*quater_num_frames + quater_num_frames ÷ 2
                # We now need to investigate the laneID of this vehicle at the four check points.
                laneID_1 = df[checkpoint_1, :lane]
                laneID_2 = df[checkpoint_2, :lane]
                laneID_3 = df[checkpoint_3, :lane]
                laneID_4 = df[checkpoint_4, :lane]
                # If the laneIDs at the check points are the same, it means the vehicle might have slowed down for keeping lane.
                # Also, we only want to investigate the vehicles which has been recorded for at least 40s.
                if laneID_1 == laneID_2 && laneID_2 == laneID_3 && laneID_3 == laneID_4 && total_num_frames >= 400
                    # We first get the fore neighbor in the same lane. To do so, we need to build the traffic scenes at the
                    # four check points.
                    # Hence, we would like to know the camera frame index at the four check points.
                    camframe_1 = df[checkpoint_1, :frame]
                    camframe_2 = df[checkpoint_2, :frame]
                    camframe_3 = df[checkpoint_3, :frame]
                    camframe_4 = df[checkpoint_4, :frame]
                    # Now retrieve the speed of the ego vehicle at the check points.
                    v_duration = 19
                    ego_v_1 = mean(df[checkpoint_1:checkpoint_1+v_duration, :speed])
                    ego_v_2 = mean(df[checkpoint_2:checkpoint_2+v_duration, :speed])
                    ego_v_3 = mean(df[checkpoint_3:checkpoint_3+v_duration, :speed])
                    ego_v_4 = mean(df[checkpoint_4:checkpoint_4+v_duration, :speed])
                    # Define a threshould to represent significant speed reduction.
                    v_sig_redu = 1
                    # If ego_v_2 is significantly less than ego_v_1, one lane keep decision happened between check points 1 and 2.
                    if ego_v_1 > ego_v_2 + v_sig_redu
                        lane_keep_cars[carid] = LaneKeepInfo([camframe_1, camframe_2], [checkpoint_1, checkpoint_2])
                        # println("id: " * string(carid))
                        # println("checking segment: 1" )
                        # println("original speed: " * string(ego_v_1))
                        # println("new speed: " * string(ego_v_2))
                    # If ego_v_3 is significantly less than ego_v_2, one lane keep decision happened between check points 2 and 3.
                elseif ego_v_2 > ego_v_3 + v_sig_redu
                        lane_keep_cars[carid] = LaneKeepInfo([camframe_2, camframe_3], [checkpoint_2, checkpoint_3])
                        # println("id: " * string(carid))
                        # println("checking segment: 2" )
                        # println("original speed: " * string(ego_v_2))
                        # println("new speed: " * string(ego_v_3))
                    # If ego_v_4 is significantly less than ego_v_3, one lane keep decision happened between check points 3 and 4.
                elseif ego_v_3 > ego_v_4 + v_sig_redu
                        lane_keep_cars[carid] = LaneKeepInfo([camframe_3, camframe_4], [checkpoint_3, checkpoint_4])
                        # println("id: " * string(carid))
                        # println("checking segment: 3" )
                        # println("original speed: " * string(ego_v_3))
                        # println("new speed: " * string(ego_v_4))
                    end
                end
            end
        end
    end
    # =============================================================================================
    # Now we want to refine the lane keep info to find when exactly the driver decide to keep lane.
    # =============================================================================================
    refined_lane_keep_cars = Dict{Int, RefinedLaneKeepInfo}()
    # For each vehicle in the lane_change_cars,
    for carid in keys(lane_keep_cars)
        # Get the lower bound of the dataframe range.
        low_bound = lane_keep_cars[carid].df_frames[1]
        # Get the upper bound of the dataframe range.
        up_bound = lane_keep_cars[carid].df_frames[2]
        # Initialize the data frame where the lane keep happened
        checkpoints = collect(low_bound + 30:10:up_bound)
        # Get the lane ID of this vehicle.
        laneID_1 = df[low_bound, :lane]
        # Check each point.
        for checkpoint in checkpoints
            cameraframe = df[checkpoint, :frame]
            # We create scenes for retrieving info of the ego vehicle, the lead vehicle,
            # the front adjacent vehicle, the rear adjacent vehicle respectively.
            # In ACT-R model, these info are get at different time: check front -2.4s, check front adjacent -1.5s,
            # check rear adjacent -0.8s, and check blind spot -0.0s
            # Scene for retrieving info of the ego and the lead.
            # We get the velocity of the ego vehicle in unit [m/s].
            ego = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "self", "middle")
            ego_v = ego[:speed]
            # Now let us get the lead vehicle in the scene.
            lead = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "middle")
            # Now let us get the front vehicle on the left lane in the scene.
            leftfore = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "left")
            # Now let us get the front vehicle on the right lane in the scene.
            rightfore = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "right")
            # Now let us get the rear vehicle on the left lane in the scene.
            leftrear = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "rear", "left")
            # Now let us get the rear vehicle on the right lane in the scene.
            rightrear =traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "rear", "right")
            # Let get the rear vehicle in checking blind spot region.
            leftblindspot = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "blindspot", "left")
            rightblindspot = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "blindspot", "right")
            # The following condition make sure that the ego vehicle has the front vehicles, and there is
            # no vehicles beside it.
            if lead[:distance] < 50 && leftfore[:distance] > 0 && rightfore[:distance] > 0 && leftrear[:distance] > 0 && rightrear[:distance] > 0 && leftblindspot[:distance] > 3 && rightblindspot[:distance] > 3
                # Obtain the lead vehicle velocity [m/s]
                lead_v = lead[:speed]
                # If the ego is in the left most lane,
                if laneID_1 == 1
                    # Obtain the rightfore vehicle velocity [m/s]
                    rightfore_v = rightfore[:speed]
                    # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                    # while the rightfore vehicle is faster than the ego vehicle.
                    if lead_v + 1 < ego_v && rightfore_v > ego_v + 0
                        refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                        break
                    end
                # If the ego is in the right most lane,
            elseif laneID_1 == 5
                    # Obtain the leftfore vehicle velocity [m/s]
                    leftfore_v = leftfore[:speed]
                    # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                    # while the leftfore vehicle is faster than the ego vehicle.
                    if lead_v + 1 < ego_v && leftfore_v > ego_v + 0
                        refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                        break
                    end
                # Otherwise, the ego is not in the edge lanes.
                else
                    # Obtain the leftfore vehicle velocity [m/s]
                    leftfore_v = leftfore[:speed]
                    # Obtain the rightfore vehicle velocity [m/s]
                    rightfore_v = rightfore[:speed]
                    # Now we want to find the situation that the lead vehicle is slower than the ego vehicle,
                    # while the leftfore or rightfore vehicle is faster than the ego vehicle.
                    if lead_v + 1 < ego_v && (leftfore_v > ego_v + 0 || rightfore_v > ego_v + 0)
                        refined_lane_keep_cars[carid] = RefinedLaneKeepInfo(cameraframe, checkpoint)
                        break
                    end

                end
            end
        end
    end
    refined_lane_keep_cars
end # end of the function



# ==============================================================================
# Now we retrieve the required data for regret decision model.
# ==============================================================================
function lane_keep_data_extract()
    # Get the raw data frames
    df = tdraw_101.df;
    refined_lane_keep_cars = lane_keep_scenarios()
    # Create a vector to store the required data
    total_entries = length(refined_lane_keep_cars)
    # total_entries = 200
    required_lane_keep_data = Matrix{Float64}(total_entries, 8)
    # Initialize a counter
    count = 0
    # For each vehicle in the refined_lane_keep_cars, we do the following.
    for carid in keys(refined_lane_keep_cars)
        count += 1
        println(count)
        if count > total_entries
            break
        end
        # We retrieve the camera frame recorded in the refined_lane_keep_cars.
        cameraframe = refined_lane_keep_cars[carid].cam_frame
        # Data 1: the velocity of the ego vehicle in unit [m/s].
        ego = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "self", "middle")
        ego_v = ego[:speed]
        # We want to get the slow velocity of the lead vehicle in the current lane.
        lead = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "middle")
        # Data 2: the lead vehicle velocity [m/s]
        lead_v = lead[:speed]
        # Data 2.5: the distance between the ego vehicle and the lead vehicle.
        lead_dist = lead[:distance]
        # We want to know to which adjacent lane the driver has considered to change.
        # Hence, we first get the info of front vehicle in both the left and right lane.
        # We now get the velocity of both the left and right front vehicle.
        leftfore = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "left")
        rightfore = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "fore", "right")
        # If the ego vehicle is in the left most or right most lane, then the adjacent
        # lane should be in the opposite direction.
        # To check this, we also need to know the current lane of the ego vehicle.
        # --Get the dataframe index for the lane ID retrieving.
        ego_df_frame = refined_lane_keep_cars[carid].df_frame
        # --Retrieve the current lane ID
        ego_lane_ID = df[ego_df_frame, :lane]
        println("laneID: "*string(ego_lane_ID))
        # If the current lane is the left most lane, a.k.a., laneID == 1
        if ego_lane_ID == 1
            adjacent = "right"
            # Data 3: velocity of the adjacent front vehicle
            adjacentfront_v = rightfore[:speed]
            adjacentfront_dist = rightfore[:distance]
        # If the current lane is the right most lane, a.k.a., laneID == 5
        elseif ego_lane_ID == 5
            adjacent = "left"
            # Data 3: velocity of the adjacent front vehicle
            adjacentfront_v = leftfore[:speed]
            adjacentfront_dist = leftfore[:distance]
        # If the current lane is not on the edge of the road, a.k.a, it has both left
        # and right adjacent lanes, we think the driver wanted to merge into the lane
        # with faster front vehicle.
        else
            # We know determine which adjacent lane the driver has considered.
            if leftfore[:speed] >= rightfore[:speed]
                adjacent = "left"
                # Data 3: velocity of the adjacent front vehicle
                adjacentfront_v = leftfore[:speed]
                adjacentfront_dist = leftfore[:distance]
            else
                adjacent = "right"
                # Data 3: velocity of the adjacent front vehicle
                adjacentfront_v = rightfore[:speed]
                adjacentfront_dist = rightfore[:distance]
            end
        end
        # We get the vehicle info in the adjacent lane.
        adjacentrear = traffic_info_NGSIM(trajdata_101, ROADWAY_101, carid, cameraframe, "rear", adjacent)
        # If there is an adjacent rear vehicle
        if adjacentrear[:distance] <  50
            # Data 4: velocity of the rear approaching vehicle
            # Obtain the rear vehicle velocity [m/s]
            adjacentrear_v = adjacentrear[:speed]
            # Data 5: the distance between the ego vehicle and the rear approaching vehicle.
            adjacentrear_dist = adjacentrear[:distance]
            # Data 6: the volume of the adjacent rear vehicle
            # Get the vehicle length
            vehicle_len = adjacentrear[:vehicle_def].length
            # Get the vehicle width
            vehicle_wid = adjacentrear[:vehicle_def].width
            # Depending on the classes of the vehicle, the height is different.
            # If the vehicle is a truck, the normal height is 4 meter
            if adjacentrear[:vehicle_def].class == 3
                vehicle_heig = 4
            # Otherwise, either the vehicle is a sedan or a motorcycle, we let the height
            # be 1.5 meter
            else
                vehicle_heig = 1.5
            end
            # Calculate the volume
            adjacentrear_vol = vehicle_len * vehicle_wid * vehicle_heig
        # otherwise, there is no adjacent rear vehicle, set the speed to be 0 m
        else
            # Data 4: velocity of the rear approaching vehicle
            # Obtain the rear vehicle velocity [m/s]
            adjacentrear_v = 0.1
            # Data 5: the distance between the ego vehicle and the rear approaching vehicle.
            adjacentrear_dist = adjacentrear[:distance]
            # Data 6: the volume of the adjacent rear vehicle
            adjacentrear_vol=0.1
        end
        println("carid:  "*string(carid))
        println("ego_v:  "*string(ego_v))
        println("lead_v:  "*string(lead_v))
        println("lead_dist:  "*string(lead_dist))
        println("adjacentfront_v:  "*string(adjacentfront_v))
        println("adjacentfront_dist:  "*string(adjacentfront_dist))
        println("adjacentrear_v:  "*string(adjacentrear_v))
        println("adjacentrear_dist:  "*string(adjacentrear_dist))
        println("adjacentrear_vol:  "*string(adjacentrear_vol))
        println("\n")
        # Now we store the data as a row in the matrix. The order is
        #   [ego_v, lead_v, adjacentfront_v, adjacentrear_v, adjacentrear_dist, adjacentrear_vol]
        required_lane_keep_data[count,:] = [ego_v, lead_v, lead_dist, adjacentfront_v, adjacentfront_dist, adjacentrear_v, adjacentrear_dist, adjacentrear_vol]
    end
    # Write to a csv file
    writecsv( "lane_keep_data.csv",  required_lane_keep_data);
    refined_lane_keep_cars
end # function end
