# making_films = [ [1113, 2933],
#                  [2111, 5979],
#                  [2383, 7078],
#                  [2134, 6025],
#                  [1953, 5708],
#                  [2217, 6791],
#                  [1570, 4606],
#                  [1566, 4585],
#                  [1469, 4169],
#                  [1981, 6095],
#                  [1047, 2622],
#                  [1881, 5518],
#                  [779, 2082],
#                  [1280, 3604],
#                  [1338, 3799],
#                  [2749, 8153],
#                  [2688, 8051],
                 #[1556, 4614],
making_films = [ [2711, 8072],
                 [1720, 4821],
                 [2146, 6458],
                 [2730, 8041],
                 [1166, 3275],
                 [2836, 8254],
                 [1479, 4245],
                 [2183, 6674],
                 [822, 2217],
                 [1668, 4792],
                 [2393, 7101],
                 [2469, 7483],
                 [2175, 6251],
                 [1795, 5353],
                 [1844, 5439],
                 [1945, 5667]]
                  # [2019, 5867],
                  # [2748, 8072],
                  # [1533, 4376],
                  # [1082, 2769],
                  # [2354, 7047],
                  # [2637, 7897],
                  # [1797, 5434],
                  # [2419, 7125],
                  # [1955, 5707],
                  # [2337, 6978],
                  # [1581, 4617],
                  # [1616, 4720],
                  # [1835, 5433],
                  # [2625, 7909],
                  # [1587, 4539]


for j = 1:length(making_films)
    carid = making_films[j][1]
    frame=making_films[j][2]-50
    filmframes = Frames(MIME("image/png"), fps=10);
    for i = 0:100
        scene = get!(Scene(500), trajdata_101, frame + i);
        carcolors = Dict{Int, Colorant}()
        carcolors[carid] = colorant"green";
        ego_ind = get_scene_id(scene, carid)
        slide = render(scene, ROADWAY_101, [NeighborsOverlay(carid, frame+i)],cam=CarFollowCamera{Int}(carid, 5.0), car_colors=carcolors);
        push!(filmframes, slide)
    end
    filmframes;
    write("lane_keep_"*string(carid)*".gif", filmframes);
end
