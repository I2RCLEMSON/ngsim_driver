# We add a function to detect lane change.
# --Get raw data frames from raw trajectory data.
df = tdraw_101.df;
# --Get the starting index to the first row for each vehicle.
car_start_rows = tdraw_101.car2start;
# --For each vehicle, we want determine if it made a lane change.
lane_change_cars = Dict{Int, LaneChangeInfo}()
for carid in keys(car_start_rows)
    # We retrieve the start row of this vehicle.
    start_row = car_start_rows[carid]
    # We need to know the first camera frame which contains this vehicle.
    start_camframe = df[start_row, :frame]
    # Here, we only want to investigate the camera frame from 1000 to 9000.
    if start_camframe >= 1000 && start_camframe <= 9000
        # Get the initial laneID of this vehicle.
        inital_laneID = df[start_row, :lane]
        # We want to know how many frames in total contains this vehicle.
        total_num_frames = df[start_row, :n_frames_in_dataset]
        # Get the last laneID of this vehicle recorded.
        end_row = start_row + total_num_frames -1
        final_laneID = df[end_row, :lane]
        # We only want to investigate vehicles in lane 1--5
        if inital_laneID <= 5 && final_laneID <= 5
            # We want to know how many frames in total contains this vehicle.
            total_num_frames = df[start_row, :n_frames_in_dataset]
            # We now divide the total camera frames into four shares.
            quater_num_frames = total_num_frames ÷ 4
            # We now sample the laneID in one camera frame from each of the four shares.
            checkpoint_1 = start_row + quater_num_frames ÷ 2
            checkpoint_2 = start_row + quater_num_frames + quater_num_frames ÷ 2
            checkpoint_3 = start_row + 2*quater_num_frames + quater_num_frames ÷ 2
            checkpoint_4 = start_row + 3*quater_num_frames + quater_num_frames ÷ 2
            # We now need to investigate the laneID of this vehicle at the four check points.
            laneID_1 = df[checkpoint_1, :lane]
            laneID_2 = df[checkpoint_2, :lane]
            laneID_3 = df[checkpoint_3, :lane]
            laneID_4 = df[checkpoint_4, :lane]
            # If the laneIDs at the check points are not the same, it means the vehicle did
            # lane changes. Also, we only want to investigate the vehicles which has been recorded for at least 40s.
            if !(laneID_1 == laneID_2 && laneID_2 == laneID_3 && laneID_3 == laneID_4) && total_num_frames >= 400
                # Now we want to know when the lane change happened and to which direction.
                # --Lane change happened between check point 1 and check point 2
                if laneID_1 != laneID_2
                     if laneID_1 < laneID_2
                         # The vehicle changed to the left lane.
                         lane_change_cars[carid] = LaneChangeInfo("right", [laneID_1, laneID_2], [df[checkpoint_1, :frame], df[checkpoint_2, :frame]], [checkpoint_1, checkpoint_2]);
                     else
                         # The vehicle changed to the right lane.
                         lane_change_cars[carid] = LaneChangeInfo("left", [laneID_1, laneID_2], [df[checkpoint_1, :frame], df[checkpoint_2, :frame]], [checkpoint_1, checkpoint_2]);
                     end
                 # --Lane change happened between check point 2 and check point 3
             elseif laneID_2 != laneID_3
                      if laneID_2 < laneID_3
                          # The vehicle changed to the left lane.
                          lane_change_cars[carid] = LaneChangeInfo("right", [laneID_2, laneID_3], [df[checkpoint_2, :frame], df[checkpoint_3, :frame]], [checkpoint_2, checkpoint_3]);
                      else
                          # The vehicle changed to the right lane.
                          lane_change_cars[carid] = LaneChangeInfo("left", [laneID_2, laneID_3], [df[checkpoint_2, :frame], df[checkpoint_3, :frame]], [checkpoint_2, checkpoint_3]);
                      end
              # --Lane change happened between check point 3 and check point 4
                else
                   if laneID_3 < laneID_4
                       # The vehicle changed to the left lane.
                       lane_change_cars[carid] = LaneChangeInfo("right", [laneID_3, laneID_4], [df[checkpoint_3, :frame], df[checkpoint_4, :frame]], [checkpoint_3, checkpoint_4]);
                   else
                       # The vehicle changed to the right lane.
                       lane_change_cars[carid] = LaneChangeInfo("left", [laneID_3, laneID_4], [df[checkpoint_3, :frame], df[checkpoint_4, :frame]], [checkpoint_3, checkpoint_4]);
                   end

                end
            end
        end
    end
end

# Now we want to refine the lane change info to find where exactly the ego vehicle across the lane.
refined_lane_change_cars = Dict{Int, RefinedLaneChangeInfo}()
# For each vehicle in the lane_change_cars,
for carid in keys(lane_change_cars)
    # Get the orignial laneID.
    original_lane = lane_change_cars[carid].from_to[1]
    changeto_lane = lane_change_cars[carid].from_to[2]
    # Get the lower bound of the dataframe range.
    low_bound = lane_change_cars[carid].df_frames[1]
    # Get the upper bound of the dataframe range.
    up_bound = lane_change_cars[carid].df_frames[2]
    # Initialize the data frame where the lane change happened
    df_frame = 0
    for dfind = low_bound:up_bound
        # Get the laneID in the dataframe indexed by dfind.
        current_lane = df[dfind, :lane]
        # If the current lane is not the original lane and it is the new lane,
        if current_lane != original_lane && current_lane == changeto_lane
            # We save this dataframe index.
            df_frame = dfind
            break
        end
    end
    # Double check if df_frame is in the frame range.
    if !( df_frame>=low_bound && df_frame<=up_bound)
        error("df_frame is not correct!")
    end
    # Retrieve the correspondind camera frame.
    cam_frame = df[df_frame, :frame]
    refined_lane_change_cars[carid] = RefinedLaneChangeInfo(lane_change_cars[carid].changto,
                                                            lane_change_cars[carid].from_to,
                                                            cam_frame,
                                                            df_frame)
end
