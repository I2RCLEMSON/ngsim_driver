include("./Supplement/core.jl");

# """
# Get the number of lane changes in lane 1--5 due to slow lead vehicle
# """
function lanedecisions_dueto_slowlead()
    # We first ask the use to specify which dataset is used.
    myngsim = ask_use_which_dataset()

    # From here we start the real meat of this function.
    # Start a timer
    tic();
    # Record the following numbers
    n_searched = 0
    n_triggers = 0
    n_lanechanges = 0
    n_lanekeeps = 0
    n_notchange = 0
    n_slow_lead = 0
    n_abortedchanges = 0
    n_laneids_notempty = 0
    # Assign constant parameters.
    SAMPLE_DURATION = 1
    EVENT_SPEPARATION = 50
    LANEDECISION_DURATION = 50
    LOOK_AHEAD = 20
    # Initialize the containers for storing the lane-changing and lane-keeping and aborted-changing cases.
    lanechange_cases = Dict{Int, Vector{Int}}();    # Dict: egoid ==> [triggerframe1, triggerframe2, ...]
    lanekeep_cases = Dict{Int, Vector{Int}}();
    abortchange_cases = Dict{Int, Vector{Int}}();
    # Pack these variables up.
    lanedecision_cases = (lanechange_cases, lanekeep_cases, abortchange_cases);

    #For a vehicle in all the vehicles:
    for egoid in keys(tdraw_my.car2start)  # tdraw_my is loaded by using MyNGSIM
        # Define info_after_trigger to contain the related information after an event-triggering
        # InfoAfterTrigger is defined in this file
        info_after_trigger = InfoAfterTrigger(egoid = egoid, duration = LANEDECISION_DURATION);
        # Get the starting row of the ego vehicle in tdraw_my
        ego_start_row = tdraw_my.car2start[egoid];
        # Get the total number of frames of the ego vehicle in tdraw_my
        total_n_frames = tdraw_my.df[ego_start_row, :n_frames_in_dataset];
        # Compute the ending row of the ego vehicle in tdraw_my
        ego_end_row = ego_start_row + total_n_frames - 1; # df is organized by clustering same vehicle together.
        # Get the lanes of the ego
        ego_laneids = tdraw_my.df[ego_start_row:ego_end_row, :lane];
        # Get the rightmost lane of the ego on the road
        ego_rightmost_laneid = maximum(ego_laneids);
        # Get the class of the ego
        # We use trajdata_my which is loaded by using MyNGSIM
        ego_class = trajdata_my.defs[egoid].class;
        # We also loaded the AgentClass module by using MyNGSIM. AgentClass has MOTORCYCLE, CAR, TRUCK, and PEDESTRIAN.
        # We only want to investigate CAR or TRUCK in lane 1--5 (the main road)
        if ego_class == AgentClass.CAR && ego_rightmost_laneid <= 5
            # Add a margin to the appearing duration of the ego.
            ego_start_row_m = ego_start_row + VEHICLE_IN_VIEW_MARGIN;
            ego_end_row_m = ego_end_row - VEHICLE_IN_VIEW_MARGIN;
            # Define an row index for the ego.
            ego_row_ind = ego_start_row_m;
            # Define the immediately last event frame for measuring event-separation.
            # Only this measure is greater than EVENT_SPEPARATION, can we declare a new event.
            ego_lastevent_frame = tdraw_my.df[ego_start_row_m, :frame]
            # While (this vehicle is in view):
            while ego_row_ind < ego_end_row_m
                # Record frame searched
                n_searched += 1;
                # Move to the next data frame (the next row)
                ego_row_ind += 1;
                # Retrieve the frame in which the ego appears.
                ego_frame = tdraw_my.df[ego_row_ind, :frame];
                # Since the lane-changing intention is triggered by a slow lead vehicle,
                # we want to define this event (slow lead vehicle).
                # The event is 1) the lead vehicle (veh 1) is slower than the ego, and
                #              2) the adjacent lead vehicle (veh 2, 2') is faster than the lead.
                # Let us get the average speed of the ego over some frames up to the current frame.
                ego_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame,
                                                      longitudinal_direc="self",
                                                      lateral_direc = "middle", sample_duration = SAMPLE_DURATION,
                                                      aggregate_method = "mean" );
                ego_speed = ego_traffic_info[:speed];
                # Let us get the average speed of veh 1 over 3 frames up to the current frame.
                veh1_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame,
                                                       longitudinal_direc="fore",
                                                       lateral_direc = "middle", sample_duration = SAMPLE_DURATION,
                                                       aggregate_method = "mean" );
                veh1_speed = veh1_traffic_info[:speed];
                veh1_id = veh1_traffic_info[:vehicle_id];
                # Let us get the average speed of veh 2 over some frames up to the current frame.
                # First, check if the ego is in the leftmost lane,
                ego_current_laneid = tdraw_my.df[ego_row_ind, :lane]
                if ego_current_laneid == 1      # In orignial NGSIM, lane 1 is the leftmost lane.
                    veh2_speed = 0.0;
                # if the ego is not in lane 1, then get the neighbor's speed.
                else
                    veh2_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame,
                                                           longitudinal_direc="fore",
                                                           lateral_direc = "left", sample_duration = SAMPLE_DURATION,
                                                           aggregate_method = "mean" );
                    veh2_speed = veh2_traffic_info[:speed];
                end
                # Let us get the average speed of veh 22 over some frames up to the current frame.
                # First, check if the ego is in the rightmost lane,
                if ego_current_laneid == 5      # In orignial NGSIM, lane 5 is the rightmost lane.
                    veh22_speed = 0.0;
                else
                    # if the ego is not in lane 5, then get the neighbor's speed.
                    veh22_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame,
                                                            longitudinal_direc="fore",
                                                            lateral_direc = "right", sample_duration = SAMPLE_DURATION,
                                                            aggregate_method = "mean" );
                    veh22_speed = veh22_traffic_info[:speed];
                end
                # We check if the event has been triggered (n_triggers >= 1).
                # If the event has been triggered as least once, we start recording
                # the after triggering information. We make sure to at most fill the duration.
                if info_after_trigger.triggered && length(info_after_trigger.laneids) < info_after_trigger.duration
                    # Push in the current lane id.
                    # We can get the lane id either from tdraw_my or from trajdata_my.
                    # Note: in tdrw_my laneid == 1 is the leftmost.
                    # But, in trajdata_my in segment 1 and 4, laneid == 5 is the leftmost!
                    # In segment 3,, laneid == 6 is the leftmost. Please be careful!
                    # For more information, see https://github.com/LongshengJiang/NGSIM-tools/blob/master/tutorials/NGSIM/US101_roadway_explained.md
                    # The above code has used the laneid according to tdraw_my.
                    # But here, we use the laneid according to trajdata_my. Don't be confused.
                    push!(info_after_trigger.laneids, get_laneid_from_trajdata_i101(egoid, ego_frame));
                    # Push in the current ego speed.
                    push!(info_after_trigger.ego_v, ego_speed);
                    # Push in the current lead speed.
                    push!(info_after_trigger.fore_v, veh1_speed);
                    # Push in the id of the lead vehicle
                    push!(info_after_trigger.leadids, veh1_id);
                end
                # ================================================================
                # Here we model the event-triggering as the following if-statement.
                # ================================================================
                # If (veh 1 is slower than veh 0) and (veh 2 or veh 22 is faster than veh 1):
                if ego_speed - veh1_speed >= SPEED_DIFF_THRESHOLD && ( veh2_speed - veh1_speed >= SPEED_DIFF_THRESHOLD || veh22_speed - veh1_speed >= SPEED_DIFF_THRESHOLD )
                    # Only if this the triggering frame is far enough from the previous event-triggering frame, can
                    # we claim a new event.
                    # If the separation is enough, we have a new event.
                    if ego_frame - ego_lastevent_frame > EVENT_SPEPARATION
                        n_triggers += 1;

                        # We first process the information in info_after_trigger for the previous recording
                        if !isempty(info_after_trigger.laneids)
                            n_laneids_notempty += 1;
                            # Checking if a lane change or a lane keep was made
                            (lanedecision_cases, Δn_lanedecisions) = lane_change_or_keep!(lanedecision_cases, info_after_trigger);
                            # Update the counters
                            n_lanechanges += Δn_lanedecisions[1];
                            n_lanekeeps += Δn_lanedecisions[2];
                            n_abortedchanges += Δn_lanedecisions[3];
                            n_notchange += Δn_lanedecisions[4];
                            n_slow_lead += Δn_lanedecisions[5];
                        end
                        # We then reinitialize the info_after_trigger
                        info_after_trigger = InfoAfterTrigger(egoid = egoid,
                                                              triggerframe = ego_frame,
                                                              trigger_ind = n_triggers,
                                                              triggered = true,
                                                              duration = LANEDECISION_DURATION);
                    end
                    # We update the event triggering frame whenever the speed criteria as follows are triggered.
                    # ego_speed - veh1_speed >= SPEED_DIFF_THRESHOLD && ( veh2_speed - veh1_speed >= SPEED_DIFF_THRESHOLD || veh22_speed - veh1_speed >= SPEED_DIFF_THRESHOLD )
                    ego_lastevent_frame = ego_frame;
                end

                # When the while-loop is about to end, we need to check the lane decisions from the last event trigger.
                # We run some clean up steps.
                if ego_row_ind == ego_end_row_m
                    if !isempty(info_after_trigger.laneids)
                        n_laneids_notempty += 1;
                        # Checking if a lane change or a lane keep was made
                        (lanedecision_cases, Δn_lanedecisions) = lane_change_or_keep!(lanedecision_cases, info_after_trigger);
                        # Update the counters
                        n_lanechanges += Δn_lanedecisions[1];
                        n_lanekeeps += Δn_lanedecisions[2];
                        n_abortedchanges += Δn_lanedecisions[3];
                        n_notchange += Δn_lanedecisions[4];
                        n_slow_lead += Δn_lanedecisions[5];
                    end
                end
            end
        end
    end
    # Print the total number of lane changes
    println(("total searched frames:", n_searched));
    println(("total triggering events:", n_triggers));
    println(("n_laneids_notempty", n_laneids_notempty));
    println(("total number of lane-changing", n_lanechanges));
    println(("total number of lane-keeping", n_lanekeeps));
    println(("total number of aborted lane change", n_abortedchanges));
    println(("number of slow lead:", n_slow_lead));
    println(("number of not change:", n_notchange));
    # Print the elapsed time.
    toc();
    tic();
    # A final touch on saving the lane decision cases.
    println("Save the results...");
    JLD.save("./Data/lanedecision_cases_"*myngsim*".jld", "lanedecision_cases",lanedecision_cases);
    toc();
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    lanedecision_detailed_analysis()
end
