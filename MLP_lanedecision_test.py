from __future__ import print_function, division
import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import transforms, utils

import torch.nn as nn
import torch.nn.functional as F

# number of subprocesses to use for data loading
num_workers = 0
# how many samples per batch to load
batch_size = 20
# percentage of training set to use as validation
valid_size = 0.2

# ================================================================
# define datasets and dataloaders
# =================================================================


class LaneDecisionDataSet(Dataset):
    def __init__(self, csv_file, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        features = self.data.iloc[idx, 2:9]
        features = np.array(features)
        features = torch.from_numpy(features).type(torch.float32)

        decision = self.data.iloc[idx, 9]
        decision = np.array([decision])
        decision = torch.from_numpy(decision).type(torch.float32)

        sample = {'features': features, 'decision': decision}

        if self.transform:
            sample = self.transform(sample)

        return sample

# choose the testing datasets
test_data = LaneDecisionDataSet(csv_file='./Data/MLP_traj_test_all.csv')
test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, num_workers=num_workers)

# ================================================================
# define NN architecture
# =================================================================


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        # number of hidden nodes in the hidden layer
        hidden_1 = 4
        hidden_2 = 2
        # linear layer (7 -> hidden_1)
        self.fc1 = nn.Linear(7, hidden_1)
        ## linear layer (hidden_1 -> hidden_2)
        # self.fc2 = nn.Linear(hidden_1, hidden_2)
        # linear layer (hidden_2 -> 1)
        self.fc3 = nn.Linear(hidden_1, 1)

    def forward(self, x):
        # add hidden layer, with sigmoid activation function
        x = torch.sigmoid(self.fc1(x))
        ## add hidden layer, with sigmoid activation function
        # x = torch.sigmoid(self.fc2(x))
        # add hidden layer, with sigmoid activation function
        x = torch.sigmoid(self.fc3(x))

        return x


# initialize the NN
model = Net()
print(model)

# specify loss function (categorical cross-entropy)
criterion = nn.MSELoss()

# specify optimizer (stochastic gradient descent and learning rate = 0.01)
optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

# iterate through different trained models
for model_i in range(1, 11):
    # Load the saved data
    model.load_state_dict(torch.load('./MLP_models/model'+str(model_i)+'.pt'))

    # initialize lists to monitor test loss and accuracy
    test_loss = 0.0
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))

    model.eval()    # prepare model for evaluation
    pred_all = torch.tensor([])     # Initialize data containers
    label_all = torch.tensor([])

    for mybatch in test_loader:
        # get the features and targets
        data = mybatch['features']
        target = mybatch['decision']
        # forward pass: compute predicted outputs by passing inputs to the model
        output = model(data)
        # calculate the loss
        loss = criterion(output, target)
        # update test loss
        test_loss += loss.item() * data.size(0)

        # store the output and the target
        pred_all = torch.cat((pred_all, output))
        label_all = torch.cat((label_all, target))

    # Calculate and print avg test loss
    test_loss = test_loss/len(test_loader.sampler)
    print("Test loss: {:.6f}\n".format(test_loss))

    # Put the predictions and the labels together
    pred_label = torch.cat((pred_all, label_all), 1)
    # Convert to numpy
    pred_label_df = pd.DataFrame(pred_label.detach().numpy(), columns=['prediction', 'label'])
    # Save the dataframe
    pred_label_df.to_csv("./Data/MLP_test_performance"+str(model_i)+".csv")
    print(pred_label_df)
