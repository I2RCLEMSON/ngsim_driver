# ngsim_driver (julia-lang + Ubuntu)

## Prerequisite
This package works on Ubuntu 18.04. It may work on newer Ubuntu but I have not test it. 

Before you use this package, make sure you have install the `ngsim_tools` properly (https://github.com/LongshengJiang/NGSIM-tools)

After installing `ngsim_tools`, you will find the directory `~/.julia/v0.6/`. The directory is hidden, make sure you show the hidden files. This directory stores the modules used by julia. 

`ngsim_tool` guides you installing necessary packages and setting up the programming environment for julia. 

Once the setup is finished, please copy the files from this package at `./Required_Modules/`, namely `MyNGSIM1`, `MyNGSIM2`, and `MyNGSIM3`, to  `~/.julia/v0.6/`. These are our customized modules and they correspond to different data sets in NGSIM as follows.
- `MyNGSIM1` --> i101_0750_0805
- `MyNGSIM2` --> i101_0805_0820
- `MyNGSIM3` --> i101_0820_0835 


## Summary
The pipeline of running the programs is

`changekeep_cases.jl` ==> `make_animation.jl` ==>`analyze_decision.jl`==>`imbalanced_classes.jl`
==> `model_train_test.jl` ==> `model_plots.jl`

When running the programs in the pipeline, you don't need to start from the very beginning every time. Each program has its output
saved. The saved data are used as inputs by the following program. You just need to start from the later program. 
For example, after you have run `changekeep_cases.jl` at least once, you can start from running `make_animation.jl`. 

Each program has one main function, make sure you call the function by its **function name** instead
of the **file name** of the program.

## Pipeline 
### `changekeep_cases.jl`
This program identifies vehicle trajectory segments in NGSIM data. 
It needs only run once. 

Only the lane-change or lane-keep data due to slow lead vehicles are identified.

The main function of `changekeep_cases.jl` is `lanedecisions_dueto_slowlead()`.  

To run this program, we need to open a terminal on Ubuntu and `cd` to the current directory (the directory of this package).

Activate julia REPL by typing `julia`. In the julia REPL, typing 
```
include("changekeep_cases.jl")
```
Wait and prompts in the REPL and select the corresponding module, for instance `MyNGSIM1`. 
Once the REPL is ready, type in
```
lanedecisions_dueto_slowlead()
```
Then, sit back and wait for the results. The results are saved to be used in later programs in the pipeline.

Whenever, you make modifications to the file `changekeep_cases.jl`, make sure you include the program in julia again using `include("changekeep_cases.jl")` for letting the modification take effect.

### `make_animation.jl`
This program makes animations for the identified vehicle trajectory segments. Those animations are saved to the directory `./Animation/`

The main function of this program is 
`make_animation()`

To run this program, type 
```
include("make_animation.jl")
make_animation()
```
You need respond to the prompts.

### `analyze_decision.jl`
This is one of the most important program. It extracts the traffic data at the decision-making moments. 
The perception and cognition modules of our PCED model are implemented in this file.
Hence, it computes the x<sub> 1</sub>, x<sub> 3</sub>, x<sub> 3</sub>, and p for the lane-change decision-making problem 

|  | **Lane-change** |  |  | **Lane-keep** | 
|-----------| ---------- |-------------|------| -------------------|
|Outcome|x <sub>2</sub>|x <sub>3</sub>|| x <sub>1</sub>|
|Probability |p|1-p||1|

In short, this program outputs the raw data points to be used for model fitting and testing. 

The main function of this program is `lanedecision_detailed_analysis()`

To run this program, type in julia REPL
```
include("analyze_decision.jl")
lanedecision_detailed_analysis()
```
You need to response to the prompts

You can change the setting of the program. 
The setting information is in the beginning of the main function `lanedecision_detailed_analysis()`.  It looks as follows.

```julia
#####################################################
 analyze_subject = ask_which_lane_decision();
 plotting = true;
 data_extracting = false;
 debugging = false;
 howmany_cases = 10000;
 ####################################################
```
To enable plotting, `plotting=true`. To disable plotting, `plotting=false`. 
If `plotting=true`, the plotted figures are saved in the directory`./Pictures/`
To turn on data_extraction, `data_extraction=true`. To turn off data_extraction, `data_extraction=false`.
You can control how many cases to include. Setting `howmany_cases = 10000` includes all the available cases.
You can make changes as needed. 

### `imbalanced_classes.jl`
This program uses SMOTE technique to re-balance the data set. 
The main function of this program is `SMOTE()`

To run this program, in julia REPL type 
```
include("imbalanced_classes.jl")
SMOTE()
```
You need to respond to the prompts if any.
This program generates re-balanced data for the later program in the pipeline: `model_train_test.jl`

### `model_train_test.jl`
This program train and test the different models studied in our paper: EV, RTx0, RTx1, RTx7, RTx and GA. 

The main function is `k_fold_cross_validation()`
This program generates the fitted parameters and the testing results of the models. 

To run this program, in julia REPL type
```
include("model_train_test.jl")
k_fold_cross_validation()
```
Sit back, relax, and wait for the training of the model. It takes a while (about a hour).
You can make yourself some coffee or go playing some basketball just like I did `^_^`

This program gives the final results in numbers. 
These results can be visualized with `model_plots.jl`

### `model_plots.jl`
This program visualizes the results.
The main function is `make_figures()`

To run this program, in julia REPL type
```
include("model_plots.jl")
make_figures()
```

## Folder structure
Besides the programs, let's also introduce the other folders and files in the package.
- `./Animation/` stores the animations generated by `make_animation.jl`
- `./Data/` stores the intermediate data generated in the pipeline
- `./Model_fit_results/` stores the plots generated by `model_plots.jl`.
- `./Pictures/` stores the pictures generated by `analyze_decision.jl` when `plotting=true`.
- `./Required_Modules/` stores the customized NGSIM modules. They must be copied to the directory where the modules of julia is saved. 
- `/Results/` stores the final results generated by `model_train_test.jl`
- `/Supplement/` stores the important supporting programs and functions. In this folder, the `core.jl` contains the most important functions corresponding to the modules in the PCED model. 
- `US101_lane_segment_explained` explains the lane setup in the NGSIM data set. This information is important for programming the functions in this package. Hence, it may be valuable if you want to modify this package. 
