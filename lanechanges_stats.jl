include("./Supplement/core.jl");

# """
# Get the total number of lane changes in lane 1--5
# """
function lanechanges_stats()
    # Start recording the lane changes
    n_lanechange = 0;
    n_car_lanekeep = 0;
    n_aborted = 0;
    # We define some containers
    # A dictionary of aborted lane changes
    aborted_lanechanges = Dict{Int, Vector{Int}}()
    #For a vehicle in all the vehicles:
    for egoid in keys(tdraw_my.car2start)  # tdraw_my is loaded by using MyNGSIM
        # Get the starting row of the ego vehicle in tdraw_my
        ego_start_row = tdraw_my.car2start[egoid];
        # Get the total number of frames of the ego vehicle in tdraw_my
        total_n_frames = tdraw_my.df[ego_start_row, :n_frames_in_dataset];
        # Compute the ending row of the ego vehicle in tdraw_my
        ego_end_row = ego_start_row + total_n_frames - 1; # df is organized by clustering same vehicle together.
        # Get the lane  of the ego
        ego_laneids = tdraw_my.df[ego_start_row:ego_end_row, :lane];
        # Get the current lane id
        ego_current_laneid = ego_laneids[1];
        # Let us get the starting frame of the ego
        ego_frame = tdraw_my.df[ ego_start_row, :frame ]
        # Also, we include an immediately last lane for checking aborted lane-changing
        ego_immlast_laneid = ego_current_laneid;
        # Let us assume the last lane change happens more than 2s ago.
        ego_lastchange_frame = ego_frame - 21;
        # Get the rightmost lane of the ego on the road
        ego_rightmost_laneid = maximum(ego_laneids);
        # Get the class of the ego
        # We use trajdata_my which is loaded by using MyNGSIM
        ego_class = trajdata_my.defs[egoid].class;
        # We also loaded the AgentClass module by using MyNGSIM. AgentClass has MOTORCYCLE, CAR, TRUCK, and PEDESTRIAN.
        # We only want to investigate CAR in lane 1--5 (the main road)
        if ego_class == AgentClass.CAR && ego_rightmost_laneid <= 5
            # Check the lane id frame by frame.
            for (i, laneid) in enumerate(ego_laneids)
                # If the lane id is not the same as the ego_old_laneid,
                if laneid != ego_current_laneid
                    # Get the frame in which the lane change happens.
                    ego_frame = tdraw_my.df[ ego_start_row + i - 1 ,:frame ]
                    # We check if this is aborting the onging lane-changing maneuver
                    # by checking if the ego goes back to the immediately last lane within 2s.
                    if laneid == ego_immlast_laneid && ego_frame - ego_lastchange_frame <= 20
                        n_aborted += 1;
                        n_lanechange -= 1;
                        println(("ego id", egoid, "abort change at frame", ego_frame));
                        # If the ego is not recorded yet, we record it for the first time
                        if !haskey(aborted_lanechanges, egoid)
                            aborted_lanechanges[egoid] = [ego_frame]
                        # else, the ego is already recorded, we add a new frame instance.
                        else
                            push!(aborted_lanechanges[egoid], ego_frame)
                        end
                    else
                        println(("ego id", egoid, "change at frame", ego_frame));
                        n_lanechange += 1
                        # The current lane becomes immediately last lane.
                        ego_immlast_laneid = ego_current_laneid;
                        # We also record when this happens
                        ego_lastchange_frame = ego_frame;
                    end
                    # Update the current lane id
                    ego_current_laneid = laneid;
                # Else: the lane id is always the same
            elseif laneid == ego_laneids[1]
                    # If it is the last recorded frame the ego,
                    if i == length(ego_laneids)
                        n_lanekeep += 1;
                    end
                end
            end
        end
    end
    # Print the total number of lane changes
    println(("total number of lane-changing", n_lanechange));
    println(("total number of cars always lane-keeping", n_car_lanekeep));
    println(("total number of aborted lane change", n_aborted));
    aborted_lanechanges
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    lanedecision_detailed_analysis()
end
