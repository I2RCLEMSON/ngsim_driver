# Include a small library
include("./Supplement/analysis.jl")

using PyCall
using LaTeXStrings

pyplt = pyimport("matplotlib.pyplot");
pyimport("mpl_toolkits.mplot3d");


"Main function of this file"
function make_figures()

    # ==============================================================================
    # Load data
    # ==============================================================================
    # Read dataframes from csv files.
    #  confusion matrices
    confusion_df_rtx_orig = DataFrame( CSV.read("./Results/RTx_performance_dataset_orig.csv") );
    stat_df_rtx_orig = DataFrame( CSV.read("./Results/RTx_performance_stats_orig.csv" ) );

    confusion_df_rtx1_orig = DataFrame( CSV.read("./Results/RTx1_performance_dataset_orig.csv") );
    stat_df_rtx1_orig = DataFrame( CSV.read("./Results/RTx1_performance_stats_orig.csv") );

    confusion_df_rtx0_orig = DataFrame( CSV.read("./Results/RTx0_performance_dataset_orig.csv") );
    stat_df_rtx0_orig = DataFrame( CSV.read("./Results/RTx0_performance_stats_orig.csv") );

    confusion_df_rtx7_orig = DataFrame( CSV.read("./Results/RTx7_performance_dataset_orig.csv") );
    stat_df_rtx7_orig = DataFrame( CSV.read("./Results/RTx7_performance_stats_orig.csv") );

    confusion_df_toledo_orig = DataFrame( CSV.read("./Results/Toledo_performance_dataset_orig.csv") );
    stat_df_toledo_orig = DataFrame( CSV.read("./Results/Toledo_performance_stats_orig.csv") );

    confusion_df_ttc_orig = DataFrame( CSV.read("./Results/TTC_performance_dataset_orig.csv") );
    stat_df_ttc_orig = DataFrame( CSV.read("./Results/TTC_performance_stats_orig.csv") );

    confusion_df_mlp_orig = DataFrame( CSV.read("./Results/MLP_performance_dataset_orig.csv") );
    stat_df_mlp_orig = DataFrame( CSV.read("./Results/MLP_performance_stats_orig.csv") );

    # parameters
    parameter_df_rtx= DataFrame( CSV.read("./Results/parameters_dataset_rtx.csv") );
    stat_df_para_rtx = DataFrame( CSV.read("./Results/parameters_stats_rtx.csv") );

    parameter_df_rtx1= DataFrame( CSV.read("./Results/parameters_dataset_rtx1.csv") );
    stat_df_para_rtx1 = DataFrame( CSV.read("./Results/parameters_stats_rtx1.csv") );

    parameter_df_rtx0= DataFrame( CSV.read("./Results/parameters_dataset_rtx0.csv") );
    stat_df_para_rtx0 = DataFrame( CSV.read("./Results/parameters_stats_rtx0.csv") );

    parameter_df_rtx7= DataFrame( CSV.read("./Results/parameters_dataset_rtx7.csv") );
    stat_df_para_rtx7 = DataFrame( CSV.read("./Results/parameters_stats_rtx7.csv") );

    parameter_df_toledo= DataFrame( CSV.read("./Results/parameters_dataset_toledo.csv") );
    stat_df_para_toledo = DataFrame( CSV.read("./Results/parameters_stats_toledo.csv") );

    # ROC curves
    RTx_roc_curves_orig = JLD.load( "./Results/RTx_roc_orig.jld", "RTx_roc_curves" );
    RTx1_roc_curves_orig = JLD.load( "./Results/RTx1_roc_orig.jld", "RTx1_roc_curves");
    RTx0_roc_curves_orig = JLD.load( "./Results/RTx0_roc_orig.jld", "RTx0_roc_curves");
    RTx7_roc_curves_orig = JLD.load( "./Results/RTx7_roc_orig.jld", "RTx7_roc_curves");
    Toledo_roc_curves_orig = JLD.load( "./Results/Toledo_roc_orig.jld", "Toledo_roc_curves");
    TTC_roc_curves_orig = JLD.load( "./Results/TTC_roc_orig.jld", "TTC_roc_curves");
    MLP_roc_curves_orig = JLD.load( "./Results/MLP_roc_orig.jld", "MLP_roc_curves");

    # training progresses
    train_progresses_rtx = JLD.load( "./Results/train_progresses_rtx.jld", "train_progresses_rtx" );

    # Load the data from the filename
    rsk_lc_df = DataFrame( CSV.read("./Data/riskseeking_lanechange.csv") );
    nrsk_lc_df = DataFrame( CSV.read("./Data/nonriskseeking_lanechange.csv") );
    lk_df = DataFrame( CSV.read( "./Data/lanekeep.csv" ) );
    syn_lc_df = DataFrame( CSV.read("./Data/synthesized_change.csv") );

    clusters = JLD.load( "./Data/lanechange_clustering.jld", "clusters" );

    # ==============================================================================
    # Generate figures
    # ==============================================================================

    # Update global plotting parameters
    fs = 12;    # global font size

    # Plot the regret q-function
    δ = linspace(-20.0, 20.0, 100 );
    qx = stat_df_para_rtx[1, :gamma1] .* δ + stat_df_para_rtx[1, :gamma2] .* sinh.( stat_df_para_rtx[1, :gamma3] .* δ )
    # q3 = stat_df_para_rtx3[1, :gamma1] .* δ + stat_df_para_rtx3[1, :gamma2] .* sinh.( stat_df_para_rtx3[1, :gamma3] .* δ );
    q7 = stat_df_para_rtx7[1, :gamma1] .* δ + stat_df_para_rtx7[1, :gamma2] .* sinh.( stat_df_para_rtx7[1, :gamma3] .* δ );
    figure();
    # change overall font size
    # plt[:rcParams]["font.size" ] = 22;
    plot( δ, q7, "--", label = "RTx7", color="g");
    plot( δ, qx, linewidth=2, label = "RTx", color="m");

    grid(true);
    legend( fontsize = fs );
    xlim( [-15.0, 15.0] );
    ylim( [-8.0, 8.0] )
    xlabel( L"\Delta x", fontsize = fs );
    ylabel( L"q(\Delta x)", fontsize = fs );
    major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
    ax = gca();
    ax[:yaxis][:set_major_locator](major_ytick);
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    ax_path_q = "./Model_fit_results/q_function.pdf";
    println("Print to: ", ax_path_q);
    savefig(ax_path_q);

    # Plot the probability weighting function
    p = linspace(0.0, 1.0, 100 );
    w7 = stat_df_para_rtx7[1, :gamma8] +  stat_df_para_rtx7[1, :gamma9] .* exp.( -stat_df_para_rtx7[1, :gamma4] .*
        ( -log.( p ) ) .^ stat_df_para_rtx7[1, :gamma5] );
    wx = stat_df_para_rtx[1, :gamma8] +  stat_df_para_rtx[1, :gamma9] .* exp.( -stat_df_para_rtx[1, :gamma4] .*
        ( -log.( p ) ) .^ stat_df_para_rtx[1, :gamma5] );

    figure();
    plot( p, w7, linewidth=2, "--", label = "RTx7", color="g");
    plot( p, wx, linewidth=2, label = "RTx", color="m");
    plot([0, 1], [0, 1], color="black", "-.");

    grid(true);
    legend( fontsize = fs );
    xlabel( L"p", fontsize = fs );
    ylabel( L"w(p)", fontsize = fs );
    xlim( [0, 1] );
    ylim( [0, 1] );
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    ax_path_w = "./Model_fit_results/w_function.pdf";
    println("Print to: ", ax_path_w);
    savefig(ax_path_w);

    # Plot roc_curves for testing data
    figure();
    # Compute the average roc curves for RTx0
    RTx0_roc_avg_x_orig = RTx0_roc_curves_orig[ 1, :, 1 ];
    RTx0_roc_avg_y_orig = [ mean( RTx0_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx0_roc_curves_orig, 2) ];
    plot(RTx0_roc_avg_x_orig, RTx0_roc_avg_y_orig, "-.", color="tab:orange",  linewidth=2, label="RTx0");
    # Plot the boundaries
    RTx0_roc_std_y_orig = [ std( RTx0_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx0_roc_curves_orig, 2) ];
    RTx0_roc_up_y_orig = RTx0_roc_avg_y_orig + RTx0_roc_std_y_orig;
    RTx0_roc_lo_y_orig = RTx0_roc_avg_y_orig - RTx0_roc_std_y_orig;
    fill_between(RTx0_roc_avg_x_orig, RTx0_roc_up_y_orig ,RTx0_roc_lo_y_orig, color="tab:orange", alpha=0.2);

    # Compute the average roc curves for RTx1
    RTx1_roc_avg_x_orig = RTx1_roc_curves_orig[ 1, :, 1 ];
    RTx1_roc_avg_y_orig = [ mean( RTx1_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx1_roc_curves_orig, 2) ];
    plot(RTx1_roc_avg_x_orig, RTx1_roc_avg_y_orig, "--", color="b",  linewidth=2, label="RTx1");
    # Plot the boundaries
    RTx1_roc_std_y_orig = [ std( RTx1_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx1_roc_curves_orig, 2) ];
    RTx1_roc_up_y_orig = RTx1_roc_avg_y_orig + RTx1_roc_std_y_orig;
    RTx1_roc_lo_y_orig = RTx1_roc_avg_y_orig - RTx1_roc_std_y_orig;
    fill_between(RTx1_roc_avg_x_orig, RTx1_roc_up_y_orig ,RTx1_roc_lo_y_orig, color="b", alpha=0.2);

    # Compute the average roc curves for RTx7
    RTx7_roc_avg_x_orig = RTx7_roc_curves_orig[ 1, :, 1 ];
    RTx7_roc_avg_y_orig = [ mean( RTx7_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx7_roc_curves_orig, 2) ];
    plot(RTx7_roc_avg_x_orig, RTx7_roc_avg_y_orig, linestyle = "dotted", color="g",  linewidth=2, label="RTx7");
    # Plot the boundaries
    RTx7_roc_std_y_orig = [ std( RTx7_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx7_roc_curves_orig, 2) ];
    RTx7_roc_up_y_orig = RTx7_roc_avg_y_orig + RTx7_roc_std_y_orig;
    RTx7_roc_lo_y_orig = RTx7_roc_avg_y_orig - RTx7_roc_std_y_orig;
    fill_between(RTx7_roc_avg_x_orig, RTx7_roc_up_y_orig, RTx7_roc_lo_y_orig, color="g", alpha=0.2);

    # Compute the average roc curves for RTx
    # Plot the mean
    RTx_roc_avg_x_orig = RTx_roc_curves_orig[ 1, :, 1 ];
    RTx_roc_avg_y_orig = [ mean( RTx_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx_roc_curves_orig, 2) ];
    plot(RTx_roc_avg_x_orig, RTx_roc_avg_y_orig, "-", color="m", linewidth=2, label="RTx");
    # Plot the boundaries
    RTx_roc_std_y_orig = [ std( RTx_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx_roc_curves_orig, 2) ];
    RTx_roc_up_y_orig = RTx_roc_avg_y_orig + RTx_roc_std_y_orig;
    RTx_roc_lo_y_orig = RTx_roc_avg_y_orig - RTx_roc_std_y_orig;
    fill_between(RTx_roc_avg_x_orig, RTx_roc_up_y_orig ,RTx_roc_lo_y_orig, color="m", alpha=0.2);

    grid(true);
    xlabel( "FPR", fontsize = fs );
    ylabel( "TPR", fontsize = fs);
    xlim( [0, 1] );
    ylim( [0, 1] );
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    legend( loc="lower right" , fontsize = fs );
    ax_path_roc_orig = "./Model_fit_results/roc_orig.pdf";
    println("Print to: ", ax_path_roc_orig);
    savefig(ax_path_roc_orig);


    # Plot roc_curves of benchmarks for testing data
    figure();

    # Compute the average roc curves for Time-to-collision model
    TTC_roc_avg_x_orig = TTC_roc_curves_orig[ 1, :, 1 ];
    TTC_roc_avg_y_orig = [ mean( TTC_roc_curves_orig[ 2, i, : ] ) for i = 1:size(TTC_roc_curves_orig, 2) ];
    plot(TTC_roc_avg_x_orig, TTC_roc_avg_y_orig, linestyle = "--", color="b",  linewidth=2, label="TTC");
    # Plot the boundaries
    TTC_roc_std_y_orig = [ std( TTC_roc_curves_orig[ 2, i, : ] ) for i = 1:size(TTC_roc_curves_orig, 2) ];
    TTC_roc_up_y_orig = TTC_roc_avg_y_orig + TTC_roc_std_y_orig;
    TTC_roc_lo_y_orig = TTC_roc_avg_y_orig - TTC_roc_std_y_orig;
    fill_between(TTC_roc_avg_x_orig, TTC_roc_up_y_orig ,TTC_roc_lo_y_orig, color="b", alpha=0.2);

    # Compute the average roc curves for Toledo gap acceptance model
    MLP_roc_avg_x_orig = MLP_roc_curves_orig[ 1, :, 1 ];
    MLP_roc_avg_y_orig = [ mean( MLP_roc_curves_orig[ 2, i, : ] ) for i = 1:size(MLP_roc_curves_orig, 2) ];
    plot(MLP_roc_avg_x_orig, MLP_roc_avg_y_orig, linestyle = "-.", color="r",  linewidth=2, label="MLP");
    # Plot the boundaries
    MLP_roc_std_y_orig = [ std( MLP_roc_curves_orig[ 2, i, : ] ) for i = 1:size(MLP_roc_curves_orig, 2) ];
    MLP_roc_up_y_orig = MLP_roc_avg_y_orig + MLP_roc_std_y_orig;
    MLP_roc_lo_y_orig = MLP_roc_avg_y_orig - MLP_roc_std_y_orig;
    fill_between(MLP_roc_avg_x_orig, MLP_roc_up_y_orig ,MLP_roc_lo_y_orig, color="r", alpha=0.2);

    # Compute the average roc curves for Toledo gap acceptance model
    Toledo_roc_avg_x_orig = Toledo_roc_curves_orig[ 1, :, 1 ];
    Toledo_roc_avg_y_orig = [ mean( Toledo_roc_curves_orig[ 2, i, : ] ) for i = 1:size(Toledo_roc_curves_orig, 2) ];
    plot(Toledo_roc_avg_x_orig, Toledo_roc_avg_y_orig, linestyle = "dotted", color="g",  linewidth=2, label="GA");
    # Plot the boundaries
    Toledo_roc_std_y_orig = [ std( Toledo_roc_curves_orig[ 2, i, : ] ) for i = 1:size(Toledo_roc_curves_orig, 2) ];
    Toledo_roc_up_y_orig = Toledo_roc_avg_y_orig + Toledo_roc_std_y_orig;
    Toledo_roc_lo_y_orig = Toledo_roc_avg_y_orig - Toledo_roc_std_y_orig;
    fill_between(Toledo_roc_avg_x_orig, Toledo_roc_up_y_orig ,Toledo_roc_lo_y_orig, color="g", alpha=0.2);

    # Compute the average roc curves for RTx
    # Plot the mean
    RTx_roc_avg_x_orig = RTx_roc_curves_orig[ 1, :, 1 ];
    RTx_roc_avg_y_orig = [ mean( RTx_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx_roc_curves_orig, 2) ];
    plot(RTx_roc_avg_x_orig, RTx_roc_avg_y_orig, "-", color="m", linewidth=2, label="PRED");
    # Plot the boundaries
    RTx_roc_std_y_orig = [ std( RTx_roc_curves_orig[ 2, i, : ] ) for i = 1:size(RTx_roc_curves_orig, 2) ];
    RTx_roc_up_y_orig = RTx_roc_avg_y_orig + RTx_roc_std_y_orig;
    RTx_roc_lo_y_orig = RTx_roc_avg_y_orig - RTx_roc_std_y_orig;
    fill_between(RTx_roc_avg_x_orig, RTx_roc_up_y_orig ,RTx_roc_lo_y_orig, color="m", alpha=0.2);

    grid(true);
    xlabel( "FPR", fontsize = fs );
    ylabel( "TPR", fontsize = fs);
    xlim( [0, 1] );
    ylim( [0, 1] );
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    legend( loc="lower right" , fontsize = fs );
    ax_path_broc_orig = "./Model_fit_results/benchmark_roc_orig.pdf";
    println("Print to: ", ax_path_broc_orig);
    savefig(ax_path_broc_orig);


    # ==========================================================================
    # Make the bar plot
    # ==========================================================================
    # Plot the bars for RTx variants
    figure();
    x_labels = [ "TPR", "TNR", "F1-score" ];
    x_pos = 1:3;
    y_RTx0_orig = [ stat_df_rtx0_orig[1, :sensitivity], stat_df_rtx0_orig[1, :specificity], stat_df_rtx0_orig[1, :f1_score] ];
    y_RTx1_orig = [ stat_df_rtx1_orig[1, :sensitivity], stat_df_rtx1_orig[1, :specificity], stat_df_rtx1_orig[1, :f1_score] ];
    y_RTx7_orig = [ stat_df_rtx7_orig[1, :sensitivity], stat_df_rtx7_orig[1, :specificity], stat_df_rtx7_orig[1, :f1_score] ];
    y_RTx_orig =  [ stat_df_rtx_orig[1, :sensitivity], stat_df_rtx_orig[1, :specificity], stat_df_rtx_orig[1, :f1_score] ];

    yerr_RTx0_orig = [ stat_df_rtx0_orig[2, :sensitivity], stat_df_rtx0_orig[2, :specificity], stat_df_rtx0_orig[2, :f1_score] ];
    yerr_RTx1_orig = [ stat_df_rtx1_orig[2, :sensitivity], stat_df_rtx1_orig[2, :specificity], stat_df_rtx1_orig[2, :f1_score] ];
    yerr_RTx7_orig = [ stat_df_rtx7_orig[2, :sensitivity], stat_df_rtx7_orig[2, :specificity], stat_df_rtx7_orig[2, :f1_score] ];
    yerr_RTx_orig = [ stat_df_rtx_orig[2, :sensitivity], stat_df_rtx_orig[2, :specificity], stat_df_rtx_orig[2, :f1_score] ];

    barwidth = 0.17;
    # plotting
    bar( x_pos - barwidth * 3.15 / 2, y_RTx0_orig, barwidth, yerr=yerr_RTx0_orig, capsize = 3, color="tab:orange", hatch="\\", label="RTx0" );
    bar( x_pos - barwidth * 1.05 / 2, y_RTx1_orig, barwidth, yerr=yerr_RTx1_orig, capsize = 3, color="b", hatch="/", label="RTx1");
    bar( x_pos + barwidth * 1.05 / 2, y_RTx7_orig, barwidth, yerr=yerr_RTx7_orig, capsize = 3, color="g", hatch="\\\\", label="RTx7");
    bar( x_pos + barwidth * 3.15 / 2, y_RTx_orig, barwidth, yerr=yerr_RTx_orig, capsize = 3, color="m", hatch="//", label="RTx");
    # Set x-y labels
    legend(loc="lower right", framealpha = 1.0, fontsize = fs );
    ax = gca();
    ax[:set_xticks]( x_pos );
    ax[:set_xticklabels]( x_labels, fontsize = fs );
    ylabel( "Rates", fontsize = fs );
    ylim([0, 1]);
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    ax_path_metrics_orig = "./Model_fit_results/metrics_orig.pdf";
    println("Print to: ", ax_path_metrics_orig);
    savefig(ax_path_metrics_orig);

    ## Plot the bars for benchmarks
    figure();
    x_labels = [ "TPR", "TNR", "F1-score" ];
    x_pos = 1:3;

    y_RTx_orig =  [ stat_df_rtx_orig[1, :sensitivity], stat_df_rtx_orig[1, :specificity], stat_df_rtx_orig[1, :f1_score] ];
    y_Toledo_orig = [ stat_df_toledo_orig[1, :sensitivity], stat_df_toledo_orig[1, :specificity], stat_df_toledo_orig[1, :f1_score] ];
    y_MLP_orig = [ stat_df_mlp_orig[1, :sensitivity], stat_df_mlp_orig[1, :specificity], stat_df_mlp_orig[1, :f1_score] ];
    y_TTC_orig = [ stat_df_ttc_orig[1, :sensitivity], stat_df_ttc_orig[1, :specificity], stat_df_ttc_orig[1, :f1_score] ];

    yerr_RTx_orig = [ stat_df_rtx_orig[2, :sensitivity], stat_df_rtx_orig[2, :specificity], stat_df_rtx_orig[2, :f1_score] ];
    yerr_Toledo_orig = [ stat_df_toledo_orig[2, :sensitivity], stat_df_toledo_orig[2, :specificity], stat_df_toledo_orig[2, :f1_score] ];
    yerr_MLP_orig = [ stat_df_mlp_orig[2, :sensitivity], stat_df_mlp_orig[2, :specificity], stat_df_mlp_orig[2, :f1_score] ];
    yerr_TTC_orig = [ stat_df_ttc_orig[2, :sensitivity], stat_df_ttc_orig[2, :specificity], stat_df_ttc_orig[2, :f1_score] ];

    # Start plotting
    barwidth = 0.17;
    bar( x_pos - barwidth * 3.15 / 2, y_TTC_orig, barwidth, yerr=yerr_TTC_orig, capsize = 3, color="b",  hatch="/", label="TTC" );
    bar( x_pos - barwidth * 1.05 / 2, y_MLP_orig, barwidth, yerr=yerr_MLP_orig, capsize = 3, color="r",  hatch="\\\\", label="MLP" );
    bar( x_pos + barwidth * 1.05 / 2, y_Toledo_orig, barwidth, yerr=yerr_Toledo_orig, capsize = 3, color="g",  hatch="\\", label="GA" );
    bar( x_pos + barwidth * 3.15 / 2, y_RTx_orig, barwidth, yerr=yerr_RTx_orig, capsize = 3, color="m", hatch="//", label="PRED");

    # Set x-y labels
    legend(loc="lower right", framealpha = 1.0, fontsize = fs );
    ax = gca();
    ax[:set_xticks]( x_pos );
    ax[:set_xticklabels]( x_labels, fontsize = fs );
    ylabel( "Rates", fontsize = fs );
    ylim([0, 1]);
    xticks( fontsize = fs );
    yticks( fontsize = fs );
    ax_path_bmetrics_orig = "./Model_fit_results/benchmark_metrics_orig.pdf";
    println("Print to: ", ax_path_bmetrics_orig);
    savefig(ax_path_bmetrics_orig);

    # # Plot the training progress
    # figure();
    # range_end = 100;
    # # Compute the mean and std of the train progress curve.
    # loglik_mat = zeros( length(train_progresses_rtx), range_end );
    # for i = 1:length(train_progresses_rtx)
    #     loglik_mat[ i , : ] = train_progresses_rtx[ i ][ 2 ][ 1:range_end ];
    # end
    #
    # loglik_mean = [ mean( loglik_mat[:, j] ) for j = 1:range_end ];
    # loglik_std  = [ std( loglik_mat[:, j] ) for j = 1:range_end ];
    # loglik_up = loglik_mean + loglik_std;
    # loglik_lo = loglik_mean - loglik_std;
    #
    # iter = train_progresses_rtx[ 1 ][ 1 ][ 1:range_end ];
    # plot(iter, loglik_mean, color="b");
    # fill_between(iter, loglik_up, loglik_lo, color="b", alpha=0.3);
    #
    # grid(true);
    # xlim([0, 100]);
    # ylim([-10000, 0]);
    # xlabel( "iteration" );
    # ylabel( "log-likelihood" );
    # ax = gca();
    # ax[:set_yticks]( -10000:2000:0 );
    # ax[:set_yticklabels]( [ "-10k", "-8k", "-6k", "-4k", "-2k", "0" ] );
    # ax_path_loglik_global = "./Model_fit_results/logliklihood_global.pdf";
    # println("Print to: ", ax_path_loglik_global);
    # savefig(ax_path_loglik_global);

    # Make a zoomed in copy
    # figure();
    # plot(iter, loglik_mean, color="b");
    # fill_between(iter, loglik_up, loglik_lo, color="b", alpha=0.3);
    #
    # grid(true);
    # xlim([10, 20]);
    # ylim([-5000, 0]);
    # ax = gca();
    # ax[:set_yticks]( -5000:1000:0 );
    # ax[:set_yticklabels]( [ "-5k", "-4k", "-3k", "-2k", "-1k", "0" ] );
    # xlabel( "iteration" );
    # ylabel( "log-likelihood" );
    # ax_path_loglik_local = "./Model_fit_results/logliklihood_local.pdf";
    # println("Print to: ", ax_path_loglik_local);
    # savefig(ax_path_loglik_local);


    # Plot clustering
    # Get data
    k_clusters = length( clusters );
    rsk_lc_mat = Matrix{Float64}(rsk_lc_df);
    nrsk_lc_mat = Matrix{Float64}(nrsk_lc_df);
    lc_mat = vcat( rsk_lc_mat, nrsk_lc_mat );
    lc_added = Matrix{Float64}(syn_lc_df);

    figure();
    fs_c = 20;     # fontsize
    ax = pyplt[:axes](projection = "3d");
    mycolor = ["b", "r", "g", "m", "c", "tab:orange"];
    for i = 1:k_clusters
        mylabel = "cluster "*string(i);
        clus_points = rsk_lc_mat[ clusters[i], : ];
        ax[:scatter]( clus_points[:, 1] - clus_points[:, 3],
                      clus_points[:, 2] - clus_points[:, 3],
                      clus_points[:, 4], label = mylabel, color=mycolor[i] );

    end

    legend( loc="upper left", fontsize = fs_c );
    xlabel(L"\Delta x_{21}^c", fontsize = fs_c );
    ylabel(L"\Delta x_{31}^c", fontsize = fs_c );
    zlabel(L"p^c", fontsize = fs_c );
    zlim([0,1]);
    ax = gca();
    ax[:set_xticklabels]( [] );
    ax[:set_yticklabels]( [] );
    ax[:set_zticklabels]( [] );
    ax_path_lc_cluster = "./Model_fit_results/lanechange_clustering.pdf";
    println("Print to: ", ax_path_lc_cluster);
    savefig(ax_path_lc_cluster);

    # Plot the original data points and the synthesized data points.
    figure();
    ax2 = pyplt[:axes](projection = "3d");
    mylabel = "original";
    ax2[:scatter]( lc_mat[:, 1] - lc_mat[:, 3],
                   lc_mat[:, 2] - lc_mat[:, 3],
                   lc_mat[:, 4], label = mylabel, color="b");
    mylabel = "synthesized";
    ax2[:scatter]( lc_added[:, 1] - lc_added[:, 3],
                   lc_added[:, 2] - lc_added[:, 3],
                   lc_added[:, 4], label = mylabel, color="r");
    legend( loc="upper left", fontsize = fs_c );
    xlabel(L"\Delta x_{21}^c", fontsize = fs_c );
    ylabel(L"\Delta x_{31}^c", fontsize = fs_c );
    zlabel(L"p^c", fontsize = fs_c );
    zlim([0,1]);
    ax = gca();
    ax[:set_xticklabels]( [] );
    ax[:set_yticklabels]( [] );
    ax[:set_zticklabels]( [] );
    ax_path_lc_added = "./Model_fit_results/lanechange_added.pdf";
    println("Print to: ", ax_path_lc_added);
    savefig(ax_path_lc_added);

end

"linear_interpolation(x, y, xq): linearly interpolate (x, y) with xq"
function linear_interpolation(x::Vector{Float64}, y::Vector{Float64}, xq::Vector{Float64})::Vector{Float64}

    # Initialize the interpolated y
    yq = Vector{Float64}( length(xq) );

    # We use a for-loop to get yq
    for i = 1:length(xq)

        # Find the lower x1 and the upper x2 for xq[i]
        lo_ind = findlast( xj -> xj <= xq[i], x );

        if lo_ind == 0
            # We do linear extrapolation on the left.
            lo_ind = 1;
            x1 = x[ lo_ind ];
            y1 = y[ lo_ind ];

            yq[i] = y1;

        else

            hi_ind = findfirst( xj-> xj > xq[i], x);

            if hi_ind == 0
                # We need to do linear extrapolation on the right.
                # The equation of extrapolation on the right is the same as interpolation.
                hi_ind = length(x);
                x2 = x[ hi_ind ];
                y2 = y[ hi_ind ];
                yq[i] = y2;

            else

                x1 = x[ lo_ind ];
                x2 = x[ hi_ind ];

                # Coorresponding y can be find
                y1 = y[ lo_ind ];
                y2 = y[ hi_ind ];

                # Linear interpolation
                yq[i] = ( xq[i] - x1 ) / ( x2 - x1 ) * ( y2 - y1 ) + y1;
            end

        end

    end

    # Return
    yq

end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    make_figures()
end
