
# The information after a triggering event.
mutable struct InfoAfterTrigger
        egoid::Int64                    # The id of the ego vehicle
        triggerframe::Int64             # The frame ath which event-triggering happens
        trigger_ind::Int64              # The n-th trigger
        triggered::Bool                 # If the event is at least triggered once
        laneids::Array{Int64, 1}        # The list of lane ids after the triggering.
        ego_v::Array{Float64, 1}        # The list of speed of the ego vehicle
        fore_v::Array{Float64, 1}       # The list of speed of the front vehicle
        leadids::Array{Int64, 1}        # The list of ids of the lead vehicles
        duration::Int64                 # The duration after event triggering
        InfoAfterTrigger(;egoid::Int64 = 0,
                        triggerframe::Int64 = 0,
                        trigger_ind::Int64 = 0,
                        triggered::Bool = false,
                        laneids::Array{Int64, 1} = Array{Int64, 1}(),
                        ego_v::Array{Float64, 1} = Array{Float64, 1}(),
                        fore_v::Array{Float64, 1} = Array{Float64, 1}(),
                        leadids::Array{Int64, 1} = Array{Int64, 1}(),
                        duration::Int64 = 50) = new(egoid, triggerframe, trigger_ind, triggered, laneids, ego_v, fore_v, leadids, duration)
end

# In this program, I define the following useful constants:
#
# A vehicle appears in the dataset in [start frame, end frame]. We add a margin
# to this, hence we only consider [start_frame + margin, end_frame - margin] for
# this vehicle.
const VEHICLE_IN_VIEW_MARGIN = 50   # unit: frames
#
# When the speed difference is between the following threshold, we think the human
# cannot tell the difference.
const SPEED_DIFF_THRESHOLD = 1.0      # unit m/s

# From here I start to define function.

# """
# get_neighbor_speed gets the speed of the neighbor in the scene.
# """
function get_neighbor_speed(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                            neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult,
                            longitudinal_direction::String;
                            fore_default_speed::Float64 = 25.0,
                            rear_default_speed::Float64 = 0.0)
    # We now compute the speed. If there is a neighbor
    if neighbor.ind != 0    # index in scene of the neighbor
        speed = scene[neighbor.ind].state.v
    # otherwise,
    else
        # If the longitudinal direction is forward, set the speed to be the speed limit 55 mph == 25 m/s
        if longitudinal_direction == "fore"
            speed = fore_default_speed
        elseif longitudinal_direction == "rear"    # Otherwise, let the speed == 0.0 m/s
            speed = rear_default_speed
        else
            error("When get_neighbor_speed, the longitudinal_direction is not valid.")
        end
    end
    speed
end

function get_neighbor_heading(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                              neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult;
                              default_heading::Float64 = 0.0)
    # We now compute the heading. If there is a neighbor
    if neighbor.ind != 0    # index in scene of the neighbor
        heading = scene[neighbor.ind].state.posF.ϕ
    # otherwise,
    else
        heading = default_heading
    end
    heading
end

function get_neighbor_lat_pos(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                              neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult;
                              default_lat_pos::Float64 = 0.0)
    # We now compute the lateral position. If there is a neighbor
    if neighbor.ind != 0    # index in scene of the neighbor
        lat_pos = scene[neighbor.ind].state.posF.t
    # otherwise,
    else
        lat_pos = default_lat_pos
    end
    lat_pos
end


function get_neighbor_id_from_scene(scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                                    neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult,
                                    alternative_id::Int = 0)
    # If there is a neighbor in the scene, we get its id in NGSIM.
    if neighbor.ind != 0
        neighbor_id = scene[neighbor.ind].id;
    # If there is no neighbor, we assume the id is 0.
    else
        neighbor_id = alternative_id;
    end
    neighbor_id
end

# a_sample_neighbor_v_Δs_def is a subfunction of samples_neighbor
function a_sample_of_neighbor!(   speed_vec::Vector{Float64},
                                  distance_vec::Vector{Float64},
                                  heading_vec::Vector{Float64},
                                  lat_pos_vec::Vector{Float64},
                                  scene::Records.Frame{Records.Entity{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64}},
                                  neighbor::AutomotiveDrivingModels.NeighborLongitudinalResult,
                                  longitudinal_direction::String,
                                  original_neighborid::Int)

    # Get the neighbor's id. The function get_neighbor_id_from_scene is defined in this file.
    neighbor_id = get_neighbor_id_from_scene(scene, neighbor);
    # If the neighbor is the original neighbor
    if neighbor_id == original_neighborid
        # We retrieve the distance between the neighbor and the ego and the speed of the neighbor.
        push!( distance_vec, neighbor.Δs );
        push!( speed_vec, get_neighbor_speed(scene, neighbor, longitudinal_direction) );
        push!( heading_vec, get_neighbor_heading(scene, neighbor) );
        push!( lat_pos_vec, get_neighbor_lat_pos(scene, neighbor) );
    # If the neighbor is not the original neighbor, we copy the distance and speed of the original neighbor.
    else
        push!(distance_vec, distance_vec[end]);
        push!(speed_vec, speed_vec[end]);
        push!( heading_vec, heading_vec[end] );
        push!( lat_pos_vec, lat_pos_vec[end] );
    end

    # The vehicle definition.
    if neighbor.ind != 0
        vehicle_def = scene[neighbor.ind].def;
    else
        vehicle_def = VehicleDef();
    end
    # We output the following variables
    (speed_vec, distance_vec, heading_vec, lat_pos_vec, vehicle_def)
end


function samples_neighbor(get_neighbor_in_scene::Function,
                           trajdata::Records.ListRecord{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64},
                           roadway::AutomotiveDrivingModels.Roadway,
                           carid::Int,
                           cameraframe::Int,
                           longitudinal_direction::String;
                           search_distance::Float64 = 100.0,
                           sample_duration::Int = 1)
    # Initialize
    speed_vec = Vector{Float64}()
    distance_vec = Vector{Float64}()
    heading_vec = Vector{Float64}()
    lat_pos_vec = Vector{Float64}()
    # Record the type and dimension of the vehicle.
    vehicle_def = AutomotiveDrivingModels.VehicleDef()

    # We first want to record the information of the neighbor right at cameraframe.
    scene = get!(Scene(500), trajdata, cameraframe);
    # We get the neigbor.
    neighbor = get_neighbor_in_scene(scene, carid, roadway, search_distance);
    # We want to get this neigbor's id.
    original_neighbor_id = get_neighbor_id_from_scene(scene, neighbor);
    # We get new samples of speed and distance.
    (speed_vec, distance_vec, heading_vec, lat_pos_vec, vehicle_def) = a_sample_of_neighbor!(speed_vec, distance_vec,
                                                                                             heading_vec, lat_pos_vec,
                                                                                             scene, neighbor,
                                                                                             longitudinal_direction,
                                                                                             original_neighbor_id);
    # If the sample duration is longer than 1, we keep recording.
    if sample_duration > 1
        for i = 1:(sample_duration)
            # Retrieve a neighbor from the scene.
            scene = get!(Scene(500), trajdata, cameraframe - i);
            neighbor = get_neighbor_in_scene(scene, carid, roadway, search_distance);
            # We get new samples of speed and distance.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec, vehicle_def) = a_sample_of_neighbor!(speed_vec, distance_vec,
                                                                                                     heading_vec, lat_pos_vec,
                                                                                                     scene, neighbor,
                                                                                                     longitudinal_direction,
                                                                                                     original_neighbor_id);
        end
    end
    # The distances now are measured w.r.t the ego vehicle's head.
    # But when the longitudinal direction is rear,  we want to know the distances of the rear vehicle to
    # the rear of the ego. Here we add this offset.
    if longitudinal_direction == "rear"
        # We get the length of the ego along the longitudinal direction.
        ego_ind = get_scene_id(scene, carid);
        len_ego = scene[ego_ind].def.length;
        ϕ_ego = scene[ego_ind].state.posF.ϕ;
        len_cos_ego = len_ego * cos(ϕ_ego);

        # We add the offset
        distance_vec = distance_vec - len_cos_ego;
    end

    # Return:
    (speed_vec, distance_vec, heading_vec, lat_pos_vec, vehicle_def, original_neighbor_id)
end


# ------------------------------------------------------------------------------
# """
# traffic_info_NGSIM gets the relative distance and absolute velocity of the ego or one of its neighbors.
# """
function traffic_info_NGSIM(trajdata::Records.ListRecord{AutomotiveDrivingModels.VehicleState,AutomotiveDrivingModels.VehicleDef,Int64},
                            roadway::AutomotiveDrivingModels.Roadway,
                            carid::Int,
                            cameraframe::Int;
                            longitudinal_direc::String = "self",
                            lateral_direc::String = "middle",
                            sample_duration::Int = 1,
                            aggregate_method::String = "mean")
    # Define a dictionary for traffic info
    traffic_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}()

    # If the vehicle to be observed is the ego self,
    if longitudinal_direc == "self"
        # Initialize
        speed_vec = Vector{Float64}()
        distance_vec = Vector{Float64}()
        heading_vec = Vector{Float64}()
        lat_pos_vec = Vector{Float64}()
        # This record the type and dimension of the vehicle.
        vehicle_def = AutomotiveDrivingModels.VehicleDef()
        vehicle_id = carid
        for i = 1:sample_duration
            scene = get!(Scene(500), trajdata, cameraframe + 1 - i);
            # We get the index of the vehicle (carid) in the scene
            ego_ind = get_scene_id(scene, carid)
            # Let the distance to self be 0
            push!(distance_vec, 0.0);
            # We get the velocity of the ego vehicle in unit [m/s].
            push!(speed_vec, scene[ego_ind].state.v);
            # Let the heading of neigbors to be 0
            push!(heading_vec, scene[ego_ind].state.posF.ϕ)
            # We get the relative heading of the ego in unit rad.
            push!(lat_pos_vec, scene[ego_ind].state.posF.t);
            # The vehicle definition and id
            if i == 1
                vehicle_def = scene[ego_ind].def
            end
        end
    # If the vehicle to be observed is in front,
    elseif longitudinal_direc == "fore"
        # further if the vehicle to be observed shares the lane with the ego
        if lateral_direc == "middle"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor(get_neighbor_fore_along_lane_NGSIM,
                                                         trajdata, roadway, carid, cameraframe,
                                                         longitudinal_direc,
                                                         search_distance = 100.0,
                                                         sample_duration = sample_duration);

        # If the vehicle to be observed is in the left,
        elseif lateral_direc == "left"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor( get_neighbor_fore_along_left_lane_NGSIM,
                                                          trajdata, roadway, carid, cameraframe,
                                                          longitudinal_direc,
                                                          search_distance = 100.0,
                                                          sample_duration = sample_duration);
        # If the vehicle to be observed is on right,
        elseif lateral_direc == "right"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor(   get_neighbor_fore_along_right_lane_NGSIM,
                                                            trajdata, roadway, carid, cameraframe,
                                                            longitudinal_direc,
                                                            search_distance = 100.0,
                                                            sample_duration = sample_duration);
        else
            error("When getting front neighbor, the lateral direction is not valid.")
        end
    # If the vehicle to be observed is at rear,
    elseif longitudinal_direc == "rear"
        # further if the vehicle to be observed is in the middle
        if lateral_direc == "middle"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor(   get_neighbor_rear_along_lane_NGSIM,
                                                            trajdata, roadway, carid, cameraframe,
                                                            longitudinal_direc,
                                                            search_distance = 100.0,
                                                            sample_duration = sample_duration);
        # If the vehicle to be observed is on left
        elseif lateral_direc == "left"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor(   get_neighbor_rear_along_left_lane_NGSIM,
                                                            trajdata, roadway, carid, cameraframe,
                                                            longitudinal_direc,
                                                            search_distance = 100.0,
                                                            sample_duration = sample_duration);
        # If the vehicle to be observed is on the right,
        elseif lateral_direc == "right"
            # Get samples of speed, distance, and vehicle type of the neighbor in this direction.
            (speed_vec, distance_vec, heading_vec, lat_pos_vec,
             vehicle_def, vehicle_id) = samples_neighbor(   get_neighbor_rear_along_right_lane_NGSIM,
                                                            trajdata, roadway, carid, cameraframe,
                                                            longitudinal_direc,
                                                            search_distance = 100.0,
                                                            sample_duration = sample_duration);
        # If the vehicle to be observed is on right,
        else
            error("When getting rear neighbor, the lateral direction is not valid.")
        end # if end

    else
        error("The longitudinal direction is not valid.")
    end # if end
    # Depending on the aggregation method, we use min or mean.
    if aggregate_method == "min"
        # Get minimum values
        speed = minimum(speed_vec)
        distance = minimum(distance_vec)
        heading = minimum(heading_vec)
        lat_pos = minimum(lat_pos_vec)
    else
        # Getting mean values
        speed = mean(speed_vec)
        distance = mean(distance_vec)
        heading = mean(heading_vec)
        lat_pos = mean(lat_pos_vec)
    end
    traffic_info[:speed] = speed
    traffic_info[:distance] = distance
    traffic_info[:vehicle_def] = vehicle_def
    traffic_info[:vehicle_id] = vehicle_id
    traffic_info[:heading] = heading
    traffic_info[:lat_pos] = lat_pos
    traffic_info
end # function end


function get_laneid_from_trajdata_i101(egoid::Int, ego_frame::Int)::Int
    scene = get!(Scene(500), trajdata_my, ego_frame);
    # We get the index of the vehicle (carid) in the scene
    ego_ind = get_scene_id(scene, egoid)
    # We get lane id from the state as follows.
    ego_roadsegment = scene[ego_ind].state.posF.roadind.tag.segment;
    ego_laneid = scene[ego_ind].state.posF.roadind.tag.lane;
    # The following is the information regarding lanes in trajdata_my. Note, tdraw_my uses different lane labelling!
    # In segment 1, 4 the lanes from right to left are labelled as 1, 2, 3, 4, 5.
    # In segment 3, the lanes from right to left are labelled as 2, 3, 4, 5, 6.
    # Lane 1 in segment 3 is the ramp.
    # We do the following modification because of the structure of the lane tags.
    # for more information see https://github.com/LongshengJiang/NGSIM-tools/blob/master/tutorials/NGSIM/US101_roadway_explained.md
    if ego_roadsegment == 3
        ego_laneid -= 1;
    end
    ego_laneid
end

function get_roadsegment_from_trajdata_i101(egoid::Int, ego_frame::Int)::Int
    scene = get!(Scene(500), trajdata_my, ego_frame);
    # We get the index of the vehicle (carid) in the scene
    ego_ind = get_scene_id(scene, egoid)
    # We get lane id from the state as follows.
    ego_roadsegment = scene[ego_ind].state.posF.roadind.tag.segment
end

function front_traffic_info_tentative_to_official(veh1_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                                  veh2_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                                  veh22_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}};
                                                  project_horizon::Float64 = 3.0)

    # If the adjacent front vehicle is about to cut in, treat that the cutting in vehicle as the front vehicle.
    # Tentative veh 1
    veh1_id = veh1_traffic_info[:vehicle_id];
    Δd10 = veh1_traffic_info[:distance];

    # veh 2
    veh2_id = veh2_traffic_info[:vehicle_id];
    Δd20 = veh2_traffic_info[:distance]; # longitudinal relative distance
    s2 = veh2_traffic_info[:lat_pos];   # lateral position
    v2 = veh2_traffic_info[:speed];
    ϕ2 = veh2_traffic_info[:heading];
    v2_lat = v2 * sin(ϕ2);

    Δt = project_horizon;
    s2_proj = s2 + v2_lat * Δt;

    # veh 22
    veh22_id = veh22_traffic_info[:vehicle_id];
    Δd220 = veh22_traffic_info[:distance];  # longitudinal relative distance
    s22 = veh22_traffic_info[:lat_pos];   # lateral position
    v22 = veh22_traffic_info[:speed];
    ϕ22 = veh22_traffic_info[:heading];
    v22_lat = v22 * sin(ϕ22);

    s22_proj = s22 + v22_lat * Δt;

    # If veh2 exists, it is in front of the ego, it is longitudinally
    # closer to the ego, and it is cutting in, we treat veh2 as the official veh 1.
    # Veh2 is the left vehicle. If it cuts in, it is turning right (negative lateral direction).
    if veh2_id != 0 && Δd20 > 1.0 && Δd20 < Δd10 && s2_proj < -1.75
        official_veh1_traffic_info = veh2_traffic_info

    # Use the same logic for veh22.
    # Veh22 is the right vehicle. If it cuts in, it is turning left (positive lateral direction).
    elseif veh22_id != 0 && Δd220 > 1.0 && Δd220 < Δd10 && s22_proj > 1.75
        official_veh1_traffic_info = veh22_traffic_info

    else
        official_veh1_traffic_info = veh1_traffic_info

    end

    # Return
    official_veh1_traffic_info
end


# """
# Choose an observation standard deviation based whether the observed target is
# in the front view or rear view (through mirror) of the ego driver.
# """
function get_observation_std(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                             oth_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}};
                             longitudinal_direction::String = "fore",
                             σ_ω_fore::Float64 = 0.1,   # deg
                             σ_θ_fore::Float64 = 0.2,   # deg
                             σ_ω_rear::Float64 = 0.1,   # deg
                             σ_θ_rear::Float64 = 0.2    # deg
                             )::Tuple{Float64, Float64}

        # We compute the length information of the ego and the target vehicle.
        len_ego = ego_traffic_info[:vehicle_def].length;
        θ_ego = ego_traffic_info[:heading];
        len_oth = oth_traffic_info[:vehicle_def].length;
        θ_oth = oth_traffic_info[:heading];

        # If we are dealing with a fore vehicle
        if longitudinal_direction == "fore"
            # We compute the relative position of the head of the target vehicle.
            Δd = oth_traffic_info[:distance] + len_oth * cos( θ_oth );
        # If we are dealing with a rear vehicle
        elseif longitudinal_direction == "rear"
            # We compute the relative position of the head of the target vehicle.
            Δd = -( oth_traffic_info[:distance] + len_ego * cos( θ_ego ) );
        else
            error("Invalid longitudinal direction. can only be fore or rear.")
        end

        # If the head is in the front view, we use the parameters in the fore direction.
        if Δd > 0
            (σ_ω, σ_θ) = (σ_ω_fore, σ_θ_fore);
        # Otherwise, we use the parameters in the rear direction.
        else
            (σ_ω, σ_θ) = (σ_ω_rear, σ_θ_rear);
        end
    # Return
    (σ_ω, σ_θ)
end

# """
# Adjust the adjacent fore and rear vehicles.
# """
function adjacent_traffic_info_tentative_to_official( tentative_vehf_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                                      tentative_vehr_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                                      ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                                      ego_frame::Int;
                                                      lateral_direction::String = "left",
                                                      Δframe_fore::Int = 6,
                                                      Δframe_rear::Int = 1,
                                                      sample_duration::Int = 1,
                                                      project_horizon::Float64 = 3.0,
                                                      debug::Bool = false)

    # Prepare info
    ego_id = ego_traffic_info[:vehicle_id];

    tentative_vehf_id = tentative_vehf_traffic_info[:vehicle_id];
    tentative_rel_dis_f_e = tentative_vehf_traffic_info[:distance]; # f stands for fore, e stands for ego.
    tentative_rel_speed_f_e = tentative_vehf_traffic_info[:speed] - ego_traffic_info[:speed];

    tentative_vehr_id = tentative_vehr_traffic_info[:vehicle_id];
    tentative_rel_dis_e_r = tentative_vehr_traffic_info[:distance]; # r stands for rear, e stands for ego.
    tentative_rel_speed_e_r = ego_traffic_info[:speed] - tentative_vehr_traffic_info[:speed];

    # We do some projection here.
    proj_rel_dis_f_e = tentative_rel_dis_f_e + tentative_rel_speed_f_e * project_horizon;
    proj_rel_dis_e_r = tentative_rel_dis_e_r + tentative_rel_speed_e_r * project_horizon;

    # We compute the longitudinal length of the vehicles
    len_ego = ego_traffic_info[:vehicle_def].length;
    cos_len_ego = len_ego * cos( ego_traffic_info[:heading] );

    len_vehf = tentative_vehf_traffic_info[:vehicle_def].length;
    cos_len_vehf = len_vehf * cos( tentative_vehf_traffic_info[:heading] );

    len_vehr = tentative_vehr_traffic_info[:vehicle_def].length;
    cos_len_vehr = len_vehr * cos( tentative_vehr_traffic_info[:heading] );

    # If the tentative adjacent front vehicle is being passed by the ego,
    if  tentative_vehf_id != 0 && proj_rel_dis_f_e <= 1.0
        # We want to find the vehicle in front of tentative vehf. That is the official vehf.
        vehf_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, tentative_vehf_id, ego_frame - Δframe_fore,
                                                    longitudinal_direc="fore",
                                                    lateral_direc = "middle", sample_duration = sample_duration,
                                                    aggregate_method = "mean" );
        # The distance should be adjusted considering the length of tentative vehf.
        vehf_traffic_info[:distance] = tentative_rel_dis_f_e + cos_len_vehf + vehf_traffic_info[:distance];
        # The tentative vehf now becomes vehr, where r stands for rear.
        vehr_traffic_info = tentative_vehf_traffic_info;
        # Likewse, the distance should be adjusted to consider the length of the vehr and the ego.
        vehr_traffic_info[:distance] = -( tentative_rel_dis_f_e + cos_len_vehf + cos_len_ego );

    # If the tentative adjacent rear vehicle is passing the ego,
    elseif tentative_vehr_id != 0 && proj_rel_dis_e_r <= -( cos_len_ego + cos_len_vehr + 1.0 )
        # We want to find the vehicle in the back of tenative vehr, That is the official vehr.
        vehr_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, tentative_vehr_id, ego_frame - Δframe_rear,
                                                  longitudinal_direc="rear",
                                                  lateral_direc = "middle", sample_duration = sample_duration,
                                                  aggregate_method = "mean" );
        # The distance should be adjusted considering the length of tentative vehf.
        vehr_traffic_info[:distance] = tentative_rel_dis_e_r + cos_len_vehr + vehr_traffic_info[:distance];
        # The tentative vehf now becomes vehr, where r stands for rear.
        vehf_traffic_info = tentative_vehr_traffic_info;
        # Likewse, the distance should be adjusted to consider the length of the vehr and the ego.
        vehf_traffic_info[:distance] = -( tentative_rel_dis_e_r + cos_len_vehr + cos_len_ego );

    else    # The tentative vehf is indeed in front of the ego. We officially appoint it as vehf
            # and the tentative vehr as vehr.
        # Front vehicle
        # Here is a catch. The tentative vehf is viewed 0.5s ago than the tentative vehr. During this 0.5 s
        # a vehr can surpass the ego and becomes the new vehf. The human driver can see this transition from windows when checking mirrors.
        # We should include this feature here.
        tenative_new_vehf_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, ego_id, ego_frame - Δframe_rear,
                                                            longitudinal_direc="fore",
                                                            lateral_direc = lateral_direction, sample_duration = sample_duration,
                                                            aggregate_method = "mean" );
        # If there is a new tentative front vehicle (vehf), we use the new front vehicle.
        if tenative_new_vehf_traffic_info[:vehicle_id] != tentative_vehf_traffic_info[:vehicle_id]
            vehf_traffic_info = tenative_new_vehf_traffic_info;
        # Otherwise, we use the old tentative front vehicle.
        else
            vehf_traffic_info = tentative_vehf_traffic_info;
        end
        # Rear vehicle:
        vehr_traffic_info = tentative_vehr_traffic_info;
    end

    (vehf_traffic_info, vehr_traffic_info)
end

# """
# Get the average height of a vehicle
# """
function get_vehicle_average_height(veh_def::VehicleDef)::Float64
    # This is a lookup table of the standard heights depends on the type of a vehicle.

    # Create the height dictionary. The heights are in meters.
    vehicle_heights = Dict{Int, Float64}( [ (AgentClass.MOTORCYCLE, 1.5), (AgentClass.CAR, 1.75), (AgentClass.TRUCK, 4.0) ] );
    # Return
    my_veh_height = vehicle_heights[ veh_def.class ]

end

function get_size_scaling_factor(ego_def::VehicleDef, other_def::VehicleDef)::Float64
    # When the ego vehicle is larger, we use the ego's size as we consider the ego's threat to
    # the other part, for instance, a motorcycle.
    # When the other vehicle is larger, we use that vehicle's size as we consider the other vehicle's
    # threat to the ego, for instance, a big truck.
    # We use the intersection area to denote the visual size of a vehicle.

    # Get the average heights of the ego and the other.
    ego_height = get_vehicle_average_height(ego_def);
    other_height = get_vehicle_average_height(other_def);

    # Compute the intersectional size of the ego and the other.
    ego_size = ego_height;  # * ego_traffic_info[:vehicle_def].width;
    other_size = other_height;  # * other_traffic_info[:vehicle_def].width;

    # Compute the size ratio which is the scaling factor.
    # The numerator is the impacting party and the denominator is the impacted party.
    # The scaling factor is >= 1.0
    scaling_factor = maximum( [ ego_size, other_size ] ) / minimum( [ ego_size, other_size ] )
end

function get_comfort_braking(sample_size::Int; lower::Float64 = -3.4, upper::Float64 = -2.01)::Vector{Float64}
    # The parameters are the
    # comfortable braking deceleration extracted from page 53 of
    # Greibe, Poul. "Braking distance, friction and behaviour." Trafitec, Scion-DTU (2007).
    comfort_braking = rand( Uniform(lower, upper), sample_size )  # m/s^2
end
