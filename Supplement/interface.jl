

function ask_use_which_dataset()::String
    # Get to know which dataset is used.
    println("Which is the dataset used? 1 = MyNGSIM1, 2 = MyNGSIM2, or 3 = MyNGSIM3");
    # Let us get user input.
    myngsim = "";
    while true
        myngsim_number = readline();
        if myngsim_number == "1"
            myngsim = "i101_0750_0805";
            break
        elseif myngsim_number == "2"
            myngsim = "i101_0805_0820";
            break
        elseif myngsim_number == "3"
            myngsim = "i101_0820_0835";
            break
        else
            println("Please use integer: 1, 2, or 3.")
        end
    end
    myngsim
end

function ask_which_lane_decision()::String
    # Get to know which dataset is used.
    println("Which is the lane decision to be analyzed? 1 = change, 2 = keep, 3 = abort, or 4 = merge");
    # Let us get user input.
    lanedecision= "";
    while true
        lanedecision_number = readline();
        if lanedecision_number == "1"
            lanedecision = "change";
            break
        elseif lanedecision_number == "2"
            lanedecision = "keep";
            break
        elseif lanedecision_number == "3"
            lanedecision = "abort";
            break
        elseif lanedecision_number == "4"
            lanedecision = "merge";
            break
        else
            println("Please use integer: 1, 2, 3 or 4.")
        end
    end
    lanedecision
end
