

function add_lanedecision_cases!(lanedecision_cases::Dict{Int64,Array{Int64,1}}, egoid::Int, triggerframe::Int)::Dict{Int64,Array{Int64,1}}
    # If the ego is not recorded yet, we record it for the first time
    if !haskey(lanedecision_cases, egoid)
        lanedecision_cases[egoid] = [triggerframe];
    # else, if the ego is already recorded and if the frame is not added, we add the new frame instance.
    elseif lanedecision_cases[egoid][end] != triggerframe
        push!(lanedecision_cases[egoid], triggerframe);
    else    # Otherwise, we don't do anything.
        nothing;
    end
    lanedecision_cases
end

# """
# Determine lane change or lane keep
# """
function lane_change_or_keep!(lanedecision_cases::Tuple{Dict{Int64,Array{Int64,1}},Dict{Int64,Array{Int64,1}},Dict{Int64,Array{Int64,1}}},
                              info_after_trigger::InfoAfterTrigger)
    # Initialize some counters
    Δn_lanechanges = 0
    Δn_lanekeeps = 0
    Δn_abortedchanges = 0
    Δn_notchange = 0
    Δn_slow_lead = 0
    # Unpack the lane decision cases.
    (lanechange_cases, lanekeep_cases, abortchange_cases) = lanedecision_cases;

    # We determine if a lane change happens. A lane change is charaterized by
    # 1) the ego moved to a new lane
    # 2) the ego did not go back to the original lane quickly (abort the lane change)
    #
    # Scan the lane ids to see if they are the same.
    if !isempty(info_after_trigger.laneids)
        aftrg_laneids_len = length(info_after_trigger.laneids);
        aftrg_current_laneid =  info_after_trigger.laneids[1];
        for i = 2:aftrg_laneids_len
            # If we find the lane id is not the same as the first one, that may be a lane change.
            if info_after_trigger.laneids[i] != aftrg_current_laneid
                # We further check if this lane change is aborted
                goingback = false;
                for j = i:minimum([i + 20, aftrg_laneids_len])
                    # If the vehicle goes back to the previous lane, that is an aborted change.
                    goingback = goingback || (info_after_trigger.laneids[j] == aftrg_current_laneid);
                end
                # If the ego goes back to the current lane, the lane change is aborted.
                if goingback
                    # Add one lane-change abortion
                    old_abortchange_cases = deepcopy(abortchange_cases);
                    abortchange_cases = add_lanedecision_cases!(abortchange_cases, info_after_trigger.egoid, info_after_trigger.triggerframe);
                    # If the abortchange_cases is updated, we count one more.
                    if abortchange_cases != old_abortchange_cases
                        Δn_abortedchanges += 1;
                    end
                else
                    # Add one lane-change
                    Δn_lanechanges += 1
                    lanechange_cases = add_lanedecision_cases!(lanechange_cases, info_after_trigger.egoid, info_after_trigger.triggerframe);
                    # Break the loop for (i = 2:aftrg_laneids_len)
                    break
                end
            end
        end
        #
        # We determine if a lane-keeping happens. A lane-keeping is charaterized by
        # 1) The ego did not change lane and the lead is the same.
        # 2) The ego matched the speed of the slow lead vehicle.
        # It is possible that the lead vehicle may speed up quickly, hence the ego
        # aborts the lane change quickly. We do NOT consider this case.
        #
        changinglane = false;
        samelead = true;
        for i = 2:aftrg_laneids_len
            changinglane = changinglane || (info_after_trigger.laneids[i] != info_after_trigger.laneids[1]);
            samelead = samelead && (info_after_trigger.leadids[i] == info_after_trigger.leadids[1]);
        end

        # If the ego did not change lane and if the lead is the same
        if !changinglane && samelead
            Δn_notchange += 1;
            # If the lead vehicle is slowing down,
            # We want to do some averaging.
            avg_range = maximum([1, length(info_after_trigger.fore_v) ÷ 10]);
            fore_v_head = mean(info_after_trigger.fore_v[1:avg_range]);
            fore_v_end = mean(info_after_trigger.fore_v[(end+1-avg_range):end]);
            if fore_v_head - fore_v_end > SPEED_DIFF_THRESHOLD
                Δn_slow_lead += 1;
                # If the relative speed of the lead to the ego is small enough, it is a lane-keeping.
                rel_speed_fore = info_after_trigger.fore_v - info_after_trigger.ego_v;
                if minimum( abs.(rel_speed_fore) ) <= (SPEED_DIFF_THRESHOLD / 2)
                    Δn_lanekeeps += 1;
                    lanekeep_cases = add_lanedecision_cases!(lanekeep_cases, info_after_trigger.egoid, info_after_trigger.triggerframe);

                end
            end
        end
        Δn_lanedecisions= [Δn_lanechanges, Δn_lanekeeps, Δn_abortedchanges, Δn_notchange, Δn_slow_lead]
        # Pack the lane decisions before return.
        lanedecision_cases = (lanechange_cases, lanekeep_cases, abortchange_cases);
        return (lanedecision_cases, Δn_lanedecisions)
    else
        error("The laneids vector is empty.");
    end
end


# """
#   Modify the tuple (true acceleration vector, perceived acceleration vector)
# """
function update_acceleration_vectors!( veh_acc_vectors::Tuple{ Vector{Float64}, Vector{Float64}},
                                       veh_v_vec::Vector{Float64};
                                       acc_threshold::Float64 = -0.5,
                                       acc_min::Float64 = -2.0)
    # Retrieve the acceleration vectors.
    veh_a_vec  = veh_acc_vectors[1];
    veh_inferred_a_vec = veh_acc_vectors[2];

    # Let compute the acceleration of veh1 from its speed sequence.
    if length(veh_v_vec) > 1
        veh_a = ( veh_v_vec[end] - veh_v_vec[end-1] ) / 0.1;
        push!(veh_a_vec, veh_a);

         # Let us determine if the brake light is on.
         if veh_a <= acc_threshold
             # We want to compute the acceleration.
             # We use the following heuristic rule:
             #      If the brake light is on for longer time, the acceleration is larger.
             # Let us first determine how long the brake light has been on.
             last_lightoff = findlast( a-> a>acc_threshold, veh_a_vec );
             duration_lighton = length( veh_a_vec ) - last_lightoff;
             @assert duration_lighton >= 1 "duration_lighton must be greater than 0."
             # The braking acceleration is proportional to the light-on duration.
             # The parameters are approximated derived from the results of case change76_477 and change140_413
             # in i101-0750-0805 dataset.
             # We lower bound the inferred acceleration to a_min.
             veh_inferred_a = maximum( [ ( 0.05 * acc_min * duration_lighton ), acc_min ] );
             push!(veh_inferred_a_vec, veh_inferred_a);
         else
             push!(veh_inferred_a_vec, 0.0);
         end
    else
        # For initialize the two acceleration vectors at the beginning.
        push!(veh_a_vec, 0.0);
        push!(veh_inferred_a_vec, 0.0);
    end
    (veh_a_vec, veh_inferred_a_vec)
end

# Find when the neighbor switched.
function get_when_neighbor_changed(frames::Vector{Int64}, neighbors::Vector{Int64})::Vector{Int64}

    @assert length(frames) == length(neighbors) "the lengths of the frames and neigbors are not equal"

    neighborchange_frames = Vector{Int64}();
    push!(neighborchange_frames, frames[1]);    # Default change at the beginning.
    for i = 2:length(neighbors)
        if neighbors[i] != neighbors[i-1]
            push!(neighborchange_frames, frames[i]);
        end
    end
    # Return
    neighborchange_frames
end

function lowpass_filter(signal::Vector; stride::Int64 = 1, width::Int64 = 10, α::Float64 = 0.3)::Vector

    # Initializing
    filtered_signal = Vector();
    push!(filtered_signal, signal[1]);

    # filtering
   for i = 2:length(signal)

       if i % stride == 0
           working_width = minimum( [ i ÷ stride, width] ) - 1;
           first_stride = stride;
       else
           working_width = minimum( [ i ÷ stride, width - 1] );
           first_stride = i % stride;
       end
       @assert first_stride > 0 "The first stride is negative"

       # First stride
        filtered_point = signal[ i - working_width * stride - first_stride + 1];
        filtered_point = filtered_point * (1-α) + signal[ i - working_width * stride ] * α;
        # The remaining strides
       for j = -working_width:0
           filtered_point = filtered_point * (1-α) + signal[i + j * stride ] * α;
       end
       push!(filtered_signal, filtered_point)
   end
   @assert length(signal)==length(filtered_signal) "Length of the filtered signal is different from that of the original signal."
   # Return
   filtered_signal
end

function get_decisionpoint_candidates(prob_seg::Vector{Float64}, prob_max::Float64; spike_width::Int64 = 2, pmargin::Float64 = 0.05)::Vector{Int64}

    # Short notation
    psg = prob_seg;
    pmi = prob_max - pmargin; # prob_max_initial
    pm = pmi;   # prob_max
    w = spike_width;

    # Get the qualified indices in the vector segment.
    qii = find( p-> p >= pmi, psg );   # qualified_ind_init
    qi  = qii;  # qualified_ind

    # obsolete{
    # # We expand the search threshold if the qualified indices are too small hence may be a spike.
    # while length(qi) <= w
    #     # Lowering the threshold
    #     pm -= pmargin;
    #     qi = find( p-> p >= pm, psg );
    # end
    #
    # # We need to decide whether to exclude the initial indices.
    # # If the initial indices point to a spike, we need to exclude them.
    # # Otherwise, we do not need to exclude them.
    # if length(qii) <= w && pmi - pm > 0.5
    #     ei = qii;    # exclude_ind
    # else
    #     ei = Vector{Int64}();
    # end
    # # Apply exclusion
    # qi = collect( setdiff( Set(qi), Set(ei) ) );
    # obsolete}

    # Return
    qualified_ind = qi
end

# """
# Refine the chosen decision point in a small vicinity.
# """
function refine_decision_point( raw_decisionpoint::Int64,
                                prob_safe_left_vec::Vector{Float64},
                                prob_safe_right_vec::Vector{Float64},
                                cost_left_fore_vec::Vector{Float64},
                                cost_current_lane_vec::Vector{Float64},
                                cost_right_fore_vec::Vector{Float64},
                                isleft::Bool;
                                pmargin::Float64 = 0.05,
                                screen_half_window::Int = 10 )::Int64
    # Refine the decision point by searching in its vicinity.
    d_point = raw_decisionpoint;
    # Probabilities
    p_l =  prob_safe_left_vec;
    p_r =  prob_safe_right_vec;
    # Radius
    s_w = screen_half_window;
    # Margin

    # obsolete{
    # m_top = minimum( [ (length( p_l ) - d_point), s_w ]);
    # }

    m_bot = minimum( [ (d_point - 1), s_w ] );
    # Segments of probability vectors

    # obsolete{
    # p_l_seg = p_l[ (d_point - m_bot):(d_point + m_top) ];
    # p_r_seg = p_r[ (d_point - m_bot):(d_point + m_top) ];
    # }

    p_l_seg = p_l[ (d_point - m_bot):d_point ];
    p_r_seg = p_r[ (d_point - m_bot):d_point ];
    # Maximal probabilities
    p_l_max = maximum( p_l_seg );
    p_r_max = maximum( p_r_seg );
    # Costs and cost differences
    c_l = cost_left_fore_vec;
    c_c = cost_current_lane_vec;
    c_r = cost_right_fore_vec;
    Δc_l = c_l - c_c;
    Δc_r = c_r - c_c;

    # Do a quick search for the final decision point
    if isleft       # Considering the left lane
        # Search the left side only.
        # Get the qualified indices in the vector segment.
        qualified_ind = get_decisionpoint_candidates(p_l_seg, p_l_max);
        if isempty( qualified_ind )
            qualified_ind = length( p_l_seg );
        end
        # Transfer to the orginal vector
        qualified_ind += (d_point - m_bot) - 1;
        # Search for maximizing the cost difference.
        decisionpoint = qualified_ind[1];
        for ind in qualified_ind
            if Δc_l[ ind ] > Δc_l[ decisionpoint ]
                decisionpoint = ind;
            end
        end

    else    # Considering the right lane
        # Search the right side only.
        # Get the qualified indices in the vector segment.
        qualified_ind = get_decisionpoint_candidates(p_r_seg, p_r_max);
        if isempty( qualified_ind )
            qualified_ind = length( p_r_seg );
        end
        # Transfer to the orginal vector
        qualified_ind += (d_point - m_bot) - 1;
        # Search for maximizing the cost difference.
        decisionpoint = qualified_ind[1];
        for ind in qualified_ind
            if Δc_r[ ind ] > Δc_r[ decisionpoint ]
                decisionpoint = ind;
            end
        end
    end

    # Return
    decisionpoint
end

#"""
# Take the sequence of lateral position of the ego before going into another lane,
# return the index of the lane changing starting point and the decision-making point.
#"""
function get_changedecision_and_turning_point(lateral_position_sequence::Vector{Float64},
                                              prob_safe_left_vec::Vector{Float64},
                                              prob_safe_right_vec::Vector{Float64},
                                              cost_left_fore_vec::Vector{Float64},
                                              cost_current_lane_vec::Vector{Float64},
                                              cost_right_fore_vec::Vector{Float64};
                                              tolerance_v::Float64 = 0.35 )::Tuple{Int, Int}
    # Let us compute the slope (lateral velocity) of the position sequence.
    pos = lateral_position_sequence;    # Use sort notation.
    v = [ ( pos[i] - pos[i-1] ) / 0.1 for i=2:length(pos) ];
    # padding 0 to v because it is one element shorter than pos.
    v = vcat( [0], v );
    @assert length(v) == length(pos) "lengths of v and pos must be equal"

    # Let us compute the initial lateral position as the average of the first 0.5 second.
    pos_init = mean( pos[1:5] );

    # Let us start the search from the end of the v.
    ind = length(v);  # Do not forget initialize ind.
    decision2action = 6;
    # We first determine the turning direction by checking the last element in pos.
    if pos[end] > 0
        # We check the condition on the left half lane.
        for ind = ( length(v) - 5 ):-1:(decision2action + 1)
            if v[ind] < tolerance_v || pos[ind] <= 0.0
                break   # break the for loop
            end
        end

    else
        # We check the condition on the right half lane.
        for ind = ( length(v) - 5 ):-1:(decision2action + 1)
            if  v[ind] > -tolerance_v || pos[ind] >= 0.0
                break   # break the for loop
            end
        end

    end

    # The ind is the index of the turning start point.
    t_point = ind;  # tentative turn point
    # We know the decision point is 0.6s before the start point.
    d_point = ind - decision2action;    # tentative decision point

    # Define the flag to choose either the left or right lane for consideration.
    consider_left = pos[end] > 0;
    # Get the refined decisionpoint
    decisionpoint = refine_decision_point(d_point, prob_safe_left_vec, prob_safe_right_vec,
                                          cost_left_fore_vec, cost_current_lane_vec, cost_right_fore_vec,
                                          consider_left, screen_half_window = 15);

    turnpoint = minimum( [decisionpoint + decision2action, length(pos)] );

    # Return
    (decisionpoint, turnpoint)

end


function get_keepdecision_and_braking_point(ego_accelerations::Vector{Float64},
                                            prob_safe_left_vec::Vector{Float64},
                                            prob_safe_right_vec::Vector{Float64},
                                            cost_left_fore_vec::Vector{Float64},
                                            cost_current_lane_vec::Vector{Float64},
                                            cost_right_fore_vec::Vector{Float64};
                                            threshold::Float64 = -0.5)::Tuple{Int, Int}

    a0s = ego_accelerations;    # use short notation

    # Search from the end.
    decision2action = 6;
    ind = 11;    # Do not forget initialize ind, otherwise no value is returned from the for-loop
    for ind = 11:1:( length(a0s)- 10 )

        if a0s[ ind - 1 ] > threshold && a0s[ ind + 1 ] < threshold && a0s[ ind - 10 ] > threshold
            break
        end
    end
    # The raw braking point and decision point.
    b_point = ind;
    d_point = b_point - decision2action;
    # Define the flag to choose either the left or right lane for consideration.
    c_l = cost_left_fore_vec;
    c_r = cost_right_fore_vec;
    consider_left = c_l[ d_point ] >= c_r[ d_point];
    # Get the refined decisionpoint
    decision_point = refine_decision_point(d_point, prob_safe_left_vec, prob_safe_right_vec,
                                           cost_left_fore_vec, cost_current_lane_vec, cost_right_fore_vec,
                                           consider_left, screen_half_window = 5);
    braking_point = minimum( [decision_point + decision2action, length(a0s)] );

    # Return
    (decision_point, braking_point)

end

# """
# Make sure the frame range always contains the vehicle.
# """
function proper_frame_range(range_margins::Tuple{Int, Int}, carid::Int, frame::Int)::Tuple{Int, Int}
    # We make sure the frames do not exceed the frame range of the car.
    # Get the number of the preceding and remaining frames.
    car_start_row = tdraw_my.car2start[carid]
    car_start_frame = tdraw_my.df[car_start_row, :frame]
    n_prec_frames = frame - car_start_frame

    # Get the margins
    (b_margin, a_margin) = range_margins;
    # If the before_margin is greater than the number of preceding frames, replace it
    # with the number of the preceding frames.
    if b_margin > n_prec_frames
        b_margin = n_prec_frames
    end
    n_total_frames = tdraw_my.df[car_start_row, :n_frames_in_dataset]
    remain_frames = n_total_frames - n_prec_frames - 1
    # If the after_margin is larger than the remaining frames, use the remaining frames
    # as the after_margin.
    if a_margin > remain_frames
        a_margin = remain_frames
    end
    # Return the proper before_margin and after_margin.
    (b_margin, a_margin)
end
