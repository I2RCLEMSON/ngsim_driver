using DataFrames
using CSV
using JLD
using Distributions
using PyPlot

"""
Linear interpolation: Given a series (x, y), find yq in (xq, yq) for each xq.
"""
function linear_interpolation(x::Vector{Float64}, y::Vector{Float64}, xq::Vector{Float64})::Vector{Float64}

    # Initialize the interpolated y
    yq = Vector{Float64}( length(xq) );

    # We use a for-loop to get yq
    for i = 1:length(xq)

        # Find the lower x1 and the upper x2 for xq[i]
        lo_ind = findlast( xj -> xj <= xq[i], x );

        if lo_ind == 0
            # We do linear extrapolation on the left.
            lo_ind = 1;
            x1 = x[ lo_ind ];
            y1 = y[ lo_ind ];

            yq[i] = y1;

        else

            hi_ind = findfirst( xj-> xj > xq[i], x);

            if hi_ind == 0
                # We need to do linear extrapolation on the right.
                # The equation of extrapolation on the right is the same as interpolation.
                hi_ind = length(x);
                x2 = x[ hi_ind ];
                y2 = y[ hi_ind ];
                yq[i] = y2;

            else

                x1 = x[ lo_ind ];
                x2 = x[ hi_ind ];

                # Coorresponding y can be find
                y1 = y[ lo_ind ];
                y2 = y[ hi_ind ];

                # Linear interpolation
                yq[i] = ( xq[i] - x1 ) / ( x2 - x1 ) * ( y2 - y1 ) + y1;
            end

        end

    end

    # Return
    yq

end

"""
Obtain the ROC curve from the groundtruth and the netadvantage (prediction)
"""
function get_roc_curve( groundtruth::Vector{Float64}, netadv::Vector{Float64} )

    # Use short notation
    y = groundtruth;

    # Choose variable thresholds in the range of the net advantage for computing false positive rate vs. true positive rate.
    thrld_vec = linspace( minimum( netadv ), maximum( netadv ), length(netadv));
    # Find the indices of the actual positive and negative.
    truechange_inds = find(d -> d == 1, y);
    truekeep_inds = find(d -> d == 0, y);

    # Initialize the points on the ROC curve.
    points = Matrix{Float64}(2, 0);

    for thr in thrld_vec

        tp = length( find( e-> e >= thr, netadv[ truechange_inds ] ) );
        fn = length( find( e-> e < thr, netadv[ truechange_inds ] ) );
        tn = length( find( e-> e < thr, netadv[ truekeep_inds ] ) );
        fp = length( find( e-> e >= thr, netadv[ truekeep_inds ] ) );
        # Get true positive rate and false positive rate.
        tpr = tp / (tp + fn);
        fpr = fp / (tn + fp);

        # Add this point
        points = hcat(points, [fpr, tpr] );

    end

    # Sort the points hence fpr is ascending.
    sortedpoints = sortcols( points, lt=lexless );

    # Rectify the roc curve to have standard x-coordinates
    rec_fpr = collect( linspace( 0.0, 1.0, 100 ) );
    rec_tpr = linear_interpolation( sortedpoints[1,:], sortedpoints[2,:], rec_fpr );

    # Return
    rec_points = vcat( rec_fpr', rec_tpr' )

end

"""
Compute the area-under-curve (auc) for the ROC curve
"""
function get_roc_auc(roc_points::Matrix{Float64}  )

    # short notation
    points = roc_points;

    # Approximate the area under the roc curve.
    x_vec = points[ 1, : ];
    y_vec = points[ 2, : ];
    # Shift x vector one position for computing the bases for integration.
    x_left_vec = x_vec[1:end-1];
    x_right_vec = x_vec[2:end];
    bases= x_right_vec - x_left_vec; # length(base) = length(x) - 1.

    # Shift y vector one position for computing the heights for integration.
    y_left_vec = y_vec[1:end-1];
    y_right_vec = y_vec[2:end];
    heights = ( y_left_vec + y_right_vec ) ./ 2.0;

    # Integrate to get the area.
    area = sum( bases .* heights )

end

"""
Add means and standard deviation to DataFrame assuming the columns are numerical.
"""
function compute_mean_std(df::DataFrame)::DataFrame

    # Compute the mean and std
    colnames = names(df);

    means = Vector{Union{String, Float64}}();
    stds  = Vector{Union{String, Float64}}();
    for name in colnames

        push!( means, mean( df[name] ) );
        push!( stds, std( df[name] ) );

    end

    # Create a new data frame.
    colnames = vcat([:note], colnames);
    # Initialize the empty traindata
    stat_df = DataFrame([ col = Any[] for i = 1:length(colnames) ], colnames);
    # Adding explanation of mean and std
    means = vcat(["mean"], means);
    stds  = vcat(["std"], stds);
    # Update the data frame.
    push!(stat_df, means);
    push!(stat_df, stds);

    #return
    stat_df
end

"Hold out a share of original data for testing, the rest are used for training and validating"
function split_trainvalidate_test( rsk_lc_df::DataFrame,
                                   nrsk_lc_df::DataFrame,
                                   lk_df::DataFrame,
                                   clusters::Array{ Array{Int64, 1}, 1 },
                                   clu_sparsities::Vector{Float64},  # cluster sparsities
                                   clu_popus::Vector{Int64},  # cluster populations
                                   clu_vols::Vector{Float64}; # cluster volumns
                                   display::Bool = false )

    # Convert data frames to matrix for each manipulation
    rsk_lc_mat = Matrix{Float64}( rsk_lc_df );
    nrsk_lc_mat = Matrix{Float64}( nrsk_lc_df );
    lk_mat = Matrix{Float64}( lk_df );

    # ======================================================================
    # Lane-change data
    # ======================================================================

    # Compute information about the clusters
    # Note the clusters contains the indices of rows of rsk_lc_mat.
    k_clusters = length( clusters );
    max_spa_ind = indmax( clu_sparsities );
    undersam_sizes = [ round(Int64, clu_vols[i] * clu_popus[max_spa_ind] / clu_vols[max_spa_ind]  )  for i = 1:k_clusters ];
    # Make sure there are enough data in each cluster
    undersam_sizes = [ maximum( [ undersam_sizes[i], 5 ] ) for i = 1:k_clusters ];

    # Hold out a share of original data for final testing.
    # Initialize a container for lane-changing testing data.
    lc_test = Matrix{Float64}(0, size(rsk_lc_mat, 2) );
    lc_train_valid = Matrix{Float64}(0, size(rsk_lc_mat, 2) );

    for ind_cluster = 1:length(clusters)

        # datapoints (dp)
        dp = rsk_lc_mat[ clusters[ ind_cluster ], : ];

        # Get the testing data
        # target_ratio (tro)
        tro_lc = undersam_sizes[ ind_cluster ] / size( dp, 1);
        ( dp_test, excluded_inds_rsklc ) = random_undersample( dp, target_ratio = tro_lc );
        # Save the data for testing
        lc_test = vcat( lc_test, dp_test );

        # Get the traning and validation data
        if !isempty( excluded_inds_rsklc )
            dp_train_valid = dp[ excluded_inds_rsklc, : ];
        else
            dp_train_valid = Matrix{Float64}(0, size(rsk_lc_mat, 2) );
        end
        # Save the rest points for training and testing.
        lc_train_valid = vcat( lc_train_valid, dp_train_valid );

    end

    # We also need to sample data points for training in non-risk-seeking cases.
    # We match the size of non risk-seeking with risk-seeking
    size_nrsk = size(lc_test, 1);
    # Using undersampling to get the test dataset
    tro_lc = size_nrsk / size(nrsk_lc_mat, 1);
    ( nrsk_lc_test, excluded_inds_nrsklc ) = random_undersample( nrsk_lc_mat, target_ratio = tro_lc );
    # Save the data for testing
    lc_test = vcat( lc_test, nrsk_lc_test );
    size_lc_test = size( lc_test, 1 );

    # Get the traning and validation data
    if !isempty( excluded_inds_nrsklc )
        nrsk_lc_train_valid = nrsk_lc_mat[ excluded_inds_nrsklc , : ];
    else
        nrsk_lc_train_valid = Matrix{Float64}(0, size(nrsk_lc_mat, 2) );
    end
    # Save the rest points for training and testing.
    lc_train_valid = vcat( lc_train_valid, nrsk_lc_train_valid );

    # ======================================================================
    # Lane-keeping data
    # ======================================================================
    # Hold out a share of original data for final testing.
    size_lk_test = size_lc_test;
    tro_lk = size_lk_test / size( lk_mat, 1 );
    # Random undersampling
    ( lk_test, excluded_inds_lk ) = random_undersample( lk_mat, target_ratio=tro_lk);

    # Get the traning and validation data
    if !isempty( excluded_inds_lk )
        lk_train_valid = lk_mat[ excluded_inds_lk , : ];
    else
        lk_train_valid = Matrix{Float64}(0, size(lk_mat, 2) );
    end

    trainvalidate_mat = vcat( lc_train_valid, lk_train_valid );
    test_mat = vcat( lc_test, lk_test );

    # Plot undersampled data
    if display
        figure();
        ax = pyplt[:axes](projection = "3d");
        ax[:scatter]( lc_test[:, 1] - lc_test[:, 3],
                      lc_test[:, 2] - lc_test[:, 3],
                      lc_test[:, 4], label = "lane-change");
        ax[:scatter]( lk_test[:, 1] - lk_test[:, 3],
                      lk_test[:, 2] - lk_test[:, 3],
                      lk_test[:, 4], label = "lane-keep");
        legend();
        xlabel("v_change - v_keep");
        ylabel("c_collision - v_keep");
        zlabel("p_safe");
        zlim([0,1]);
    end

    # Convert the matrices to dataframes
    df_names = [:c_change, :c_collision, :c_keep, :p_safe, :decision, :subj_v, :adj_lead_v, :adj_lead_rd, :adj_lag_v, :adj_lag_rd ];
    trainvalidate_df = DataFrame( trainvalidate_mat, df_names );
    test_df = DataFrame( test_mat, df_names );

    # Return
    ( trainvalidate_df, test_df )
end

"Random undersampling"
function random_undersample(dataset::Matrix{Float64}; target_ratio::Float64=0.8)

    # We randomly select a row of the dataset and exclude this row.
    n_org_rows = size(dataset, 1);
    n_target_rows = floor( Int, n_org_rows * target_ratio );
    n_deleterows = n_org_rows - n_target_rows;

    # Randomly select the distinct indices of the rows to be deleted.
    deleterow_inds = sample( 1:n_org_rows, n_deleterows, replace=false );
    # The new matrix excludes the selected rows.
    targetrow_inds = setdiff(1:n_org_rows, deleterow_inds);
    new_dataset = dataset[ targetrow_inds, : ];

    # Return
    (new_dataset, deleterow_inds)

end
