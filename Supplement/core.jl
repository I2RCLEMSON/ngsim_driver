# The labeling of vehicles is in the following diagram.
#-------------------------------------------------------------------------------
#             ==> veh 3                                  ==> veh 2
#-------------------------------------------------------------------------------
#  ==> veh 4                ==> veh 0 (ego)               ==> veh 1
#-------------------------------------------------------------------------------
#          ==> veh 32                                  ==> veh 22
#-------------------------------------------------------------------------------


# To run this program, the prerequsite modules are

# Get to know which dataset is used.
println("Which is the dataset used? 1 = MyNGSIM1, 2 = MyNGSIM2, or 3 = MyNGSIM3");
# Let us get user input.
while true
    myngsim_number = readline();
    if myngsim_number == "1"
        using MyNGSIM1
        println("Using MyNGSIM1");
        break
    elseif myngsim_number == "2"
        using MyNGSIM2
        println("Using MyNGSIM2");
        break
    elseif myngsim_number == "3"
        using MyNGSIM3
        println("Using MyNGSIM3");
        break
    else
        println("Please use integer: 1, 2, or 3.")
    end
end

using JLD
println("Using JLD");
using Reel
println("Using Reel");
# using AutoViz
# println("Using AutoViz");
using PyPlot
println("Using Pyplot");
using CSV
println("Using CSV");
using DataFrames
println("Using DataFrames");
using Distributions
println("Using Distributions");

# include files.
include("data.jl");
include("interface.jl");
include("process.jl");


# ==============================================================================
# Rewards/costs-related
# ==============================================================================

function get_lane_admissible_speeds(v_ego::Float64,
                                    veh1_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                    veh2_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                    veh22_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}};
                                    size_scaling_1::Float64 = 1.0,
                                    size_scaling_2::Float64 = 1.0,
                                    size_scaling_22::Float64 = 1.0,
                                    project_horizon::Float64 = 3.0)::Vector{Float64}
    # Unpack the variables here
    # veh 1
    Δd_1_0 = veh1_perceived_info[:Δd];
    Δv_1_0 = veh1_perceived_info[:Δv];
    a_1    = veh1_perceived_info[:a];
    # veh 2
    Δd_2_0 = veh2_perceived_info[:Δd];
    Δv_2_0 = veh2_perceived_info[:Δv];
    a_2    = veh2_perceived_info[:a];
    # veh 22
    Δd_22_0 = veh22_perceived_info[:Δd];
    Δv_22_0 = veh22_perceived_info[:Δv];
    a_22    = veh22_perceived_info[:a];

    # Short notation
    γ1 = size_scaling_1;
    γ2 = size_scaling_2;
    γ22 = size_scaling_22;
    Δt = project_horizon;

    # Create an iterator to store (Δd, Δv)
    front_traffic = [ (Δd_1_0, Δv_1_0, a_1, γ1),
                      (Δd_2_0, Δv_2_0, a_2, γ2),
                      (Δd_22_0, Δv_22_0, a_22, γ22) ];
    # Define the threshold for being considered as free flow.
    Δd_freeflow = 50.0;   # m
    # Speed limit of the road
    v_speedlimit = 25.0;  # m/s
    # Power
    n = 3.0;

    # We compute the speed that are possible for each lane: current, left, and right.
    v_lane_vec = Vector();
    for (Δd, Δv, a, γ) in front_traffic    # γ is for future use.
        # Speed of the leading vehicle
        v_lead = maximum( [ ( v_ego + Δv + a * Δt ), 0.0 ] );
        # A blending ratio
        α = minimum( [ maximum( [ Δd /( Δd_freeflow ), 0.0 ] ), 1.0 ] );
        # The possible speed in the lane, which is
        #    lane speed = lead vehicle speed + bias due to empty space.
        v_lane = v_lead + α ^ n * (v_speedlimit - v_lead);
        push!(v_lane_vec, v_lane);
    end
    # The admissible speeds of the currect, left, and right lanes.
    v_lane_vec
end

# """
# Compute the threating cost of the possible collision with the adjacent rear vehicle.
# """
function get_collision_threat_cost(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                   veh3_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                   Δv_0_3::Float64)::Float64
    # Cost scaling factor. This is not a density.
    ρ = 1.0;
    # We want to compute the size ratio of the adjacent rear vehicle and the ego.
    # This ratio is used to scale the threat.
    size_ratio = get_size_scaling_factor(ego_traffic_info[:vehicle_def], veh3_traffic_info[:vehicle_def]);
    # Let us get perceived speed of the adjacent rear vehicle. It is the mean speed.
    v_3 = maximum( [ (ego_traffic_info[:speed] -  Δv_0_3), 0.0] );  # >= 0
    # When the ego (veh0) is faster than the adjacent rear vehicle (veh3), there is
    # no threat. That is, when Δv_0_3 > 0, threat = 0.
    Δv_0_3_neg = minimum( [ Δv_0_3, 0.0 ] );    # ≦ 0

    # We now compute the threat cost
    threat_cost = ρ * size_ratio * v_3 * Δv_0_3_neg;    # This cost ≦ 0.
end

# ==============================================================================
# Probability-related
# ==============================================================================

# """
#   Give the true physical relative distance and speed, and return the human
#   perceived relative distance and speed.
# """
function perception_model(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                          observed_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}};
                          longitudinal_direction::String = "rear",
                          Δv_prior_fore::Float64 = 0.0, # m/s
                          Δv_prior_rear::Float64 = 0.0, # m/s
                          σ_Δv_prior::Float64 = 2.0,
                          σ_ω_p::Float64 = 0.4,
                          σ_Δd_prior::Float64 = 50.0,
                          σ_θ_p::Float64 = 0.4,
                          γ_ω::Float64 = 1.0,
                          γ_θ::Float64 = 1.0,
                          debug::Bool = false)::Tuple{Float64, Float64, Float64, Float64}

    # We first get the relative distance and relative speed between the ego and the observed.
    Δd = observed_traffic_info[:distance];
    # We adjust the relative distance such that it is measured with reference to where the driver
    # locates, which is assumed to be in 1/3 car length from the car head.
    # Also, we get the relative speed according to
    #   relative speed = speed of the lead - speed of the follower.
    if longitudinal_direction == "rear"
        # In this case the lead is the ego car.
        Δd = Δd + 2.0 / 3.0 * ego_traffic_info[:vehicle_def].length;
        Δv = ego_traffic_info[:speed] - observed_traffic_info[:speed];

    elseif longitudinal_direction == "fore"
        # In this case the follower is the ego car.
        Δd = Δd + 1.0 / 3.0 * ego_traffic_info[:vehicle_def].length;
        Δv = observed_traffic_info[:speed] - ego_traffic_info[:speed];
    else
        error("The longitudinal_direction is not proper in ( perception_model ).");
    end

    # We need to deal with some special cases when Δd < 0.0
    Δd_initial = Δd;
    Δv_initial = Δv;
    if Δd_initial < 0.0
        # We switch the case to the opposite direction with proper treatment of the signs.
        Δd = -Δd;
        Δv = -Δv;
        Δv_prior_fore = -Δv_prior_fore;
        Δv_prior_rear = -Δv_prior_rear;
    end

    # Let us first model how humans estimate scene distance using looming signal.
    # Let θ be the angle subtended by the target vehicle. We have
    #   tan( θ ) = w / Δd,
    # where w is the width of the observed vehicle.
    # Transfer this angle to the perception domain with
    #   θ_p = log( γ_θ * θ + 1),
    # Combine the two above equation, and let the observed vehicle width be
    w_obs = observed_traffic_info[:vehicle_def].width;
    # and let the radian to degree factor be
    r2d = 180.0 / pi;
    # we get
    θ_p = log( γ_θ * r2d * atan( w_obs / Δd ) + 1 );  # (deg)
    # where, γ_θ, w_obs, and Δd >= 0.
    # We assume the measurement of θ_p is subject to Gaussian noise with constant
    # standard deviation σ_θ_p.
    # We now simulate a measurement of θ_p.
    # θ_p_likeli_distr = Normal( θ_p, σ_θ_p );
    # θ_p_m = rand(θ_p_likeli_distr, 1)[1];
    #  For analysis purpose, we let θ_p_m = θ_p
    θ_p_m = θ_p;
    # In reality, we might use
    #           θ_p_likeli_distr = Normal( θ_p, σ_θ_p );
    #           θ_p_m = rand(θ_p_likeli_distr, 1)[1];

    # We assume the human mind has an internal world representation, which maps
    # the perceived visual angles back to physical distance.
    Δd_m = w_obs / tan( 1.0 / ( γ_θ * r2d ) * ( exp( θ_p_m ) - 1 ) );

    # Although Δd_m is log-normal, for computational simplicity, we assume it is
    # Normal. But we need to compute the width of its distribution by using the
    # width of the distribution of θ_p.
    # We use some approximation here.
    #   θ ≈ r2d * w / Δd,
    #   θ_p = log( γ_θ * θ + 1),
    # Hence,
    #   Δd ≈ γ_θ * r2d * w / ( exp( θ_p ) - 1 )
    # We want to use the standard deviation σ_θ_p around θ_p_m to compute the
    # the standard deviation σ_Δd_m around Δd_m.
    # From the above three equations, we can derive
    #                                               1
    #   σ_Δd_m = Δd_m * ( 1 - ------------------------------------------------------ )
    #                                               Δd_m
    #                       exp(σ_θ_p) +   ---------------------( exp(σ_θ_p) -1)
    #                                      γ_θ * r2d * w_obs
    σ_Δd_m = Δd_m * ( 1.0 - 1.0 / ( exp( σ_θ_p ) + Δd_m / ( γ_θ * r2d * w_obs ) * ( exp( σ_θ_p ) - 1.0 ) ) );
    # As seen, as Δd_m increase, the σ_Δd_m grows more than linearly.

    # For computational complicity, we assume the distance in the internal world representation follows
    # Gaussian distribution:
    #   N(Δd_m, σ_Δd_m)
    # We also need to consider the biasing due to the distance prior which centers on zero and has sd as
    # σ_Δd_prior. Here, we let the st to be relative large.
    # This posterior distribution after applying Bayes' rule has
    mean_Δd_post = Δd_m * ( 1.0 - σ_Δd_m ^ 2 / ( σ_Δd_m ^ 2 + σ_Δd_prior ^ 2 ) );
    σ_Δd_post = sqrt( σ_Δd_m ^ 2 * σ_Δd_prior ^ 2 / ( σ_Δd_m ^ 2 + σ_Δd_prior ^ 2 ) );

    # Now we deal with the speed perception and internal representation .
    # Human visual perception of motion is based on the optical flow of the angles subtended by the moving object.
    # Hence, we first transfer the follower's speed to the angular speed of the subtended angle.
    c_v2ω = w_obs / ( Δd ^ 2.0 + w_obs ^ 2.0);  # c_v2ω > 0
    # The angular speed is
    #   ω = -1.0 * r2d * c_v2ω * Δv;     (deg/s)                                (1)
    # Transfer ω to ω_p in the perception domain using,
    #   ω_p = sign( ω ) * log( γ_θ * |ω| + 1).   (deg/s)                            (2)
    # Since r2d >0 and c_v2ω > 0, we also have
    #   sign(ω) = sign(ω_p) = - sign(Δv)                                            (3)
    # Combine Eqn. (1)--(3), we get
    ω_p = -sign( Δv ) * log( γ_ω * r2d * c_v2ω * abs(Δv) + 1 );                #(4)
    # This ω_p is the true one.

    # We assume the likelihood of the measurement is Gaussion and sample one measurement.
    #  For analysis purpose, we let ω_p_m = ω_p
    ω_p_m = ω_p;
    # In reality, we might use
    #       ω_p_likeli_distr = Normal( ω_p,  σ_ω_p );
    #       ω_p_m = rand(ω_p_likeli_distr, 1)[1];
    #       The true ω_p is within the Gaussion distribution N( ω_p_m, σ_ω_p ).

    # Now we want to transfer the perceived angular speed ω_p_m back to the linear speed in the
    # internal representation. Note in the internal world representation the only available distance
    # is assumed to be mean_Δd_post. Hence,
    c_v2ω_i = w_obs / ( mean_Δd_post ^ 2.0 + w_obs ^ 2.0);
    # c_v2ω_i is used to transfer ω_p_m to Δv_m
    Δv_m = -sign( ω_p_m ) * 1 / (γ_ω * r2d * c_v2ω_i) * ( exp( abs(ω_p_m) ) - 1 );         #(5)
    # Combine Eqn. (3)--(5), we can also derive the standard deviation of the linear speed as
    σ_Δv_m = ( abs(Δv_m) + 1 / (γ_ω * r2d * c_v2ω_i) ) * ( exp( σ_ω_p ) - 1 );
    # where σ_Δv > 0

    # We assume the distribution of Δv is Gaussion. We also assume the prior distribution of
    # Δv is also Gaussion. To facilitate baising to negative speed, we let the prior distribution be
    #   Δv_prior < = 0
    #   N(Δv_prior, σ_Δv_prior^2)
    # If the driver thinks about the rear vehicle, the prior knowledge is that the vehicle is approaching.
    if longitudinal_direction == "rear"
        Δv_prior = Δv_prior_rear;
    # If the driver thinks about the fore vehicle, the prior knowledge is that the vehicle is stationary.
    else
        Δv_prior= Δv_prior_fore;
    end

    # Hence, we can have the closed form of the posterior distribution which is also a Gaussion as follows.
    mean_Δv_post = ( Δv_prior * σ_Δv_m ^ 2 + Δv_m * σ_Δv_prior ^ 2) / ( σ_Δv_m ^ 2 + σ_Δv_prior ^ 2 );
    σ_Δv_post = sqrt( σ_Δv_m ^ 2 * σ_Δv_prior ^ 2 / ( σ_Δv_m ^ 2 + σ_Δv_prior ^ 2 ) );

    # # test code{
    # println("σ_θ_p ", σ_θ_p)
    # println("Δd_m ", Δd_m)
    # println("σ_Δd_m ", σ_Δd_m)
    # println("mean_Δd_post ", mean_Δd_post)
    # println("σ_Δd_post ", σ_Δd_post)
    # println("σ_ω_p ", σ_ω_p)
    # println("σ_Δv_m ", σ_Δv_m)
    # println("Δv_m ", Δv_m )
    # println("mean_Δv_post ", mean_Δv_post)
    # println(" ")
    # #}

    # Lastly, don't forget at the beginning of this function, we transformed the distance and speed such that
    # the distances are all positive. It made our use of trigonomery a bit easier. Now before we exit this
    # function, we need to tranform them back.
    if Δd_initial < 0.0
        Δd = -Δd;
        Δv = -Δv;
        mean_Δd_post = -mean_Δd_post;
        mean_Δv_post = -mean_Δv_post;
    end

    # Return
    (mean_Δd_post, σ_Δd_post, mean_Δv_post, σ_Δv_post)
end

# """
#  Get the minimal time before a necessary evasion.
# """
function time_to_avoid_front_vehicle(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                     fore_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                     ego_acc_range::Vector{Float64},
                                     fore_acceleration::Float64;
                                     operation::String = "lane-keep",
                                     targetlane::String = "same-lane",
                                     net_safe_margin_f::Float64 = 1.0,
                                     sample_size::Int64 = 100,
                                     σ_ω_p::Float64 = 0.15,
                                     σ_θ_p::Float64 = 0.15,
                                     debug::Bool = false)::Tuple{Float64, Float64}

    # Get the perceived information of the current front vehicle (veh1).
    Δd_10_mean = fore_perceived_info[:Δd];
    σ_Δd_10 = fore_perceived_info[:σ_Δd];
    Δv_10_mean = fore_perceived_info[:Δv];
    σ_Δv_10 = fore_perceived_info[:σ_Δv];

    # Let us create samples of Δd_10 and Δv_10. (Monte Carlo)
    Δd_10_distr = Normal( Δd_10_mean, σ_Δd_10 );
    # Let us sample some particles from this distribution.
    Δd_10_vec = rand(Δd_10_distr, sample_size);
    # We do the same thing for the relative speed.
    Δv_10_distr = Normal( Δv_10_mean, σ_Δv_10 );
    Δv_10_vec = rand(Δv_10_distr, sample_size);


    # Safety margin. Being below this safety margin means a collision.
    scale01 = get_size_scaling_factor( ego_traffic_info[:vehicle_def], fore_perceived_info[:vehicle_def] );
    evade_timeheadway = 0.0;

    # Let's consider the driver's location in the ego vehicle.
    safe_margin_f = net_safe_margin_f + 1/3 * ego_traffic_info[:vehicle_def].length;
    # Let us compute the safety margin for both the lane change and lane keep cases.
    if operation == "lane-change"
        # The safety distance considers the sizes of veh0 and veh1, the time headway of veh0,
        # and a minimal distance.
        Δd_safe = scale01 * ( safe_margin_f + ego_traffic_info[:speed] * evade_timeheadway );
        Δd_safe_vec = ones(sample_size) * Δd_safe;

    elseif operation == "lane-keep"
        # We first compute the necessary distance needed for the ego to decelerate without collision.
        # In the deceleration process, the ego vehicle must decelerate more than veh1.

        # obsolete{
        # if targetlane == "same-lane"
        # }

        # Assume the braking deceleration of vehicle 0.
        a0_brake_vec = get_comfort_braking(sample_size);
        Δa10_brake_vec = fore_acceleration - a0_brake_vec; # Δa10_brake_vec > 0.

        # obsolete{
        # elseif targetlane == "adjacent-lane"
        #     # The driver does not want the ego to decelerate heavily after lane changing, hence
        #     Δa10_brake_vec = 0.1 * ones(sample_size);
        # else
        #     error("The targetlane must be same-lane or adjacent-lane.")
        # end
        # }

        # When Δv_10>0, veh1 is faster than the ego, the ego does not need to decelerate.
        # Here we calculate the necessary deceleration distance.
        Δv_10_neg_vec = [ minimum( [Δv, 0.0] ) for Δv in Δv_10_vec];
        Δd_deceleration_vec = Δv_10_neg_vec .^ 2.0 ./ ( 2 .* Δa10_brake_vec);

        # We compute the total safety distance.
        # The safety distance considers the sizes of veh0 and veh1, the time headway of veh0,
        # a minimal distance, and the deceleration distance.
        Δd_safe_vec = scale01 .* ( safe_margin_f + ego_traffic_info[:speed] * evade_timeheadway + Δd_deceleration_vec );

    else
        error("Wrong operation name. Can only be lane-change or lane-keep");
    end

    # Get the acceleration difference
    a0_vec = rand( Uniform( ego_acc_range[1], ego_acc_range[2] ), sample_size );
    Δa_10_vec = fore_acceleration - a0_vec;

    # We want to find the time when operation is eiligible and when the ego has to
    # evade the current lane (or slow down in the current lane).
    Δt_admissible_mat = Matrix{Float64}(2, 0);  # Each column [Δt_evade, Δt_open ]
    # Use a big number to represent infinity. This trick can avoid some indefinity of using Inf.
    bignumber = 100.0;
    # Main loop
    for ind = 1: sample_size,
        # Use short notations
        Δd_10 = Δd_10_vec[ ind ];
        Δv_10 = Δv_10_vec[ ind ];
        Δa_10 = Δa_10_vec[ ind ];
        Δd_safe = Δd_safe_vec[ ind ];

        # The following is the main logic.
        # If the observed Δd < Δd_safe, it means the ego (veh0) must evade immediately.
        if targetlane == "same-lane" && Δd_10 < Δd_safe  # Case 1.
            Δt_close = 0.0;
            Δt_open = 0.0;
        else    # Δd_10 >= Δd_safe
            # If the accelerations are the same, the gap Δd_10 is closed linearly.
            if Δa_10 == 0;
                # Further if Δv_10<0, the gap is closing.
                if Δv_10 < 0
                    Δt_close =  maximum( [ ( Δd_safe - Δd_10 ) / Δv_10, 0.0 ] );
                    Δt_open = 0.0;
                # Else, the gap is opening. Δt_close is infinity.
                else            # Case 3.
                    Δt_close = bignumber;
                    Δt_open = maximum( [ ( Δd_safe - Δd_10 ) / Δv_10, 0.0 ] );
                end
            # If Δa_10<0, the gap Δd_10 is closed quadratically.
            elseif Δa_10 < 0;   # Case 4
                if Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) < 0
                    Δt_close = 0.0;
                    Δt_open  = 0.0;
                else
                    Δt_close = maximum( [ ( -Δv_10 - sqrt( Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) ) ) / Δa_10, 0.0 ] ); # right root
                    Δt_open = maximum( [ ( -Δv_10 + sqrt( Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) ) ) / Δa_10, 0.0 ] );  # left root
                    # # test code{
                    # println("Δt_close Δt_open ", ( Δt_close, Δt_open))
                    # println("Δv_10, Δa_10,  Δd_10 - Δd_safe",(Δv_10, Δa_10,  Δd_10 - Δd_safe))
                    # #}
                end

            # If Δa_10>0,
            else
                # If the apex is out of the safety margin, the veh1 will not intrude the safety margin of the ego.
                if  Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) < 0    # Case 5.
                    Δt_close = bignumber;
                    Δt_open = 0.0;
                else
                    Δt_close = ( -Δv_10 - sqrt( Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) ) ) / Δa_10;
                    # If the target lane is the current lane, the other vehicle is not allowed to enter the safety margin.
                    if targetlane == "same-lane"    # Under this condition, we have Δd_10 >= Δd_safe.
                        if Δt_close > 0.0
                            Δt_close = Δt_close;
                            Δt_open = 0.0;
                        else
                            Δt_close = bignumber;
                            Δt_open = 0.0;
                        end

                    # Otherwise, the target lane is the adjacent lane, the other vehicle is allowed to enter the safety margin.
                    else
                        if Δt_close > 3.0
                            Δt_close = Δt_close;
                            Δt_open = 0.0;
                        else
                            Δt_close = bignumber;
                            Δt_open = maximum( [ ( -Δv_10 + sqrt( Δv_10 ^ 2.0 - 2 * Δa_10 * ( Δd_10 - Δd_safe ) ) ) / Δa_10, 0.0 ] );
                        end
                    end
                end
            end
        end
        # Save this entry to the matrix
        Δt_admissible_mat = hcat(Δt_admissible_mat, [Δt_close, Δt_open])
        @assert Δt_close >= 0.0 && Δt_open >= 0.0 " Δt_close and Δt_open are nonnegative."
    end

    # Here we determine the final Δt_admissible according to the 25 quantile of Δt_evade
    # Sorting the matrix according to the first row.
    Δt_admissible_mat = Δt_admissible_mat[ : ,  sortperm( Δt_admissible_mat[1,:] )];

    # We want to use the box plot on Δt_evade_vec to determine the final choice of the times.
    # Compute the boxplot related terms.
    sorted_Δt_close_vec = Δt_admissible_mat[1,:];
    q25_ind = length( sorted_Δt_close_vec ) ÷ 4;

    # Return
    ( Δt_admissible_mat[1, q25_ind], Δt_admissible_mat[2, q25_ind] )
end

# """
# Use the lateral position, longitudinal speed, and longitudinal acceleration to
# compute the time to move from the current lane to the target lane.
# """
function time_to_change_lane(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                             ego_acc_range::Vector{Float64};
                             lateral_direction::String = "left",
                             lanewidth::Float64 = 1.75, # meter
                             averageheading::Float64 = 0.09, # rad
                             )::Float64
    # Get the current lateral position.
    ego_lat_pos = ego_traffic_info[:lat_pos];
    # The actual time should be propotional to the lateral position of the ego.
    if lateral_direction == "left"
        s = abs( lanewidth - ego_lat_pos );
    elseif lateral_direction == "right"
        s = abs( lanewidth + ego_lat_pos );
    else
        error("lateral direction is not valid; must be left or right")
    end
    # Get the acceleration as the maximum in the acceleration range. , m/s^2
    a = ego_acc_range[2];

    # Define the approximated heading of the ego during lane changing.
    θ = averageheading;   # rad
    # Retrieve the longitudinal speed of the ego.
    v = maximum( [ ego_traffic_info[:speed], 0.0 ] );  # m/s
    # Comfortable lateral transition time:
    Δt_lat_tran = ( -v + sqrt( v ^ 2 + 2 * a * ( s / θ ) ) ) / a

end

function get_scan_time( adjrear_distance::Float64; scan_operation_time::Float64 = 1.0 )::Float64

    # If veh3 or veh32 is in the front of the ego, the ego driver does not need to look back,
    # the reaction is faster.
    if adjrear_distance < 0.0
        Δt_scan_react = 0.0;   # (s) = mental operation delay
    # Otherwise, the driver needs to look back.
    else
        Δt_scan_react = scan_operation_time;
    end
    # Return
    Δt_scan_react
end


function noisy_newton_collision_model(ego_traffic_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                      veh1_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                      veh2_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                      veh3_perceived_info::Dict{Symbol, Union{Float64, Int, VehicleDef}},
                                      ego_target_speed::Float64,
                                      veh1_acceleration::Float64,
                                      veh2_acceleration::Float64,
                                      obs_std_fore_cur::Tuple{Float64, Float64},
                                      obs_std_fore_adj::Tuple{Float64, Float64};
                                      lateral_direction::String = "left",
                                      sample_size::Int = 100,
                                      net_safe_margin_r::Float64 = 2.0,
                                      debug::Bool = false)::Float64

    # Let us first get the statistics of the observed distance and speed of the observed vehicles as they are
    # represented in the ego driver's internal world representation.

    mean_Δd_03 = veh3_perceived_info[:Δd];
    σ_Δd_03 = veh3_perceived_info[:σ_Δd];
    mean_Δv_03 = veh3_perceived_info[:Δv];
    σ_Δv_03 = veh3_perceived_info[:σ_Δv];

    # We want to get the time after which the ego vehicle must decelerate in the current lane in
    # order to not let veh1 enters its safety range.
    # We assume the ego driver does not change speed while she scans the adjacent traffic.
    a0_scan = [0.001, 0.01];
    # The time threshold to start deceleration
    a1 = veh1_acceleration;     # Use short notation.
    # ( Δt_brake_cur, _ ) = time_to_avoid_front_vehicle(ego_traffic_info,     # The current lane is always open
    #                                                   veh1_perceived_info,
    #                                                   a0_scan, a1,
    #                                                   operation = "lane-keep",
    #                                                   targetlane = "same-lane",
    #                                                   σ_ω_p = obs_std_fore_cur[1],
    #                                                   σ_θ_p = obs_std_fore_cur[2]); # second

    # Forced evading:
    # We can also use randomize a0 in the following simulation.
    # We assume the upper bound of a0 is proportional to the the distance between the ego and veh2.
    # We bound the upper bound of a0 in [0.01, 2.0]
    veh2_Δd_proj = veh2_perceived_info[ :Δd ] + veh2_perceived_info[ :Δv ] * 3.0;   # projection horizon = 3 s.
    a0_upbound = minimum( [ maximum( [ 0.1 * veh2_Δd_proj, 0.01 ] ), 2.0 ] );
        # a0_upbound = 1.0;
    # This equation is computed according to the trajectory of case change37-752 generated by MyNGSIM2.
    a0_range = [ 0.001, a0_upbound ];

    # We want to get the time after which the ego vehicle must leave the current lane in
    # order not to let veh1 enters its safety range.
    ( Δt_evade_cur, _ ) = time_to_avoid_front_vehicle(ego_traffic_info,   # The current lane is always open
                                                      veh1_perceived_info,
                                                      a0_range, a1,
                                                      operation = "lane-change",
                                                      targetlane = "same-lane",
                                                      σ_ω_p = obs_std_fore_cur[1],
                                                      σ_θ_p = obs_std_fore_cur[2] ); # second

    # The upper bound of the time allowed for veh0 to accelerate
    # Let also consider the adjacent front vehicle. The adjacent front vehicle should not intrude
    # the ego's front safety margin.
    a2 = veh2_acceleration;   # m/s^2
    ( Δt_brake_adj, Δt_open_adj ) = time_to_avoid_front_vehicle(  ego_traffic_info,
                                                                  veh2_perceived_info,
                                                                  a0_range, a2,
                                                                  operation = "lane-keep",
                                                                  targetlane = "adjacent-lane",
                                                                  net_safe_margin_f = 0.0,
                                                                  σ_ω_p = obs_std_fore_adj[1],
                                                                  σ_θ_p = obs_std_fore_adj[2] ); # second

    # We want to know the required lateral transition time.
    comfort_heading = 0.05;
    sharp_heading = 0.10;
    evade_heading = 0.175;
    Δt_com_lat_tran = time_to_change_lane(ego_traffic_info, a0_range,
                                          lateral_direction = lateral_direction, averageheading = comfort_heading);
    Δt_sharp_lat_tran = time_to_change_lane(ego_traffic_info, a0_range,
                                          lateral_direction = lateral_direction, averageheading = sharp_heading);
    Δt_evade_lat_tran = time_to_change_lane(ego_traffic_info, a0_range,
                                          lateral_direction = lateral_direction, averageheading = evade_heading);


     # # test code{
     # println("a0_range ", a0_range)
     # println("Δt_brake_adj <= Δt_open_adj ", Δt_brake_adj <= Δt_open_adj)
     # println("Δt_com_lat_tran <= Δt_open_adj ", Δt_com_lat_tran <= Δt_open_adj)
     # println("Δt_sharp_lat_tran >= Δt_brake_adj ", Δt_sharp_lat_tran >= Δt_brake_adj)
     # println("Δt_evade_cur <= Δt_open_adj ", Δt_evade_cur<= Δt_open_adj)
     # println("Δt_brake_adj ", Δt_brake_adj)
     # println("Δt_open_adj ", Δt_open_adj)
     # println("Δt_com_lat_tran ", Δt_com_lat_tran)
     # println("Δt_sharp_lat_tran ", Δt_sharp_lat_tran)
     # println("Δt_evade_cur ", Δt_evade_cur)
     # #}

    # Check the upper and lower bound. We make sure the road is NOT blocked to lane-changing.
    # Δt_scan = get_scan_time( veh3_perceived_info[:Δd] );
    # In the following conditions, the driver considers the adjacent lane as not safe:
    #   1. There is not time to scan the adjacent lane: Δt_brake_cur <= Δt_scan
    #   2. The adjacent lane never open: Δt_brake_adj <= Δt_open_adj
    #   3. The adjacent lane will not open yet if starting lane-changing now: Δt_com_lat_tran <= Δt_open_adj
    #   4. The adjacent lane will be closed even if starting sharplane-changing now:  Δt_sharp_lat_tran >= Δt_brake_adj
    #   5. The adjacent lane is not open when the ego is forced to evade the current lane: Δt_evade_cur <= Δt_open_adj
    #   6. The ego hits the front vehicle before moving out of the current lane: Δt_evade_lat_tran >= Δt_evade_cur
    if ( Δt_brake_adj <= Δt_open_adj || Δt_com_lat_tran <= Δt_open_adj || Δt_sharp_lat_tran >= Δt_brake_adj ||   # Δt_brake_cur <= Δt_scan ||
         Δt_evade_cur <= Δt_open_adj || Δt_evade_lat_tran >= Δt_evade_cur )
        prob_safe = 0.0;

    else
        Δt_upper = minimum( [ Δt_evade_cur + 0.001, Δt_brake_adj, 10.001] );
        # We set an upper bound 10.0 to the simulation, since the simulation will not be accurate for longer simulation time.
        Δt_lower = minimum( [ Δt_sharp_lat_tran, Δt_evade_cur, 10.0]);
        # The simulation time of veh0 is
        Δt_sim_distr = Uniform(Δt_lower, Δt_upper);
        Δt_sim_vec = rand( Δt_sim_distr, sample_size );

        # We now use the time to simulate the distance traveled by veh0 and veh3.
        # The moved distance of veh0 is
        v0 = ego_traffic_info[:speed];
        # Generate the acceleration particles.
        a0_vec = rand( Uniform( a0_range[1], a0_range[2]), sample_size );
        # Projected distance that will be traveled by the ego.
        s0_vec = v0 .* Δt_sim_vec + 1/2 .* a0_vec .* Δt_sim_vec .^ 2.0;

        # The moved distance of veh3 is (using perceived particles).
        # We first want to construct the posterior Gaussian distribution of the perceived and internalized
        # distance.
        Δd_03_distr = Normal( mean_Δd_03, σ_Δd_03 );
        # Let us sample some particles from this distribution.
        Δd_03_vec = rand(Δd_03_distr, sample_size);
        # We do the same thing for the relative speed. Construct posterior distribution and sample particles.
        Δv_03_distr = Normal( mean_Δv_03, σ_Δv_03 );
        Δv_03_vec = rand(Δv_03_distr, sample_size);
        # The moved speed of veh3 or veh32:
        v3_vec = v0 - Δv_03_vec;
        # We also add a little mental shaking for veh3 (veh32)
        # The parameters are derived from page 53 of
        # Greibe, Poul. "Braking distance, friction and behaviour." Trafitec, Scion-DTU (2007).
        a3_vec = rand( Normal(0, 1.7), sample_size );
        # We now simulate the distance traveled by veh3 (veh32)
        Δt3_vec = Δt_sim_vec;
        s3_vec = v3_vec .* Δt3_vec + 1/2 .* a3_vec .* Δt3_vec .^ 2;

        # The projected (simulated) relative distance between veh0 and veh3 is
        Δd_03_proj_vec = Δd_03_vec + s0_vec - s3_vec;

        # The minimal required distance.
        Δd_safe_min = net_safe_margin_r + 2/3 * ego_traffic_info[:vehicle_def].length;
        # We compute the distance required by veh3 to decelerate to safe speed.
        Δv_03_neg_vec = [ minimum( [Δv, 0.0] ) for Δv in Δv_03_vec ];
        # Assume the braking deceleration of vehicle 3.
        a3_brake = get_comfort_braking(sample_size);
        Δa03_dece = a0_vec - a3_brake; # m/s^2    The assumed deceleration difference.
        Δd_deceleration03_vec = Δv_03_neg_vec .^ 2.0 ./ ( 2 .* Δa03_dece );

        # The total safe distance.
        Δd_safe_vec = Δd_safe_min + Δd_deceleration03_vec;

        # Get the safe particles
        dis_error_vec = Δd_03_proj_vec - Δd_safe_vec;
        safe_particles = find( x -> x > 0.0, dis_error_vec);
        # We now can compute the empirical probability of
        prob_safe = length(safe_particles) / sample_size;
    end

    # Return:
    prob_safe
end

"""
Compute the probability of lane changing and net advantage using RTx
"""
function get_lanechange_prob_RTx( cost_adjacent_fore::Vector{Float64}, cost_adjacent_rear::Vector{Float64},
                                  cost_current_lane::Vector{Float64},  prob_safe::Vector{Float64},
                                  model_parameters::Vector{Float64} )

    # Use short notation
    γ = model_parameters;

    # Feature data, these are vectors
    x1 = cost_adjacent_fore;
    x2 = cost_adjacent_rear;
    x3 = cost_current_lane;
    x4 = prob_safe;
    x4 = [ minimum( [ maximum([ x4[i], 0.01 ]), 0.99 ] ) for i = 1:length(x4) ];

    # Compute the net advantage of RTx
    # The following short notations are vectors.
    γx1 = γ[3] .* ( x1 - x3 + γ[6] );
    γx2 = γ[3] .* ( γ[7] .* x2 - x3 );
    q1 = γ[1] .* ( x1 - x3 + γ[6] ) + γ[2] .* sinh.( γx1 );
    q2 = γ[1] .* ( γ[7] .* x2 - x3 ) + γ[2] .* sinh.( γx2 );
    w = γ[8] + γ[9] .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );
    # Use the above short notations, we have the RTx net advantage signal.
    e_rtx = w .* ( q1 - q2 ) + q2; # This is a vector.
    prob_change =  1 ./ ( 1 + exp.( -e_rtx ) );

    # Return
    ( prob_change, e_rtx )
end

"""
Compute the probability of lane changing using Toledo's Gap acceptance model
"""
function get_lanechange_prob_toledo( ego_v::Vector{Float64},
                                     adj_fore_v::Vector{Float64}, adj_fore_Δd::Vector{Float64},
                                     adj_rear_v::Vector{Float64}, adj_rear_Δd::Vector{Float64},
                                     model_parameters::Vector{Float64};
                                     project_horizon::Float64 = 3.0 )

    # This is the implementation of the gap acceptance model in
    #   [1] Toledo, Tomer, and Romina Katz. "State dependence in lane-changing models."
    #       Transportation research record 2124.1 (2009): 81-88.

    # Use short notations
    # Retrieve data
    v0 = ego_v;
    v2 = adj_fore_v;
    Δd2 = adj_fore_Δd;
    v3 = adj_rear_v;
    Δd3 = adj_rear_Δd;
    # Parameters
    β = model_parameters;
    @assert length(β) == 9 "Length of vector β must be 9."
    νₙ = rand( Normal() );   # Individual specific term

    # Compute the projected lead gap (ld_g) and lag gap (lg_g)
    # Relative speeds
    Δv20 = v2 - v0;
    Δv30 = v3 - v0;

    # Projected actual gaps ( preview based planning )
    proj_h = project_horizon;   # second

    ld_g = Δd2 + Δv20 * proj_h; # lead gap after proj_h
    ld_g = [ maximum([ gap, 1e-323 ]) for gap in ld_g ]; # lower bounded ld_g

    lg_g = Δd3 - Δv30 * proj_h; # lag gap after proj_h
    lg_g = [ maximum([ gap, 1e-323 ]) for gap in lg_g ]; # lower bounded lg_g

    # Prepare the explanatory variables in GA.
    x1 = [ maximum( [0, v] ) for v in Δv20 ]; # lower bounded by 0
    x2 = [ minimum( [0, v] ) for v in Δv20 ]; # upper bounded by 0
    x3 = [ maximum( [0, v] ) for v in Δv30 ]; # lower bounded by 0

    # Compute the difference between log-gap and the mean critical log gap
    Δlog_ld = log.( ld_g ) - ( β[1] + β[2] .* x1 + β[3] .* x2 + β[4] * νₙ );
    Δlog_lg = log.( lg_g ) - ( β[6] + β[7] .* x3 + β[8] * νₙ );

    # Compute P( lead gap > critical lead gap )
    Φ_ld = cdf.( Normal(0, 1.0),  Δlog_ld ./ β[5] );
    # Compute P( lag gap > critical lag gap )
    Φ_lg = cdf.( Normal(0, 1.0),  Δlog_lg ./ β[9] );

    # Compute probability of choosing lane-change
    p = Φ_ld .* Φ_lg;
    # To avoid computation rounding error, we bound the probability in [0.01, 0.99]
    p = [  minimum( [ maximum( [p_i, 0.01] ), 0.99 ] ) for p_i in p ];

    # Return
    p

end
