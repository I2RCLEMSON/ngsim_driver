include("./Supplement/core.jl");

# """
# We analyze in detail the extracted lane change cases.
# """
function lanedecision_detailed_analysis()

    # User command is specified here.
    ############################################################################
    analyze_subject = ask_which_lane_decision();
    plotting = true;
    data_extracting = false;
    debugging = false;
    howmany_cases = 10000;
    ############################################################################
    println("plotting is ", plotting);
    println("data extracting is ", data_extracting);
    println("debugging is ", debugging);

    # Define the range of frames to analyze.
    AFTER_MARGIN = 50;
    BEFORE_MARGIN = 50;
    SAMPLE_DURATION = 1;
    range_margins = (BEFORE_MARGIN, AFTER_MARGIN);
    COLLISION_WITH_WALL_COST = -100.0;

    # Loading the saved data. This requires package JLD.
    whichdataset = ask_use_which_dataset();
    filepath = "./Data/lanedecision_cases_"*whichdataset*".jld"
    println("loaded file: ", filepath);
    (change_cases, keep_cases, abort_cases) = JLD.load(filepath, "lanedecision_cases");

    if analyze_subject == "change"
        println("Analyzing change cases");
        cases = change_cases;
        if whichdataset == "i101_0750_0805"
            cases_to_exclude = [ (704, 1956), (645, 1818), (2056, 6187), (590, 1737), (2408, 7180)];
            # (704, 1956), (590, 1737) is not in lane.
            # (645, 1818), (2056, 6187) are multi lane change
            # (2408, 7180) is pushed by the side vehicle.
            white_list = [ ];

            if debugging
                white_list = [ (1612, 4841)];
            end

        elseif whichdataset == "i101_0805_0820"
            cases_to_exclude = [(341, 1944), (1017, 4646), (1221, 5127), (1950, 7756), (920, 4375), (1957,7759)];
            # (341, 1944) is not in lane and it is multi-lane change.
            # (1017, 4646), (1950, 7756) have the adjacent neighbor turning away.
            # (1221, 5127) has too many front vehicle changes.
            #  (920, 4375) does not find the change decision point.
            # (1957,7759) has inaccurate decision point.
            white_list = [ ];

            if debugging
                white_list = [ (291, 1951) ];
            end

        elseif whichdataset == "i101_0820_0835"
            cases_to_exclude = [ (1453, 7420), (1768, 8725), (1136, 5415), ];
            # (1453, 7420) causing errors
            # (1768, 8725) is mulit-lane-change
            # (1136, 5415) takes too long to change lane, hence cannot determine when
            # the final decision is made.
            # (849, 4485) is mulit-lane-change

            white_list = [];
            if debugging
                white_list = [ (665, 3620)]#, (848, 4514) ];
            end
        else
            cases_to_exclude = [ ];
            white_list = [ ];
        end
    elseif analyze_subject == "keep"
        println("Analyzing keep cases");
        cases = keep_cases;
        cases_to_exclude = [(2826, 8215), (2732, 8439), (1388, 3845), (1231, 3533),
                            (946, 2412), (399, 1037), (1231, 3533), (1663, 4762),
                            (1673, 4791), (1688, 4835), (2053, 5909), (2648, 7868),
                            (2826, 8215), (990, 2513), (1011, 2577) ];
        # (2826, 8215), (2732, 8439), (399, 1037) has significant signal loss.
        # (1388, 3845) is a lane change.
        # (1231, 3533) has neighbors not in lane.
        # (946, 2412) is yielding to the merging vehicle.
        # (1231, 3533) has a close neighbor not in lane and is not seen by the ego.
        # (1663, 4762) has important signal loss.
        # (1673, 4791) has new adjacent rear neighbor cutting in from the second adjacent lane.
        # (1688, 4835) has important signal loss.
        # (2053, 5909) has important signal loss.
        # (2648, 7868) has new adjacent rear neighbor cutting in from the second adjacent lane.
        # (2826, 8215) has important signal loss.
        # (2872, 8500) is aborted lane change
        # (990, 2513) has important signal loss; missing neighbors.
        # (1011, 2577) has important signal loss; missing neighbors.

        white_list = [ ];

        if debugging
            white_list = [ (1970, 5857) ];
        end

    elseif analyze_subject == "abort"
        println("Analyzing abort cases");
        cases = abort_cases;
        cases_to_exclude = [ ];
        white_list = [ ];
    else
        error("Wrong subject name. Use change, keep or abort");
    end

    total_keys = collect( keys(cases) );
    howmany_cases = minimum( [ howmany_cases, length(total_keys) ] );
    wanted_keys = total_keys[1:howmany_cases];

    # Initialize variables for extracted data
    if data_extracting

        car_id_vec = Vector{Int64}();
        re_frame_vec = Vector{Int64}();     # recorded frame
        de_frame_vec = Vector{Int64}();     # decision-making frame
        cost_keep_vec = Vector{Float64}();
        cost_change_vec = Vector{Float64}();
        cost_collision_vec = Vector{Float64}();
        prob_safe_vec = Vector{Float64}();
        subj_v_vec = Vector{Float64}();    # subjective vehicle speed
        adj_lead_v_vec = Vector{Float64}(); # adjacent lead vehicle speed
        adj_lead_rd_vec = Vector{Float64}(); # adjacent lead relative distance
        adj_lag_v_vec = Vector{Float64}(); # adjacent lag vehicle speed
        adj_lag_rd_vec = Vector{Float64}(); # adjacent lag vehicle relative distance
        decision_vec = Vector{Int64}(); # 1 for lane change, 0 for lane keep

    end

    # We do the following analysis for each recorded vehicle,
    for egoid in wanted_keys # cases is of type Dict{Int, Vector{Int}}
        # Now for each recorded frame we want to do the following analysis.
        for recorded_frame in cases[egoid]
            # First, we want to skip the following cases, because the manual review found they are not proper.
            if (egoid, recorded_frame) in cases_to_exclude
                nothing
            else
                if isempty( white_list ) || (egoid, recorded_frame) in white_list

                    # ==========================================================
                    #   Initializing
                    # ==========================================================
                    # Extract the relative heading of the ego in the analyzing range.
                    # Generate the analyzing range.
                    (b_margin, a_margin) = proper_frame_range(range_margins, egoid, recorded_frame);
                    ana_framerange = collect( recorded_frame - b_margin: recorded_frame + a_margin );
                    # We want to get the relative heading of the ego for each frame in the analyzing range.
                    # Initialize a container to save the retrieved ego relative heading, lateral displacement, and speed.
                    ego_rel_heading_vec = Vector{Float64}();
                    ego_lat_dis_vec = Vector{Float64}();
                    ego_v_vec = Vector{Float64}();
                    ego_a_vec = Vector{Float64}();
                    # Let the first value of ego_a_vec to be 0.0.
                    push!(ego_a_vec, 0.0);

                    # Initialize a container to save the retrieved speed, the distance to the ego.
                    # Veh 1
                    veh1_v_vec = Vector{Float64}();
                    veh1_Δd_vec = Vector{Float64}();
                    # Veh 2
                    veh2_v_vec = Vector{Float64}();
                    veh2_Δd_vec = Vector{Float64}();
                    # Veh 3
                    veh3_v_vec = Vector{Float64}();
                    veh3_Δd_vec = Vector{Float64}();
                    # Veh 22
                    veh22_v_vec = Vector{Float64}();
                    veh22_Δd_vec = Vector{Float64}();
                    # Veh 32
                    veh32_v_vec = Vector{Float64}();
                    veh32_Δd_vec = Vector{Float64}();

                    # Initialize containers to save the acceleration.
                    # Veh 1
                    veh1_a_vec = Vector{Float64}();
                    veh1_inferred_a_vec = Vector{Float64}();
                    # Veh 2
                    veh2_a_vec = Vector{Float64}();
                    veh2_inferred_a_vec = Vector{Float64}();
                    # Veh 22
                    veh22_a_vec = Vector{Float64}();
                    veh22_inferred_a_vec = Vector{Float64}();

                    # Initialize container to save lane costs
                    cost_current_lane_vec = Vector{Float64}();
                    cost_left_fore_vec = Vector{Float64}();    # Left lane
                    cost_right_fore_vec = Vector{Float64}();   # Right lane
                    Δcost_left_fore_vec = Vector{Float64}();   # Left lane
                    Δcost_right_fore_vec = Vector{Float64}();  # Right lane

                    # Initialize container to save lane costs
                    cost_left_rear_vec = Vector{Float64}();    # Left lane
                    cost_right_rear_vec = Vector{Float64}();   # Right lane
                    prob_safe_3_vec = Vector{Float64}();        # Left lane
                    prob_safe_32_vec = Vector{Float64}();       # Right lane
                    Δcost_left_rear_vec = Vector{Float64}();    # Left lane
                    Δcost_right_rear_vec = Vector{Float64}();   # right lane

                    # Initialize containers to store neighbor ids
                    fore_neighbors = Vector{Int64}();
                    left_fore_neighbors = Vector{Int64}();
                    right_fore_neighbors = Vector{Int64}();
                    left_rear_neighbors = Vector{Int64}();
                    right_rear_neighbors = Vector{Int64}();

                    # At some frames the ego is side-blocked by a neighbor. We assume at these frames the ego driver
                    # cannot start the lane changing maneuver.
                    left_blocked_vec = Vector{Int}();    # If blocked, the value is 1, otherwise it is 0.
                    right_blocked_vec = Vector{Int}();

                    # Initialize container to save the lane ids of the ego to determine where the lane is changed.
                    ego_laneids = Vector{Int}();

                    # Initialize the length of the ego vehicle for later use.
                    ego_width = 0.0;

                    # ==========================================================
                    #   Main loop
                    # ==========================================================
                    for ego_frame in ana_framerange

                        # used for debugging
                        if debugging
                            println("===========================================")
                            println("ego_id: ", egoid)
                            println("ego frame: ", ego_frame)
                        end

                        # ============================================================
                        # Get true traffic_info of vehicles from NGSIM
                        # ============================================================

                        # -----------------------------------------------------------
                        # Get traffic info of the ego.
                        ego_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame,
                                                              longitudinal_direc="self",
                                                              lateral_direc = "middle", sample_duration = SAMPLE_DURATION,
                                                              aggregate_method = "mean" );
                        push!(ego_rel_heading_vec, ego_traffic_info[:heading]);
                        push!(ego_lat_dis_vec, ego_traffic_info[:lat_pos]);
                        push!(ego_v_vec, ego_traffic_info[:speed]);
                        ego_width = ego_traffic_info[:vehicle_def].width;

                        # Let compute the acceleration of ego from its speed sequence.
                        if length(ego_v_vec) > 1
                            ego_a = ( ego_v_vec[end] - ego_v_vec[end-1] ) / 0.1;
                            push!(ego_a_vec, ego_a);
                        end

                        # obsolete{
                        # # -------------------------------------------------------------
                        # # Get the traffic info of the following vehicle (veh 4)
                        # # Add time delay due to ACT-R. Let the decision-making moment be t = 0.
                        # # The observation of veh4 (checking middle mirror) happens at T = -1.5 s.
                        # Δframe_rear = 15;
                        # veh4_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_rear,
                        #                                        longitudinal_direc="rear",
                        #                                        lateral_direc = "middle", sample_duration = SAMPLE_DURATION,
                        #                                        aggregate_method = "mean" );
                        # obsolete}

                        # -----------------------front vehicles --------------------------
                        # Get the traffic info of the lead vehicle (veh 1)
                        # Add time delay due to ACT-R. Let the decision-making moment be t = 0.
                        # The observation of veh1 happens at T = -0.7 s.
                        Δframe_fore = 6;
                        tentative_veh1_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_fore,
                                                                            longitudinal_direc="fore",
                                                                            lateral_direc = "middle", sample_duration = SAMPLE_DURATION,
                                                                            aggregate_method = "mean" );

                        # Get the traffic info of the left front vehicle (veh 2)
                        tentative_veh2_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_fore,
                                                               longitudinal_direc="fore",
                                                               lateral_direc = "left", sample_duration = SAMPLE_DURATION,
                                                               aggregate_method = "mean" );

                        # Get the traffic info of the right front vehicle (veh 22)
                        tentative_veh22_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_fore,
                                                              longitudinal_direc="fore",
                                                              lateral_direc = "right", sample_duration = SAMPLE_DURATION,
                                                              aggregate_method = "mean" );

                        # Make adjustement to veh1 in case veh2 or veh22 is cutting in.
                        veh1_traffic_info = front_traffic_info_tentative_to_official(tentative_veh1_traffic_info,
                                                                                     tentative_veh2_traffic_info,
                                                                                     tentative_veh22_traffic_info);
                        # We save the true speed of veh1 and relative distance
                        push!(veh1_v_vec, veh1_traffic_info[:speed]);
                        push!(veh1_Δd_vec, veh1_traffic_info[:distance]);
                        push!(fore_neighbors, veh1_traffic_info[:vehicle_id]);
                        # Let us also update the accelerations
                        update_acceleration_vectors!( ( veh1_a_vec, veh1_inferred_a_vec ), veh1_v_vec);

                        # --------------------- rear vehicles -------------------------------
                        # Get the traffic info of the left rear vehicle (veh 3)
                        Δframe_adjrear = 1;
                        tentative_veh3_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_adjrear,
                                                               longitudinal_direc="rear",
                                                               lateral_direc = "left", sample_duration = SAMPLE_DURATION,
                                                               aggregate_method = "mean" );
                        # Make necessary adjustment to the right vehicles.
                        (veh2_traffic_info, veh3_traffic_info) = adjacent_traffic_info_tentative_to_official(
                                                                                      tentative_veh2_traffic_info,
                                                                                      tentative_veh3_traffic_info,
                                                                                      ego_traffic_info,
                                                                                      ego_frame,
                                                                                      lateral_direction = "left",
                                                                                      Δframe_fore = Δframe_fore,
                                                                                      Δframe_rear = Δframe_adjrear);
                        # We save the true speed of veh2 and relative distance
                        push!(veh2_v_vec, veh2_traffic_info[:speed]);
                        push!(veh2_Δd_vec, veh2_traffic_info[:distance]);
                        # We save the true speed of veh3 and relative distance
                        push!(veh3_v_vec, veh3_traffic_info[:speed]);
                        push!(veh3_Δd_vec, veh3_traffic_info[:distance]);
                        # Save neigbors
                        push!(left_fore_neighbors, veh2_traffic_info[:vehicle_id]);
                        push!(left_rear_neighbors, veh3_traffic_info[:vehicle_id]);
                        # Let us also update the accelerations of veh2
                        update_acceleration_vectors!( ( veh2_a_vec, veh2_inferred_a_vec ), veh2_v_vec);

                        # Get the traffic info of the right rear vehicle (veh 32)
                        tentative_veh32_traffic_info = traffic_info_NGSIM(trajdata_my, ROADWAY_my, egoid, ego_frame - Δframe_adjrear,
                                                               longitudinal_direc="rear",
                                                               lateral_direc = "right", sample_duration = SAMPLE_DURATION,
                                                               aggregate_method = "mean" );
                        # Make necessary adjustment to the right vehicles.
                        (veh22_traffic_info, veh32_traffic_info) = adjacent_traffic_info_tentative_to_official(
                                                                                               tentative_veh22_traffic_info,
                                                                                               tentative_veh32_traffic_info,
                                                                                               ego_traffic_info,
                                                                                               ego_frame,
                                                                                               lateral_direction = "right",
                                                                                               Δframe_fore = Δframe_fore,
                                                                                               Δframe_rear = Δframe_adjrear);
                        # We save the true speed of veh22 and relative distance
                        push!(veh22_v_vec, veh22_traffic_info[:speed]);
                        push!(veh22_Δd_vec, veh22_traffic_info[:distance]);
                        # We save the true speed of veh32 and relative distance
                        push!(veh32_v_vec, veh32_traffic_info[:speed]);
                        push!(veh32_Δd_vec, veh32_traffic_info[:distance]);
                        # Save neigbors
                        push!(right_fore_neighbors, veh22_traffic_info[:vehicle_id]);
                        push!(right_rear_neighbors, veh32_traffic_info[:vehicle_id]);
                        # Let us also update the accelerations of veh22
                        update_acceleration_vectors!( ( veh22_a_vec, veh22_inferred_a_vec ), veh22_v_vec);

                        # ==========================================================
                        #   Get human perceived traffic info
                        # ==========================================================
                        # We compute the peceived relative distance between veh 1 and the ego.
                        if debugging
                            println("perceive veh1");
                        end
                        (σ_ω_p_1, σ_θ_p_1) = get_observation_std(ego_traffic_info, veh1_traffic_info, longitudinal_direction = "fore");
                        ( rel_dis_1_0, σ_Δd_10, rel_speed_1_0, σ_Δv_10 ) = perception_model( ego_traffic_info,
                                                                                             veh1_traffic_info,
                                                                                             longitudinal_direction = "fore",
                                                                                             σ_ω_p = σ_ω_p_1,
                                                                                             σ_θ_p = σ_θ_p_1);
                        # Retrieve the perceived acceleration according to the brake light.
                        veh1_inferred_a = veh1_inferred_a_vec[ end ];
                        # Pack this perceived traffic info
                        veh1_perceived_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}(:vehicle_id => veh1_traffic_info[:vehicle_id],
                                                                                            :vehicle_def => veh1_traffic_info[:vehicle_def],
                                                                                            :Δd => rel_dis_1_0, :σ_Δd => σ_Δd_10,
                                                                                            :Δv => rel_speed_1_0, :σ_Δv => σ_Δv_10,
                                                                                            :a => veh1_inferred_a );

                        # We get the relative distance and speed of veh 2 w.r.t the ego.
                        if debugging
                            println("perceive veh2");
                        end
                        (σ_ω_p_2, σ_θ_p_2) = get_observation_std(ego_traffic_info, veh2_traffic_info, longitudinal_direction = "fore");
                        ( rel_dis_2_0, σ_Δd_20, rel_speed_2_0, σ_Δv_20 ) = perception_model( ego_traffic_info,
                                                                                             veh2_traffic_info,
                                                                                             longitudinal_direction = "fore",
                                                                                             σ_ω_p = σ_ω_p_2,
                                                                                             σ_θ_p = σ_θ_p_2);
                        # Retrieve the perceived acceleration according to the brake light.
                        veh2_inferred_a = veh2_inferred_a_vec[ end ];
                        # Pack this perceived traffic info
                        veh2_perceived_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}(:vehicle_id => veh2_traffic_info[:vehicle_id],
                                                                                            :vehicle_def => veh2_traffic_info[:vehicle_def],
                                                                                            :Δd => rel_dis_2_0, :σ_Δd => σ_Δd_20,
                                                                                            :Δv => rel_speed_2_0, :σ_Δv => σ_Δv_20,
                                                                                            :a => veh2_inferred_a );

                        # Let us get the perceived relative distance and speed of the ego w.r.t veh 3
                        if debugging
                            println("perceive veh3");
                        end
                        (σ_ω_p_3, σ_θ_p_3) = get_observation_std(ego_traffic_info, veh3_traffic_info, longitudinal_direction = "rear");
                        ( rel_dis_0_3, σ_Δd_03, rel_speed_0_3, σ_Δv_03 ) = perception_model( ego_traffic_info,
                                                                                             veh3_traffic_info,
                                                                                             longitudinal_direction = "rear",
                                                                                             Δv_prior_rear = -rel_speed_2_0,
                                                                                             σ_ω_p = σ_ω_p_3,
                                                                                             σ_θ_p = σ_θ_p_3);
                        # Pack this perceived traffic info
                        veh3_perceived_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}(:vehicle_id => veh3_traffic_info[:vehicle_id],
                                                                                            :vehicle_def => veh3_traffic_info[:vehicle_def],
                                                                                            :Δd => rel_dis_0_3, :σ_Δd => σ_Δd_03,
                                                                                            :Δv => rel_speed_0_3, :σ_Δv => σ_Δv_03,
                                                                                            :a => 0.0 );

                        # We get the relative distance and speed of veh 22 w.r.t. the ego
                        if debugging
                            println("perceive veh22");
                        end
                        (σ_ω_p_22, σ_θ_p_22) = get_observation_std(ego_traffic_info, veh22_traffic_info, longitudinal_direction = "fore");
                        ( rel_dis_22_0, σ_Δd_220, rel_speed_22_0, σ_Δv_220 ) = perception_model( ego_traffic_info,
                                                                                                 veh22_traffic_info,
                                                                                                 longitudinal_direction = "fore",
                                                                                                 σ_ω_p = σ_ω_p_22,
                                                                                                 σ_θ_p = σ_θ_p_22);
                        # Retrieve the perceived acceleration according to the brake light.
                        veh22_inferred_a = veh22_inferred_a_vec[ end ];
                        # Pack this perceived traffic info
                        veh22_perceived_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}(:vehicle_id => veh22_traffic_info[:vehicle_id],
                                                                                             :vehicle_def => veh22_traffic_info[:vehicle_def],
                                                                                             :Δd => rel_dis_22_0, :σ_Δd => σ_Δd_220,
                                                                                             :Δv => rel_speed_22_0, :σ_Δv => σ_Δv_220,
                                                                                             :a => veh22_inferred_a );

                        # Let us get the perceived relative distance and speed of the ego w.r.t veh 32
                        if debugging
                            println("perceive veh32");
                        end
                        (σ_ω_p_32, σ_θ_p_32) = get_observation_std(ego_traffic_info, veh32_traffic_info, longitudinal_direction = "rear");
                        ( rel_dis_0_32, σ_Δd_032, rel_speed_0_32, σ_Δv_032 ) = perception_model( ego_traffic_info,
                                                                                                 veh32_traffic_info,
                                                                                                 longitudinal_direction = "rear",
                                                                                                 Δv_prior_rear = -rel_speed_22_0,
                                                                                                 σ_ω_p = σ_ω_p_32,
                                                                                                 σ_θ_p = σ_θ_p_32);
                        # Pack this perceived traffic info
                        veh32_perceived_info = Dict{Symbol, Union{Float64, Int, VehicleDef}}(:vehicle_id => veh32_traffic_info[:vehicle_id],
                                                                                             :vehicle_def => veh32_traffic_info[:vehicle_def],
                                                                                             :Δd => rel_dis_0_32, :σ_Δd => σ_Δd_032,
                                                                                             :Δv => rel_speed_0_32, :σ_Δv => σ_Δv_032,
                                                                                             :a => 0.0 );


                        # ==========================================================
                        #  Compute costs/rewards
                        # ==========================================================

                        # Get the lane ids
                        ego_laneid = get_laneid_from_trajdata_i101(egoid, ego_frame);
                        push!( ego_laneids, ego_laneid );

                        # The costs/rewards are based on the admissible speed in current, left, and right lane.

                        # obsolete{
                        # # Since the front traffic info is observed Δframe_veh1 frames ago,
                        # # we should add the delay to the observed ego_laneid.
                        # ego_laneid_delay = minimum( [ Δframe_fore, ( length(ego_laneids) - 1 ) ] );
                        # obsolete}

                        # CLR stands for center/current, left, and right
                        CLR_lane_speeds = get_lane_admissible_speeds(ego_traffic_info[:speed],
                                                                     veh1_perceived_info,
                                                                     veh2_perceived_info,
                                                                     veh22_perceived_info);

                        # We compute the cost/reward due to veh 1 here. It is the admissible speed in the current lane.
                        cost_current_lane = CLR_lane_speeds[1];
                        push!( cost_current_lane_vec, cost_current_lane );

                        # In trajdata_my, the leftmost lane is labelled as 5, which is different if we used tdraw_my. Be careful.
                        # If the ego is in lane 5, then it has no left fore or rear neighbors, but turning left could cause collision
                        # with road edges.
                        if ego_laneid == 5
                            # The cost of the virtual left lane is the collision cost.
                            cost_left_fore = COLLISION_WITH_WALL_COST;
                        else
                            # We compute the lane cost/reward due to veh2 here .
                            cost_left_fore = CLR_lane_speeds[2];
                        end
                        push!( cost_left_fore_vec, cost_left_fore );
                        # We also compute the following difference, since this could be the lane change motivation.
                        Δcost_left_fore = cost_left_fore - cost_current_lane;
                        push!( Δcost_left_fore_vec, Δcost_left_fore );

                        # In trajdata_my, the rightmost lane is labelled as 1, which is different if we used tdraw_my. Be careful.
                        # If the ego is in lane 1, then it has no right fore or rear neighbors, but turning right could cause collision
                        # with road edges.
                        if ego_laneid == 1 # Rightmost lane of the main road according to trajdata_my
                            # The cost of the virtual right lane is the collision cost.
                            cost_right_fore = COLLISION_WITH_WALL_COST;
                        else
                            cost_right_fore = CLR_lane_speeds[3];
                        end
                        push!( cost_right_fore_vec, cost_right_fore );
                        # We also compute the following difference, since this could be the lane change motivation.
                        Δcost_right_fore = cost_right_fore - cost_current_lane;
                        push!( Δcost_right_fore_vec, Δcost_right_fore );

                        # We now compute the collision threat imposed by the veh3 in the left lane and veh32 in the
                        # right lane.
                        if ego_laneid == 5
                            # The cost of the virtual left lane is the collision cost.
                            cost_left_rear = COLLISION_WITH_WALL_COST;
                        else
                            # We compute the lane cost/reward due to veh2 here .
                            cost_left_rear = get_collision_threat_cost(ego_traffic_info, veh3_traffic_info, rel_speed_0_3);
                        end
                        push!( cost_left_rear_vec, cost_left_rear );
                        Δcost_left_rear = cost_left_rear - cost_current_lane;
                        push!( Δcost_left_rear_vec, Δcost_left_rear );

                        if ego_laneid == 1 # Rightmost lane of the main road according to trajdata_my
                            # The cost of the virtual right lane is the collision cost.
                            cost_right_rear = COLLISION_WITH_WALL_COST;
                        else
                            cost_right_rear = get_collision_threat_cost(ego_traffic_info, veh32_traffic_info, rel_speed_0_32);
                        end
                        push!( cost_right_rear_vec, cost_right_rear );
                        Δcost_right_rear = cost_right_rear - cost_current_lane;
                        push!( Δcost_right_rear_vec, Δcost_right_rear );

                        # ==========================================================
                        #  Compute probability
                        # ==========================================================

                        # In trajdata_my, the leftmost lane is labelled as 5, which is different if we used tdraw_my. Be careful.
                        # If the ego is in lane 5, then it has no left fore or rear neighbors, but turning left could cause collision
                        # with road edges.
                        if ego_laneid == 5
                            # Turning left is for sure not safe.
                            push!( prob_safe_3_vec, 0.0 );
                            # The ego is left blocked by the road edge.
                            push!( left_blocked_vec, 1  );
                        else
                            # Let us get the probability of being safe.
                            if debugging
                                println("left: ")
                            end
                            left_target_speed = CLR_lane_speeds[2];
                            prob_safe_3 = noisy_newton_collision_model( ego_traffic_info,
                                                                        veh1_perceived_info, veh2_perceived_info, veh3_perceived_info,
                                                                        left_target_speed,
                                                                        veh1_inferred_a, veh2_inferred_a,
                                                                        (σ_ω_p_1, σ_θ_p_1), (σ_ω_p_2, σ_θ_p_2),
                                                                        lateral_direction = "left",
                                                                        debug = false);
                            push!( prob_safe_3_vec, prob_safe_3 );

                            # If either of veh 2 or veh 3 is too close, the ego is left blocked. Otherwise, the ego is left free.
                            dis_tolerance_margin = 4.0;  # meters
                            if rel_dis_2_0 < dis_tolerance_margin || rel_dis_0_3 < dis_tolerance_margin
                                push!( left_blocked_vec, 1 );
                            else
                                push!( left_blocked_vec, 0 );
                            end
                        end

                        # In trajdata_my, the rightmost lane is labelled as 1, which is different if we used tdraw_my. Be careful.
                        # If the ego is in lane 1, then it has no right fore or rear neighbors, but turning right could cause collision
                        # with road edges.
                        if ego_laneid == 1 # Rightmost lane of the main road according to trajdata_my
                            push!( prob_safe_32_vec, 0.0 );
                            # The ego is right blocked by the road edge.
                            push!( right_blocked_vec, 1  );
                        else
                            if debugging
                                println("right: ")
                            end
                            right_target_speed = CLR_lane_speeds[3];
                            prob_safe_32 = noisy_newton_collision_model( ego_traffic_info, veh1_perceived_info,
                                                                         veh22_perceived_info, veh32_perceived_info,
                                                                         right_target_speed,
                                                                         veh1_inferred_a, veh22_inferred_a,
                                                                         (σ_ω_p_1, σ_θ_p_1), (σ_ω_p_22, σ_θ_p_22),
                                                                         lateral_direction = "right",
                                                                         debug = false);
                            push!( prob_safe_32_vec, prob_safe_32 );

                            # If either of veh 22 or veh 32 is too close, the ego is right blocked. Otherwise, the ego is right free.
                            # The following dis_tolerance_margin is defined above when considering left lane changing.
                            dis_tolerance_margin = 4.0;  # meters
                            if rel_dis_22_0 < dis_tolerance_margin || rel_dis_0_32 < dis_tolerance_margin
                                push!( right_blocked_vec, 1 );
                            else
                                push!( right_blocked_vec, 0 );
                            end
                        end

                    end



                    # # ====================================================
                    # # Exclude the trivial cases for lane keep cases.
                    # # ====================================================
                    # if create_exclusion_list
                    #     # Make sure the adjacent lanes can be attractive.
                    #     Δcost_2_1_vec = cost_left_fore_vec - cost_current_lane_vec;
                    #     Δcost_22_1_vec = cost_right_fore_vec - cost_current_lane_vec;
                    #     left_positive_moments = find(x->x>0, Δcost_2_1_vec);
                    #     right_positive_moments = find(x->x>0, Δcost_22_1_vec);
                    #     if isempty(left_positive_moments) && isempty(right_positive_moments)
                    #         # This case needs to be excluded, since the decision is obvious.
                    #         push!(exclusion_list, (egoid, recorded_frame));
                    #         println(" Exclude ", (egoid, recorded_frame), " due to unattractive adjacent lanes.")
                    #     end
                    #     # Make sure the ego is not blocked by the adjacent neighbors for most of the time.
                    #     left_blocked_ratio = sum( left_blocked_vec ) / length( left_blocked_vec );
                    #     right_blocked_ratio = sum( right_blocked_vec) / length( right_blocked_vec );
                    #     if left_blocked_ratio > 0.3 && right_blocked_ratio > 0.3
                    #         # This case needs to be excluded, since the ego has no options.
                    #         push!(exclusion_list, (egoid, recorded_frame));
                    #         println(" Exclude ", (egoid, recorded_frame), " due to blocking neighbors.")
                    #     end
                    # end

                    # ====================================================================================
                    # Extracting data
                    # ====================================================================================

                    # We first want to determine when a lane change happens. If no lane-changing, we consider
                    # the whole frame range.
                    # Let us get where the lane change happens.
                    newlane_ind = find(x->x!=ego_laneids[1], ego_laneids);
                    if !isempty(newlane_ind)
                        chg_ind = newlane_ind[1];
                    else
                        chg_ind = length(ego_laneids);
                    end

                    if analyze_subject == "change"
                        # Here we determine the turning point and decision point for lane changing
                        (changedecision_point, turn_point) = get_changedecision_and_turning_point( ego_lat_dis_vec[1: chg_ind-1],
                                                                                                   prob_safe_3_vec[1: chg_ind-1],
                                                                                                   prob_safe_32_vec[1: chg_ind-1],
                                                                                                   cost_left_fore_vec[1: chg_ind-1],
                                                                                                   cost_current_lane_vec[1: chg_ind-1],
                                                                                                   cost_right_fore_vec[1: chg_ind-1] );

                    elseif analyze_subject == "keep"
                        # Here we determine the braking point and decision point for lane keeping.
                        (keepdecision_point, brake_point) = get_keepdecision_and_braking_point( ego_a_vec[1: chg_ind-1],
                                                                                                prob_safe_3_vec[1: chg_ind-1],
                                                                                                prob_safe_32_vec[1: chg_ind-1],
                                                                                                cost_left_fore_vec[1: chg_ind-1],
                                                                                                cost_current_lane_vec[1: chg_ind-1],
                                                                                                cost_right_fore_vec[1: chg_ind-1]);
                    else
                        error("no proper analyze_subject");
                    end

                    # Data extraction
                    if data_extracting
                        # Prepare the data
                        # 1) Determine which side to consider, left or right?
                        # If it is the lane-changing case, we use the recorded changing direction to determine the side.
                        if analyze_subject == "change"
                            # If the vehicle turned to left,
                            if ego_lat_dis_vec[ chg_ind-1 ] > 0     # left
                                # Retrieve the data at the decision point.
                                de_frame = ana_framerange[ changedecision_point ];
                                cost_keep = cost_current_lane_vec[ changedecision_point ];
                                cost_change = cost_left_fore_vec[ changedecision_point ];
                                cost_collision = cost_left_rear_vec[ changedecision_point ];
                                prob_safe = prob_safe_3_vec[ changedecision_point ];
                                subj_v = ego_v_vec[ changedecision_point ];
                                adj_lead_v = veh2_v_vec[ changedecision_point ];
                                adj_lead_rd = veh2_Δd_vec[ changedecision_point ];
                                adj_lag_v = veh3_v_vec[ changedecision_point ];
                                adj_lag_rd = veh3_Δd_vec[ changedecision_point ];
                                decision = 1;

                            # Otherwise, the vehicle turned to right.
                            else                                    # right
                                # Retrieve the data at the decision point.
                                de_frame = ana_framerange[ changedecision_point ];
                                cost_keep = cost_current_lane_vec[ changedecision_point ];
                                cost_change = cost_right_fore_vec[ changedecision_point ];
                                cost_collision = cost_right_rear_vec[ changedecision_point ];
                                prob_safe = prob_safe_32_vec[ changedecision_point ];
                                subj_v = ego_v_vec[ changedecision_point ];
                                adj_lead_v = veh22_v_vec[ changedecision_point ];
                                adj_lead_rd = veh22_Δd_vec[ changedecision_point ];
                                adj_lag_v = veh32_v_vec[ changedecision_point ];
                                adj_lag_rd = veh32_Δd_vec[ changedecision_point ];
                                decision = 1;
                            end

                        elseif analyze_subject == "keep"
                            # If the fore cost of the left lane is larger than the right lane, we assume the
                            # driver considered the left lane.
                            if cost_left_fore_vec[ keepdecision_point ] >= cost_right_fore_vec[ keepdecision_point ]    # left
                                # Retrieve the data at the decision point.
                                de_frame = ana_framerange[ keepdecision_point ];
                                cost_keep = cost_current_lane_vec[ keepdecision_point ];
                                cost_change = cost_left_fore_vec[ keepdecision_point ];
                                cost_collision = cost_left_rear_vec[ keepdecision_point ];
                                prob_safe = prob_safe_3_vec[ keepdecision_point ];
                                subj_v = ego_v_vec[ keepdecision_point ];
                                adj_lead_v = veh2_v_vec[ keepdecision_point ];
                                adj_lead_rd = veh2_Δd_vec[ keepdecision_point ];
                                adj_lag_v = veh3_v_vec[ keepdecision_point ];
                                adj_lag_rd = veh3_Δd_vec[ keepdecision_point ];
                                decision = 0;

                            # Otherwise, we assume the driver considered the right lane.
                            else                    # right
                                # Retrieve the data at the decision point.
                                de_frame = ana_framerange[ keepdecision_point ];
                                cost_keep = cost_current_lane_vec[ keepdecision_point ];
                                cost_change = cost_right_fore_vec[ keepdecision_point ];
                                cost_collision = cost_right_rear_vec[ keepdecision_point ];
                                prob_safe = prob_safe_32_vec[ keepdecision_point ];
                                subj_v = ego_v_vec[ keepdecision_point ];
                                adj_lead_v = veh22_v_vec[ keepdecision_point ];
                                adj_lead_rd = veh22_Δd_vec[ keepdecision_point ];
                                adj_lag_v = veh32_v_vec[ keepdecision_point ];
                                adj_lag_rd = veh32_Δd_vec[ keepdecision_point ];
                                decision = 0;
                            end

                        else
                            error("no proper analyze_subject");
                        end

                        # We are extracting data into vectors here!
                        push!(car_id_vec, egoid);
                        push!(re_frame_vec, recorded_frame);
                        push!(de_frame_vec, de_frame);
                        push!(cost_keep_vec, cost_keep);
                        push!(cost_change_vec, cost_change);
                        push!(cost_collision_vec, cost_collision);
                        push!(prob_safe_vec, prob_safe );
                        push!(subj_v_vec, subj_v);
                        push!(adj_lead_v_vec, adj_lead_v);
                        push!(adj_lead_rd_vec, adj_lead_rd);
                        push!(adj_lag_v_vec, adj_lag_v);
                        push!(adj_lag_rd_vec, adj_lag_rd);
                        push!(decision_vec, decision );

                        # Display the progress
                        println(" Finish generate vectors for car ", egoid);
                    end

                    # =========================================================
                    # Plotting
                    # =========================================================
                    if plotting
                        # Plotting
                        ioff();
                        # Here we define the y limt for cost plots.
                        costlimt = abs( COLLISION_WITH_WALL_COST ) + 2;

                        figure(figsize=[6.4, 9.6]);
                        subplot(611);
                        plot(ana_framerange[1: chg_ind-1], ego_v_vec[1: chg_ind-1]);
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["ego_speed"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        minor_ytick = matplotlib[:ticker][:MultipleLocator](0.5);     # Prepare the minor ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(612);
                        plot(ana_framerange[1: chg_ind-1], veh1_v_vec[1: chg_ind-1]);
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["veh1_speed"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        minor_ytick = matplotlib[:ticker][:MultipleLocator](0.5);     # Prepare the minor ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(613);
                        plot(ana_framerange[1: chg_ind-1], veh1_a_vec[1: chg_ind-1]);
                        plot([ ana_framerange[1],  ana_framerange[chg_ind-1] ], [-1.0, -1.0], "r");
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["veh1_a"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        minor_ytick = matplotlib[:ticker][:MultipleLocator](0.5);     # Prepare the minor ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(614);
                        plot(ana_framerange[1: chg_ind-1], veh1_Δd_vec[1: chg_ind-1]);
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["veh1_Δd"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        minor_ytick = matplotlib[:ticker][:MultipleLocator](0.5);     # Prepare the minor ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(615);
                        plot( ana_framerange[1: chg_ind-1], ego_rel_heading_vec[1: chg_ind-1] );
                        # obsolete{
                        # plot( irreversible_moment_vec, zeros( length( irreversible_moment_vec ) ), "r." );
                        # obsolete}
                        ylim((-0.1, 0.1));
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["ego heading"]);
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(616);
                        plot(ana_framerange[1: chg_ind-1], ego_a_vec[1: chg_ind-1]);
                        plot([ ana_framerange[1],  ana_framerange[chg_ind-1] ], [-1.0, -1.0], "r");
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["ego_a"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        minor_ytick = matplotlib[:ticker][:MultipleLocator](0.5);     # Prepare the minor ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible

                        ax_path_ego = "./Pictures/"*analyze_subject*string(egoid)*"_"*string(recorded_frame)*"_aego.png";
                        println("Print to: ", ax_path_ego);
                        savefig(ax_path_ego);
                        close();

                        # Here we find when the neighbors changed.
                        veh1_change_frames = get_when_neighbor_changed(ana_framerange[1: chg_ind-1], fore_neighbors[1: chg_ind-1]);
                        veh2_change_frames = get_when_neighbor_changed(ana_framerange[1: chg_ind-1], left_fore_neighbors[1: chg_ind-1]);
                        veh3_change_frames = get_when_neighbor_changed(ana_framerange[1: chg_ind-1], left_rear_neighbors[1: chg_ind-1]);
                        veh22_change_frames = get_when_neighbor_changed(ana_framerange[1: chg_ind-1], right_fore_neighbors[1: chg_ind-1]);
                        veh32_change_frames = get_when_neighbor_changed(ana_framerange[1: chg_ind-1], right_rear_neighbors[1: chg_ind-1]);

                        figure(figsize=[6.4, 9.6]);
                        subplot(611);
                        if analyze_subject == "change"
                            plot( ana_framerange[1: chg_ind-1], ego_lat_dis_vec[1: chg_ind-1]);
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ], [-2, 2], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ], [-2, 2], "r--");
                            legend(["ego lateral position"]);
                        elseif analyze_subject == "keep"
                            plot(ana_framerange[1: chg_ind-1], ego_a_vec[1: chg_ind-1]);
                            # plot( ana_framerange[1: chg_ind-1], ego_lat_dis_vec[1: chg_ind-1]);
                            plot([ ana_framerange[1],  ana_framerange[chg_ind-1] ], [-0.5, -0.5], "r");
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ], [-2, 2], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ], [-2, 2], "r--");
                            legend(["ego acceleration"]);
                            # legend(["veh 0 lateral position"]);
                        end
                        ylim((-2.0, 2.0));
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));

                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );

                        subplot(612);
                        plot( ana_framerange[1: chg_ind-1], cost_left_rear_vec[1: chg_ind-1]);
                        # Δcost_left_rear_filtered = lowpass_filter(Δcost_left_rear_vec, width = 10, α= 0.2);
                        plot( ana_framerange[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]);
                        # plot([ana_framerange[1], ana_framerange[chg_ind-1] ],[ 0, 0 ]);
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "r--");
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "r--");
                        end
                        # veh3 changes
                        for changeframe in veh3_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "y--");
                        end
                        # veh32 changes
                        for changeframe in veh32_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) ),
                                    maximum( vcat( cost_left_rear_vec[1: chg_ind-1], cost_right_rear_vec[1: chg_ind-1]) )], "m--");
                        end
                        # ylim( (-costlimt, 0.05 ) );
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["cost_left_rear", "cost_right_rear"]);
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(613);
                        plot(ana_framerange[1: chg_ind-1], cost_left_fore_vec[1: chg_ind-1]);
                        plot(ana_framerange[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1]);
                        # cost_left_fore_filtered = lowpass_filter(cost_left_fore_vec, width = 10, α= 0.2);
                        # plot( ana_framerange[1: chg_ind-1], Δcost_left_fore_filtered[1: chg_ind-1]);
                        # plot([ana_framerange[1], ana_framerange[chg_ind-1] ],[ 0, 0 ]);
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ],
                            [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                              maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ],
                            [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                              maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "r--");
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ],
                            [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                              maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ],
                            [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                              maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "r--");
                        end
                        # veh2 changes
                        for changeframe in veh2_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "y--");
                        end
                        # veh1 changes
                        for changeframe in veh1_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_left_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "m--");
                        end
                        # ylim( ( -0.05, maximum( Δcost_2_1_vec[1: chg_ind-1] ) ) );
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["cost_left_fore", "cost_current_lane"]);
                        major_ytick2 = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick2);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(614);
                        plot( ana_framerange[1: chg_ind-1], prob_safe_3_vec[1: chg_ind-1]);
                        prob_safe_3_filtered = lowpass_filter(prob_safe_3_vec[1: chg_ind-1]);
                        plot( ana_framerange[1: chg_ind-1], prob_safe_3_filtered);
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ], [-0.2, 1.2], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ], [-0.2, 1.2], "r--");
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ], [-0.2, 1.2], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ], [-0.2, 1.2], "r--");
                        end

                        # veh3 changes
                        for changeframe in veh3_change_frames
                            plot( [ changeframe, changeframe ], [-0.2, 1.2], "y--");
                        end
                        ylim( (-0.2, 1.2) );
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["prob_safe_3"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(615);
                        plot(ana_framerange[1: chg_ind-1], cost_right_fore_vec[1: chg_ind-1]);
                        plot(ana_framerange[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1]);
                        # Δcost_right_fore_filtered = lowpass_filter(Δcost_right_fore_vec, width = 10, α= 0.2);
                        # plot( ana_framerange[1: chg_ind-1], Δcost_right_fore_filtered[1: chg_ind-1]);
                        # plot([ana_framerange[1], ana_framerange[chg_ind-1] ],[ 0, 0 ]);
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "r--");
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "r--");
                        end

                        # veh22 changes
                        for changeframe in veh22_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "y--");
                        end
                        # veh1 changes
                        for changeframe in veh1_change_frames
                            plot( [ changeframe, changeframe ],
                                  [ minimum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ),
                                    maximum( vcat( cost_right_fore_vec[1: chg_ind-1], cost_current_lane_vec[1: chg_ind-1] ) ) ], "m--");
                        end
                        # ylim( ( -0.05, maximum( Δcost_22_1_vec[1: chg_ind-1] ) ) );
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["cost_right_fore", "cost_current_lane"]);
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick2);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(616);
                        plot( ana_framerange[1: chg_ind-1], prob_safe_32_vec[1: chg_ind-1] );
                        prob_safe_32_filtered = lowpass_filter(prob_safe_32_vec[1:chg_ind-1]);
                        plot( ana_framerange[1:chg_ind-1], prob_safe_32_filtered);
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ], [-0.2, 1.2], "g-");
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ], [-0.2, 1.2], "r--");
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ], [-0.2, 1.2], "g-");
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ], [-0.2, 1.2], "r--");
                        end

                        # veh32 changes
                        for changeframe in veh32_change_frames
                            plot( [ changeframe, changeframe ], [-0.2, 1.2], "y--");
                        end
                        ylim( (-0.2, 1.2) );
                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        legend(["prob_safe_32"]);
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        ax_path = "./Pictures/"*analyze_subject*string(egoid)*"_"*string(recorded_frame)*"_cost_prob.eps";
                        println("Print to: ", ax_path);
                        savefig(ax_path);
                        close();

                        # EV1 net advantage and prob of lanechange
                        model_parameters_ev0 = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
                        ( prob_change2left_ev0, e_ev0_left ) = get_lanechange_prob_RTx( cost_left_fore_vec, cost_left_rear_vec,
                                                                                        cost_current_lane_vec, prob_safe_3_vec,
                                                                                        model_parameters_ev0 );
                        ( prob_change2right_ev0, e_ev0_right ) = get_lanechange_prob_RTx( cost_right_fore_vec, cost_right_rear_vec,
                                                                                          cost_current_lane_vec, prob_safe_32_vec,
                                                                                          model_parameters_ev0 );
                        # Smoothing the curves
                        prob_change2left_ev0_filtered = lowpass_filter( prob_change2left_ev0 );
                        prob_change2right_ev0_filtered = lowpass_filter( prob_change2right_ev0 );

                        # EV1 net advantage and prob of lanechange
                        model_parameters_ev1 = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0];
                        ( prob_change2left_ev1, e_ev1_left ) = get_lanechange_prob_RTx( cost_left_fore_vec, cost_left_rear_vec,
                                                                                        cost_current_lane_vec, prob_safe_3_vec,
                                                                                        model_parameters_ev1);
                        ( prob_change2right_ev1, e_ev1_right ) = get_lanechange_prob_RTx( cost_right_fore_vec, cost_right_rear_vec,
                                                                                          cost_current_lane_vec, prob_safe_32_vec,
                                                                                          model_parameters_ev1 );
                        # Smoothing the curves
                        prob_change2left_ev1_filtered = lowpass_filter( prob_change2left_ev1 );
                        prob_change2right_ev1_filtered = lowpass_filter( prob_change2right_ev1 );

                        # RTx net advantage and prob of lanechange
                        model_parameters_rtx = [0.493623, 0.230809, 0.00669648, 1.17972, 3.63107, 0.0, 0.0, 0.0, 0.880075];


                        ( prob_change2left_rtx, e_rtx_left ) = get_lanechange_prob_RTx( cost_left_fore_vec, cost_left_rear_vec,
                                                                                        cost_current_lane_vec, prob_safe_3_vec,
                                                                                        model_parameters_rtx );
                        ( prob_change2right_rtx, e_rtx_right ) = get_lanechange_prob_RTx( cost_right_fore_vec, cost_right_rear_vec,
                                                                                          cost_current_lane_vec, prob_safe_32_vec,
                                                                                          model_parameters_rtx );
                        # Smoothing the curves
                        prob_change2left_rtx_filtered = lowpass_filter( prob_change2left_rtx );
                        prob_change2right_rtx_filtered = lowpass_filter( prob_change2right_rtx );

                        # Toledo' gap acceptance model prob of lanechange
                        model_parameters_toledo = [2.63355, -6.15088, 0.192649, 1.76419e-6, 1.06009, 1.30335, -0.663519, -2.00428e-6, 3.37967 ];

                        prob_change2left_toledo = get_lanechange_prob_toledo( ego_v_vec,
                                                                              veh2_v_vec, veh2_Δd_vec,
                                                                              veh3_v_vec, veh3_Δd_vec,
                                                                              model_parameters_toledo );

                        prob_change2right_toledo = get_lanechange_prob_toledo( ego_v_vec,
                                                                               veh22_v_vec, veh22_Δd_vec,
                                                                               veh32_v_vec, veh32_Δd_vec,
                                                                               model_parameters_toledo );

                        # Smoothing the curves
                        prob_change2left_toledo_filtered = lowpass_filter( prob_change2left_toledo );
                        prob_change2right_toledo_filtered = lowpass_filter( prob_change2right_toledo );

                        # Plotting
                        figure();
                        subplot(211);
                        plot( ana_framerange[1: chg_ind-1], prob_change2left_toledo_filtered[1: chg_ind-1], label="GA");
                        plot( ana_framerange[1: chg_ind-1], prob_change2left_ev0_filtered[1: chg_ind-1], label="EV0");
                        plot( ana_framerange[1: chg_ind-1], prob_change2left_ev1_filtered[1: chg_ind-1], label="EV1");
                        plot( ana_framerange[1: chg_ind-1], prob_change2left_rtx_filtered[1: chg_ind-1], label="RTx");
                        plot([ ana_framerange[1], ana_framerange[chg_ind-1] ], [0.5, 0.5], "r-", label=nothing );
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ],
                                  [ 0.0, 1.0 ], "g-", label=nothing);
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ],
                                  [ 0.0, 1.0 ], "r--", label=nothing);
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ],
                                  [ 0.0, 1.0 ], "g-", label=nothing);
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ],
                                  [ 0.0, 1.0 ], "r--", label=nothing);
                        end

                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        ylim([0,1]);
                        legend();
                        title("Left");
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        major_ytick = matplotlib[:ticker][:MultipleLocator](2);     # Prepare the major ytick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible
                        xticks( [] );           # Make the tick labels invisible.

                        subplot(212);
                        plot( ana_framerange[1: chg_ind-1], prob_change2right_toledo_filtered[1: chg_ind-1], label="GA");
                        plot( ana_framerange[1: chg_ind-1], prob_change2right_ev0_filtered[1: chg_ind-1], label="EV0");
                        plot( ana_framerange[1: chg_ind-1], prob_change2right_ev1_filtered[1: chg_ind-1], label="EV1");
                        plot( ana_framerange[1: chg_ind-1], prob_change2right_rtx_filtered[1: chg_ind-1], label="RTx");
                        plot([ ana_framerange[1], ana_framerange[chg_ind-1] ], [0.5, 0.5], "r-", label=nothing );
                        if analyze_subject == "change"
                            plot( [ ana_framerange[changedecision_point], ana_framerange[changedecision_point] ],
                                  [ 0.0, 1.0 ], "g-", label=nothing );
                            plot( [ ana_framerange[turn_point], ana_framerange[turn_point] ],
                                  [ 0.0, 1.0 ], "r--", label=nothing );
                        elseif analyze_subject == "keep"
                            plot( [ ana_framerange[keepdecision_point], ana_framerange[keepdecision_point] ],
                                  [ 0.0, 1.0 ], "g-", label=nothing );
                            plot( [ ana_framerange[brake_point], ana_framerange[brake_point] ],
                                  [ 0.0, 1.0 ], "r--", label=nothing );
                        end

                        xlim((ana_framerange[1], ana_framerange[chg_ind-1]));
                        ylim([0,1]);
                        legend();
                        title("Right")
                        major_xtick = matplotlib[:ticker][:MultipleLocator](10);    # Prepare the major xtick
                        minor_xtick = matplotlib[:ticker][:MultipleLocator](5);     # Prepare the minor xtick
                        ax = gca();
                        ax[:xaxis][:set_major_locator](major_xtick);    # set the ticks
                        ax[:xaxis][:set_minor_locator](minor_xtick);
                        # ax[:yaxis][:set_major_locator](major_ytick);
                        # ax[:yaxis][:set_minor_locator](minor_ytick);
                        grid(true, "both");     # Set both major and minor grid visible

                        ax_path_probchange = "./Pictures/"*analyze_subject*string(egoid)*"_"*string(recorded_frame)*"_prob_change.png";
                        println("Print to: ", ax_path_probchange);
                        savefig(ax_path_probchange);
                        close();

                    end
                end
            end
        end

    end

    # If the data extracting is on, we want to save the extracted data.
    if data_extracting

        # Pack the extracted vectors into an array
        extracted_data = Any[ car_id_vec, re_frame_vec, de_frame_vec,
                              cost_keep_vec, cost_change_vec, cost_collision_vec, prob_safe_vec,
                              subj_v_vec, adj_lead_v_vec, adj_lead_rd_vec, adj_lag_v_vec, adj_lag_rd_vec,
                              decision_vec ];
        data_names = [ :id, :re_frame, :de_frame, :c_keep, :c_change, :c_collision, :p_safe,
                       :subj_v, :adj_lead_v, :adj_lead_rd, :adj_lag_v, :adj_lag_rd,
                       :decision ];
        # Pack the data into a dataframe
        extracted_df = DataFrame(extracted_data, data_names);
        sorted_df = sort(extracted_df);
        # Write this dataframe into a csv file.
        println("Write the data frame to csv files.");
        CSV.write("./Data/"*whichdataset*"_"*analyze_subject*".csv", extracted_df);
        CSV.write("./Data/"*whichdataset*"_"*analyze_subject*"_sorted.csv", sorted_df);
    end

    # obsolete{
    # # Finally we want to save the created exclusion list.
    # if create_exclusion_list
    #     println("There are ", length(exclusion_list), " cases to exclude. ")
    #     JLD.save("./Data/lanekeep_exclusion_list.jld", "exclusion_list", exclusion_list)
    # end
    # }
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    lanedecision_detailed_analysis()
end
