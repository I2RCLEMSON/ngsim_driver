include("./Supplement/core.jl");

# """
# Get the number of lane changes in lane 1--5 due to slow lead vehicle
# """
function merging_cases()
    # We first ask the use to specify which dataset is used.
    myngsim = ask_use_which_dataset()

    # From here we start the real meat of this function.
    # Start a timer
    tic();
    # Initialize the containers for storing the merging cases.
    merge_cases = Dict{Int, Vector{Int}}();    # Dict: egoid ==> [start frame, merge frame, end frame]

    # Initialize a counter
    n_cases = 0;
    n_centerlane6 = 0;
    n_centerlane5 = 0;

    #For a vehicle in all the vehicles:
    for egoid in keys(tdraw_my.car2start)  # tdraw_my is loaded by using MyNGSIM

        # Get the starting row of the ego vehicle in tdraw_my
        ego_start_row = tdraw_my.car2start[egoid];
        # Get the total number of frames of the ego vehicle in tdraw_my
        total_n_frames = tdraw_my.df[ego_start_row, :n_frames_in_dataset];
        # Compute the ending row of the ego vehicle in tdraw_my
        ego_end_row = ego_start_row + total_n_frames - 1; # df is organized by clustering same vehicle together.
        # Get the initial lane of the ego
        ego_init_laneid = tdraw_my.df[ego_start_row, :lane];
        # Get the lane ids of the whole trajectory of the ego.
        ego_laneids = tdraw_my.df[ego_start_row:ego_end_row, :lane]
        # Get the class of the ego
        # We use trajdata_my which is loaded by using MyNGSIM
        ego_class = trajdata_my.defs[egoid].class;
        # We also loaded the AgentClass module by using MyNGSIM. AgentClass has MOTORCYCLE, CAR, TRUCK, and PEDESTRIAN.
        # We only want to investigate CAR entering the view from lane 7 (the on-ramp)
        if ego_class == AgentClass.CAR && ego_init_laneid == 7

            # We start recording.
            ego_start_row_m = ego_start_row;

            # We find when the vehicle enters lane 6 or lane 5
            new_lane = find(laneid -> laneid <= 6, ego_laneids);
            entering = new_lane[1];
            if ego_laneids[entering] == 6
                n_centerlane6 += 1;
            elseif ego_laneids[entering] == 5
                println( "Cars directly enters lane 5: ", egoid);
                n_centerlane5 += 1;
            else
                error("Directly enter lane 4--1 ?")
            end

            # Get the starting frame here
            ego_start_frame = tdraw_my.df[ego_start_row_m, :frame];

            # We find when the merging is completed; the ego enters lane  5
            merge_completed = find(laneid -> laneid == 5, ego_laneids);
            @assert !isempty(merge_completed) "The merging vehicle did not merge into lane 5."
            # We stop recording here with some margin.
            # Add a margin to the appearing duration of the ego.
            ego_end_row_m = merge_completed[1] + ego_start_row;
            # Get the completing frame here
            ego_merge_frame = tdraw_my.df[ego_end_row_m, :frame];
            ego_end_frame = ego_merge_frame + 10;

            merge_cases[egoid] = [ego_start_frame, ego_merge_frame ,ego_end_frame];

            # println("Car id: ", egoid);
            # println("Merging trajectory range: ", merge_cases[egoid]);
            # println("Merging trajectory length: ", ego_end_frame - ego_start_frame + 1);
            # println(" ");

            n_cases += 1;

        end
    end
    # Print the elapsed time.
    toc();
    tic();
    # A final touch on saving the lane decision cases.
    println("There are ", n_cases, " merging cases");
    println("There are ", n_centerlane6, " enters lane 6");
    println("There are ", n_centerlane5, " enters lane 5");
    println("Save the results...");
    JLD.save("./Data/merge_cases_"*myngsim*".jld", "merge_cases",merge_cases);
    toc();
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    lanedecision_detailed_analysis()
end
