using DataFrames
using CSV
using StatsBase
using PyPlot
using PyCall
using JLD

pyplt = pyimport("matplotlib.pyplot");
pyimport("mpl_toolkits.mplot3d");

# This file contains functions for dealing with imbalanced classes.
# The main file is risk_sensitive_SMOTE()


"Standard k-mean clustering algorithm"
function SMOTE(; prob_upper::Float64 = 0.90, prob_lower::Float64 = 0.1,
                 imbalance_ratio::Float64 = 1.0 , k_neigbors::Int64 = 5, k_clusters::Int64 = 3 )

    # ==========================================================================
    # Loading data
    # ==========================================================================
    # Read data

    println("Loading ", "i101_0750_0805_change.csv");
    data_c = DataFrame( CSV.read( "./Data/i101_0750_0805_change.csv" ) );
    println("Loading ", "i101_0805_0820_change.csv");
    data_c = DataFrame( CSV.read( "./Data/i101_0805_0820_change.csv", data_c, append=true ) );
    println("Loading ", "i101_0820_0835_change.csv");
    data_c = DataFrame( CSV.read( "./Data/i101_0820_0835_change.csv", data_c, append=true ) );

    println("Loading ", "i101_0750_0805_keep.csv");
    data_k = DataFrame( CSV.read("./Data/i101_0750_0805_keep.csv") );
    println("Finish loading")
    # In our data, the change cases are fewer. We want to synthesize change cases
    # to increase their number to match the number of the keep cases to the imbalance_ratio.
    n_required = floor( Int, imbalance_ratio * length( data_k[:id] ) - length( data_c[:id] ) );

    # ==========================================================================
    # Oversampling data_c
    # ==========================================================================

    # In the following we only do the oversampling for data_c.
    # Take feature data of data_c, these are vectors
    c_change_lc = Array{Float64, 1}( data_c[:c_change] );
    c_collision_lc = Array{Float64, 1}( data_c[:c_collision] );
    c_keep_lc = Array{Float64, 1}( data_c[:c_keep] );
    p_safe_lc = data_c[:p_safe]; # We bound the probability to [0.01, 0.99]
    p_safe_lc = [ minimum( [ maximum([ p_safe_lc[i], 0.01 ]), 0.99 ] ) for i = 1:length(p_safe_lc) ];
    p_safe_lc = Array{Float64, 1}(p_safe_lc);

    # We define the features
    x1_lc = c_change_lc;
    x2_lc = c_collision_lc;
    x3_lc = c_keep_lc;
    x4_lc = p_safe_lc;
    # Label data, this is a vector.
    y_lc = data_c[:decision];
    y_lc = Array{Int, 1}(y_lc);

    # Get the additional information in the data frame for later use
    v0_lc = Array{Float64, 1}( data_c[:subj_v] );
    v2_lc = Array{Float64, 1}( data_c[:adj_lead_v] );
    Δd2_lc = Array{Float64, 1}( data_c[:adj_lead_rd] );
    v3_lc = Array{Float64, 1}( data_c[:adj_lag_v] );
    Δd3_lc = Array{Float64, 1}( data_c[:adj_lag_rd] );

    # Pack the features
    Xy_lc_mat = hcat( x1_lc, x2_lc, x3_lc, x4_lc, y_lc, v0_lc, v2_lc, Δd2_lc, v3_lc, Δd3_lc );

    # Get normalized features. The normalized features are used for clustering.
    # Note here we use a combination of the original features x.
    cluster_x1 = normalize_feature( x1_lc - x3_lc );
    cluster_x2 = normalize_feature( x2_lc - x3_lc );
    cluster_x3 = normalize_feature( x4_lc );
    cluster_pool_all = hcat( cluster_x1, cluster_x2, cluster_x3 );

    # Risk-sensitive SMOTE
    # We use the probability of being safe as the indicator of risk and we only want
    # to do SMOTE to the risk-seeking (rsk) cases.
    # We only synthesize new cases using SMOTE for risk-seeking cases.
    rsk_inds = find( x-> x < prob_upper && x > prob_lower, x4_lc );
    Xy_rsk_lc_mat = Xy_lc_mat[ rsk_inds, : ];
    # We divide the each row in Xy_rsk_lc_mat into two portions for convinence.
    pool = Xy_rsk_lc_mat[ :, 1:4 ];
    v_and_Δd = Xy_rsk_lc_mat[ :, 6:10 ];
    cluster_pool = cluster_pool_all[ rsk_inds, : ];
    # A row in pool corresponds to a row in cluster_pool.

    # The non-risk-seeking lane-change cases are
    nrsk_inds = setdiff( 1:length(x4_lc), rsk_inds );
    Xy_nrsk_lc_mat = Xy_lc_mat[ nrsk_inds, : ];

    # Use k_mean_clustering for distributing synthesized samples to sparse region.
    min_clu_size = 0;
    k_mean_iter = 0;
    clusters = Array{Array{Int64, 1}, 1}();

    while min_clu_size <= 8 && k_mean_iter <= 100

        (clusters, ) = k_mean_clustering( cluster_pool, k_clusters, cens_dist_tol=0.001 );
        # Compute the minimal size of the clusters.
        cluster_sizes = [ length( clusters[i] ) for i = 1:length( clusters ) ];
        min_clu_size = minimum( cluster_sizes );

    end

    # Plot clustering
    ion();
    figure();
    ax = pyplt[:axes](projection = "3d");
    for i = 1:k_clusters
        mylabel = "cluster"*string(i);
        clus_points = pool[ clusters[i], : ];
        ax[:scatter]( clus_points[:, 1] - clus_points[:, 3],
                      clus_points[:, 2] - clus_points[:, 3],
                      clus_points[:, 4], label = mylabel );

    end
    legend();
    xlabel("v_change - v_keep");
    ylabel("c_collision - v_keep");
    zlabel("p_safe");
    zlim([0,1]);

    # Compute the mean distance for each cluster using cluster_pool
    sparsities = Vector{Float64}();
    cluster_populations = Vector{Int64}();
    cluster_volumns = Vector{Float64}();

    for a_cluster in clusters

        # a_cluster is a array of indices. We first use these indices to retrieve
        # the points in the dataset cluster_pool.
        clustered_points = cluster_pool[ a_cluster, : ];

        # Compute the sparsity of this cluster
        ( sparsity, cluster_population, cluster_volumn ) = cluster_sparsity( clustered_points );
        push!(sparsities, sparsity);
        push!(cluster_populations, cluster_population);
        push!(cluster_volumns, cluster_volumn);

    end
    # Sampling weights of each cluster are
    sam_weights = sparsities ./ sum( sparsities );
    println("sampling weights of each cluster: ", sam_weights);

    # Distribute the number of required samples to each cluster.
    n_required_in_clusters = floor.( Int, n_required .* sam_weights[1:end-1] );
    n_required_end = n_required - sum( n_required_in_clusters );
    push!( n_required_in_clusters,  n_required_end);
    println("Required number of samples in each cluster: ", n_required_in_clusters);

    # Use SMOTE to synthesize samples in each cluster.
    y_label = 1;    # We only do the oversampling for lane-changing.

    # Initialize the container for the synthesized samples.
    Xy_lc_added = [];

    for ind_cluster = 1:length(clusters)

        # We initialize added samples
        n_added = 1;
        while n_added <= n_required_in_clusters[ ind_cluster ]

            # Randomly sample one case in the cluster using cluster_pool.
            cluster_datapoints = cluster_pool[ clusters[ ind_cluster ], : ];
            mysample_ind = rand( 1:size(cluster_datapoints, 1) );
            # Find the k-nearest neigbor of this sample.
            myknn_inds = k_nearest_neigbor(cluster_datapoints, mysample_ind, k_neigbors);

            # Randomly select one neigbor from the knns. Note the first one is mysample itself.
            one_ind = rand( 2:length(myknn_inds) );
            myneighbor_ind = myknn_inds[ one_ind ];

            # We interpolate one synthetic sample between mysample and myneighbor.
            # Randomly select a step size in (0, 0.99).
            stepsize = rand( 0.01:0.01:0.99 );

            # Interpolate using the original dataset, i.e., pool.
            # Note we do NOT use the transformed dataset cluster_pool here.
            datapointsX = pool[ clusters[ ind_cluster ], : ];
            newsample_X = ( datapointsX[ mysample_ind, : ] +
                            stepsize .* ( datapointsX[ myneighbor_ind, : ] - datapointsX[ mysample_ind, : ] ) );
            # Synthesize new data for the v_and_Δd portion.
            datapointsvΔd = v_and_Δd[ clusters[ ind_cluster ], :  ];
            newsample_vΔd = ( datapointsvΔd[ mysample_ind, : ] +
                              stepsize .* ( datapointsvΔd[ myneighbor_ind, : ] - datapointsvΔd[ mysample_ind, : ] ) );

            newsample_Xy = vcat( newsample_X, vcat( [ y_label ], newsample_vΔd ) );
            # Add the new samples to new sample matrix
            Xy_lc_added = vcat( Xy_lc_added, newsample_Xy' );

            # Spin once
            n_added += 1;

        end
    end
    println("Finish synthesize samples");

    # Plot the original data points and the synthesized data points.
    ion();
    figure();
    ax2 = pyplt[:axes](projection = "3d");
    mylabel = "original";
    ax2[:scatter]( Xy_lc_mat[:, 1] - Xy_lc_mat[:, 3],
                   Xy_lc_mat[:, 2] - Xy_lc_mat[:, 3],
                   Xy_lc_mat[:, 4], label = mylabel);
    mylabel = "Synthesized";
    ax2[:scatter]( Xy_lc_added[:, 1] - Xy_lc_added[:, 3],
                   Xy_lc_added[:, 2] - Xy_lc_added[:, 3],
                   Xy_lc_added[:, 4], label = mylabel)
    legend();
    xlabel("v_change - v_keep");
    ylabel("c_collision - v_keep");
    zlabel("p_safe");
    zlim([0,1]);

    # Save the sythesized data
    Xy_names = [:c_change, :c_collision, :c_keep, :p_safe, :decision, :subj_v, :adj_lead_v, :adj_lead_rd, :adj_lag_v, :adj_lag_rd ];
    # Pack the data into a dataframe
    Xy_lc_syn_df = DataFrame(Xy_lc_added, Xy_names);
    println("Size of the synthesized lane changing data is ", size(Xy_lc_added, 1) );
    # Write this dataframe into a csv file.
    println("Write the synthesized lane changing data to csv files.");
    CSV.write("./Data/synthesized_change.csv", Xy_lc_syn_df);

    # Save the risk-seeking lane change cases.
    Xy_rsk_lc_df = DataFrame(Xy_rsk_lc_mat, Xy_names);
    println("Size of the risk-seeking lane change data is ", size( Xy_rsk_lc_mat, 1 ) );
    # Write this dataframe into a csv file.
    println("Write the risk-seeking lane change data to csv files.");
    CSV.write( "./Data/riskseeking_lanechange.csv", Xy_rsk_lc_df );

    # Save the non-risk-seeking lane change cases.
    Xy_nrsk_lc_df = DataFrame(Xy_nrsk_lc_mat, Xy_names);
    println("Size of the non-risk-seeking lane change data is ", size( Xy_nrsk_lc_mat, 1 ) );
    # Write this dataframe into a csv file.
    println("Write the non-risk-seeking lane change data to csv files.");
    CSV.write( "./Data/nonriskseeking_lanechange.csv", Xy_nrsk_lc_df );

    # Save the clustering info
    JLD.save( "./Data/lanechange_clustering.jld", "clusters", clusters,
                                                  "sparsities", sparsities,
                                                  "cluster_populations", cluster_populations,
                                                  "cluster_volumns", cluster_volumns);

    # ==========================================================================
    # undersampling data_k
    # ==========================================================================
    # In the following we only do the undersampling for data_k.
    # Take feature data of data_k, these are vectors
    c_change_lk = Array{Float64, 1}( data_k[:c_change] );
    c_collision_lk = Array{Float64, 1}( data_k[:c_collision] );
    c_keep_lk = Array{Float64, 1}( data_k[:c_keep] );
    p_safe_lk = data_k[:p_safe]; # We bound the probability to [0.01, 0.99]
    p_safe_lk = [ minimum( [ maximum([ p_safe_lk[i], 0.01 ]), 0.99 ] ) for i = 1:length(p_safe_lk) ];
    p_safe_lk = Array{Float64, 1}(p_safe_lk);

    # We define the features
    x1_lk = c_change_lk;
    x2_lk = c_collision_lk;
    x3_lk = c_keep_lk;
    x4_lk = p_safe_lk;
    # Label data, this is a vector.
    y_lk = data_k[:decision];
    y_lk = Array{Int, 1}(y_lk);

    # Get the additional information in the data frame for later use
    v0_lk = Array{Float64, 1}( data_k[:subj_v] );
    v2_lk = Array{Float64, 1}( data_k[:adj_lead_v] );
    Δd2_lk = Array{Float64, 1}( data_k[:adj_lead_rd] );
    v3_lk = Array{Float64, 1}( data_k[:adj_lag_v] );
    Δd3_lk = Array{Float64, 1}( data_k[:adj_lag_rd] );

    # Pack the features
    Xy_lk_mat = hcat(x1_lk, x2_lk, x3_lk, x4_lk, y_lk, v0_lk, v2_lk, Δd2_lk, v3_lk, Δd3_lk);

    # Random undersampling
    ( Xy_lk_undersam, ) = random_undersample(Xy_lk_mat, target_ratio=imbalance_ratio);

    # Pack the data into a dataframe
    Xy_lk_undersam_df = DataFrame(Xy_lk_undersam, Xy_names);
    println("Size of lane-keep training and validating data is ", size(Xy_lk_undersam, 1) );
    # Write this dataframe into a csv file.
    println("Write the keep data frame to csv files.");
    CSV.write("./Data/lanekeep.csv", Xy_lk_undersam_df);

end

"""
cluster_sparsity( D ): given a dataset D, where each data point is a row, compute
the inverse of the density of this dataset.
"""
function cluster_sparsity( dataset::Matrix{Float64}; sudo_dim = 3.0 )

    # Compute the mean distance between any two points in this cluster.
    dist_mat = distance_matrix( dataset );
    # Flatten this matrix and ignore the diagnal elements.
    dist_vec = Vector{Float64}();
    datasize = size(dist_mat, 1);

    for i = 1:datasize

        cols = setdiff( 1:datasize, i );
        dist_vec = vcat( dist_vec, dist_mat[ i, cols ] );

    end

    # Compute the mean distance.
    dist_mean = mean( dist_vec );
    sudo_volumn = dist_mean ^ sudo_dim;

    # Compute the sparsity
    sparsity = sudo_volumn / datasize;

    # Return
    ( sparsity, datasize, sudo_volumn )

end


"Output normalized feature x"
function normalize_feature(x::Vector{Float64})::Vector{Float64}

    x_norm = (x - minimum(x)) ./ maximum( x - minimum(x) )

end

"Find the k nearest_neigbors of a sample point"
function k_nearest_neigbor(feature_pool::Matrix{Float64}, sample_ind::Int64, k::Int64)

    # We make sure the sample_ind is within the length of the feature_pool
    @assert sample_ind > 0 && sample_ind <= size(feature_pool, 1) "The sample is not in the pool."

    # Update k to make sure it is not greater than the number of samples in the pool.
    k = minimum( [ k, size(feature_pool, 1) - 1 ] );

    # We use square of euclidean distance (sq_dist) to measure closeness.
    diff_mat = feature_pool - ones( size(feature_pool, 1) ) * feature_pool[ sample_ind, : ]';
    sq_dist_vec = [ sum( diff_mat[i,:] .^ 2 ) for i = 1:size(diff_mat, 1) ];
    # Let us sort the squared distances but record the original indices instead of the sorted distances.
    neigbor_inds = sortperm( sq_dist_vec );
    # We retrieve the first k+1 neigbors since, the sample itself is included.
    knn_inds = neigbor_inds[1: k+1 ]

end

"Random undersampling"
function random_undersample(dataset::Matrix{Float64}; target_ratio::Float64=0.8)

    # We randomly select a row of the dataset and exclude this row.
    n_org_rows = size(dataset, 1);
    n_target_rows = floor( Int, n_org_rows * target_ratio );
    n_deleterows = n_org_rows - n_target_rows;

    # Randomly select the distinct indices of the rows to be deleted.
    deleterow_inds = sample( 1:n_org_rows, n_deleterows, replace=false );
    # The new matrix excludes the selected rows.
    targetrow_inds = setdiff(1:n_org_rows, deleterow_inds);
    new_dataset = dataset[ targetrow_inds, : ];

    # Return
    (new_dataset, deleterow_inds)

end

"Use boxplot to derive upper lower limit of vector x"
function boxplot_x(x::Vector{Float64})::Tuple{Float64, Float64}

    sort!(x);
    q25_ind = length( x ) ÷ 4;
    q75_ind = length( x ) ÷ 4 * 3;
    iqr = x[ q75_ind ] - x[ q25_ind ];
    lo_whisker = x[ q25_ind ] - 1.5 * iqr;
    hi_whisker = x[ q75_ind ] + 1.5 * iqr;

    (lo_whisker, hi_whisker)
end

"k_means_clustering( d, k): separate the data points in d into k clusters in the feature space"
function k_mean_clustering(feature_pool::Matrix{Float64}, k::Int64;
                           cens_dist_tol::Float64 = 0.01,  maxcount::Int64 = 1000,
                           display::Bool = true )

    # In the feature_pool, each row is a data points, each column is a feature.
    # Randomly choose k data points as the k centroids.
    datasize = size( feature_pool, 1 );
    n_feature = size( feature_pool, 2);
    cen_inds = sample( 1:datasize, k, replace=false );
    centroids = feature_pool[ cen_inds, : ];

    last_centroids = zeros(k, n_feature);
    # Initialize empty clusters
    clusters = [ Vector{Int64}() for i = 1:k ];
    # Compute the squared distances between the centroids and the last centroids
    cens_dist = sum_centriods_squared_distance(centroids, last_centroids);

    # Start the main iteration.
    count = 0;
    if display
        println(" Start k-means clustering")
    end

    while cens_dist > cens_dist_tol && count <= maxcount

        # spin the counter
        count += 1;

        if display
            println("count: ", count)
        end

        # We initialize the corresponding k clusters as an array of empty sets. The sets are used
        # to store the indices of the data points in the feature_pool.
        clusters = [ Vector{Int64}() for i = 1:k ];
        # ith centroid in centroids corresponds to ith cluster in clusters.

        # Step 1: Assign data points to their closest centroid.
        for ind in 1:datasize

            # Get the current point
            mypoint = feature_pool[ ind, : ];
            # Compute the squared euclidean distance between each data point in the pool w.r.t each centroids.
            diff_mat = centroids - ones( size(centroids, 1) ) * mypoint';
            sq_dist_vec = [ sum( diff_mat[i,:] .^ 2 ) for i = 1:size(diff_mat, 1) ];

            # We assign this point to the cluster where it has minimal squared distance to the centroid.
            chosen_cluster =  indmin( sq_dist_vec );
            # Add the index of this point to the chosen cluster.
            push!( clusters[ chosen_cluster ], ind );
        end

        # Step 2: Update the centroid of the k clusters.
        last_centroids = centroids;
        centroids = zeros(k, n_feature);
        for clus_ind = 1:k

            # Retrieve the points in the current cluster.
            clustered_points = feature_pool[ clusters[ clus_ind ], : ];
            # We now compute the new centroid of this cluster.
            centroids[ clus_ind, : ] = [ mean( clustered_points[:, col ] ) for col = 1:size(clustered_points, 2) ];

        end

        # Update the sum of distances between the current centroids and the last centroids.
        cens_dist = sum_centriods_squared_distance(centroids, last_centroids);

    end
    ( clusters, centroids )

end

"""
distance_matrix( D ): compute the euclidean distance between any two data points in
the dataset D in which each row is a data point.
"""
function distance_matrix( dataset::Matrix{Float64} )::Matrix{Float64}

    datasize = size( dataset, 1 );
    featuresize = size( dataset, 2 );

    # Initialize the distance matrix
    dist_mat = Matrix{Float64}( datasize, datasize );

    # Compute the distance between any two data points one by one.
    for i = 1:datasize, j = 1:datasize

        datapoint_1 = dataset[ i, : ];
        datapoint_2 = dataset[ j, : ];
        # Compute dimension-wise differences
        diff = datapoint_1 - datapoint_2;
        # Compute the distance between datapoint_1 and datapoint_2
        dist_mat[i, j] = sqrt( sum( diff .^ 2.0 ) );

    end

    # Return
    dist_mat
end

"""
sum_centriods_squared_distance(X, Y): given a list of current centriods X, where each centroid is a row, and
a list of the last centroids Y, compute the sum of the distances between the corresponding current and last centroids.
"""
function sum_centriods_squared_distance( current_centroids::Matrix{Float64}, last_centroids::Matrix{Float64})::Float64

    # We use squared distance for efficiency.
    diff_mat = current_centroids - last_centroids;
    sq_dist_vec = [ sum( diff_mat[i,:] .^ 2 ) for i = 1:size(diff_mat, 1) ];

    sum_sq_dist = sum( sq_dist_vec );
end

# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    SMOTE()
end
