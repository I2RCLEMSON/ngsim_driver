# Include a small library
include("./Supplement/analysis.jl")

"""
k_fold_cross_validation(filename) loads csv data specified in the filename
and does what its name indicates.
k: the number of training-validating segments
k_iter: the number of folding
repeat: the number of running the whole process
"""
function k_fold_cross_validation(;k::Int64 = 10, k_iter::Int64 = 1, repeat::Int64 = 100)
    # This is the main function in this file.


    # Load the data from the filename
    rsk_lc_df = DataFrame( CSV.read("./Data/riskseeking_lanechange.csv") );
    nrsk_lc_df = DataFrame( CSV.read("./Data/nonriskseeking_lanechange.csv") );
    lk_df = DataFrame( CSV.read( "./Data/lanekeep.csv" ) );

    clusters = JLD.load( "./Data/lanechange_clustering.jld", "clusters" );
    clu_sparsities = JLD.load( "./Data/lanechange_clustering.jld", "sparsities" );
    clu_popus = JLD.load( "./Data/lanechange_clustering.jld", "cluster_populations");
    clu_vols = JLD.load( "./Data/lanechange_clustering.jld", "cluster_volumns");

    syn_lc_df = DataFrame( CSV.read("./Data/synthesized_change.csv") );


    # Here we start the k-fold train-test cycle
    # Initialize some data containers.
    para_name_toledo = [:beta1, :beta2, :beta3, :beta4, :beta5, :beta6, :beta7, :beta8, :beta9];
    para_name = [:gamma1, :gamma2, :gamma3, :gamma4, :gamma5, :gamma6, :gamma7, :gamma8, :gamma9];

    parameter_df_toledo = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name_toledo);
    parameter_df_rtx0 = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);
    parameter_df_rtx1 = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);
    # parameter_df_rtx3 = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);
    # parameter_df_rtx4 = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);
    parameter_df_rtx7 = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);
    parameter_df_rtx = DataFrame([ col = Vector{Float64}() for i = 1:length(para_name) ], para_name);


    confusion_df_toledo = DataFrame();
    confusion_df_rtx0 = DataFrame();
    confusion_df_rtx1 = DataFrame();
    # confusion_df_rtx3 = DataFrame();
    # confusion_df_rtx4 = DataFrame();
    confusion_df_rtx7 = DataFrame();
    confusion_df_rtx = DataFrame();
    confusion_df_ttc = DataFrame();

    confusion_df_toledo_orig = DataFrame();
    confusion_df_rtx0_orig = DataFrame();
    confusion_df_rtx1_orig = DataFrame();
    confusion_df_rtx7_orig = DataFrame();
    confusion_df_rtx_orig = DataFrame();
    confusion_df_ttc_orig = DataFrame();

    Toledo_roc_curves = zeros(2, 100, repeat );
    RTx0_roc_curves = zeros(2, 100, repeat);
    RTx1_roc_curves = zeros(2, 100, repeat );
    RTx7_roc_curves = zeros(2, 100, repeat );
    RTx_roc_curves = zeros(2, 100, repeat );
    TTC_roc_curves = zeros(2, 100, repeat );

    Toledo_roc_curves_orig = zeros(2, 100, repeat );
    RTx0_roc_curves_orig = zeros(2, 100, repeat );
    RTx1_roc_curves_orig = zeros(2, 100, repeat );
    RTx7_roc_curves_orig = zeros(2, 100, repeat );
    RTx_roc_curves_orig = zeros(2, 100, repeat );
    TTC_roc_curves_orig = zeros(2, 100, repeat );


    train_progresses_rtx0 = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();
    train_progresses_rtx1 = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();
    # train_progresses_rtx3 = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();
    # train_progresses_rtx4 = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();
    train_progresses_rtx7 = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();
    train_progresses_rtx = Array{ Tuple{Vector{Int64}, Vector{Float64}}, 1 }();

    for rep = 1:repeat

        # Hold out a share of original data for testing.
        # Since this split involves randomness, we want to do new split at each
        # repetition.
        # Note only the original data are used in this split, hence the test set
        # contains only the original data.
        (trainvalidate_df, test_df) = split_trainvalidate_test( rsk_lc_df, nrsk_lc_df,
                                                                lk_df, clusters,
                                                                clu_sparsities,
                                                                clu_popus,
                                                                clu_vols,
                                                                display = false );

        # Here we add the synthesized data to the trainvalidate_df
        trainvalidate_df = vcat( syn_lc_df,  trainvalidate_df );

        # Randomly shuffle the rows of the dataset.
        shuffled = trainvalidate_df[ randperm( size(trainvalidate_df, 1) ), : ];

        # Split the dataset into k groups
        gs = size(shuffled, 1) ÷ k;  # group size.
        splitted = [df = DataFrame() for i = 1:k];
        # Splitting the shuffled dataset for the first k-1 groups.
        for i = 1:k-1
            d_start = ( i-1 ) * gs + 1;
            d_end = i * gs;
            splitted[i] = shuffled[ d_start:d_end, : ];
        end
        # The last group collects the remaining data.
        splitted[k] = shuffled[ ( (k-1)*gs+1 ):end, : ];

        # We start training the logistic regression
        println("Repetition ", rep);

        ## Upper bound k_iter for safety.
        k_iter = minimum( [k_iter, k] );

        # Initialize the best model parameters and best f1-scores
        best_para_rtx0 = Vector{Float64}();
        best_f1_rtx0 = 0.0;

        best_para_rtx1 = Vector{Float64}();
        best_f1_rtx1 = 0.0;

        best_para_rtx7 = Vector{Float64}();
        best_f1_rtx7 = 0.0;

        best_para_rtx = Vector{Float64}();
        best_f1_rtx = 0.0;

        best_para_toledo = Vector{Float64}();
        best_f1_toledo = 0.0;

        for i = 1:k_iter
            # Take the ith group as the hold out and train the model using the remaining
            # data.
            validatedata = splitted[i];

            # Pool the training data.
            # We first initialize an empty traindata data frame but with correct column names.
            colnames = names(validatedata);
            # Initialize the empty traindata
            traindata = DataFrame([ col = Any[] for i = 1:length(colnames) ], colnames);
            # Add the remaining groups to the traindata
            for j in setdiff(1:k, i)
                traindata = vcat( traindata, splitted[j] );
            end

            # We start training the logistic regression
            println("    Test group is ", i);

            # Specify step size for gradient ascent
            stepsize = [1e-4, 1e-4, 1e-4, 1e-3, 1e-3, 1e-4, 1e-4, 1e-5, 1e-5];
            fittolerance = 0.0001

            # Train zero-parameter RTx (RTx0)
            # Show progress
            println("        Fitting RTx0");
            ( model_parameters_rtx0, train_progress_rtx0 ) = model_fit( traindata, model = "RTx0",
                                                                      stepsize=stepsize,
                                                                      tolerance = fittolerance );
            push!( parameter_df_rtx0, model_parameters_rtx0 );
            push!( train_progresses_rtx0, train_progress_rtx0 );

            # Train one-parameter RTx (rtx1)
            # Show progress
            println("        Fitting RTx1");
            ( model_parameters_rtx1, train_progress_rtx1 ) = model_fit( traindata, model = "RTx1",
                                                                      stepsize=stepsize,
                                                                      tolerance = fittolerance );
            push!( parameter_df_rtx1, model_parameters_rtx1 );
            push!( train_progresses_rtx1, train_progress_rtx1 );

            # Train seven-parameter RTx (rtx7)
            # Show progress
            println("        Fitting RTx7");
            ( model_parameters_rtx7, train_progress_rtx7 ) = model_fit( traindata, model = "RTx7",
                                                                      stepsize=stepsize,
                                                                      tolerance = fittolerance );
            push!( parameter_df_rtx7, model_parameters_rtx7 );
            push!( train_progresses_rtx7, train_progress_rtx7 );

            # Train RTx
            # Show progress
            println("        Fitting RTx");
            ( model_parameters_rtx, train_progress_rtx ) = model_fit( traindata, model = "RTx",
                                                                      stepsize=stepsize,
                                                                      tolerance = fittolerance );
            push!( parameter_df_rtx, model_parameters_rtx );
            push!( train_progresses_rtx, train_progress_rtx );

            # Train Toledo
            println("        Fitting Toledo");
            model_parameters_toledo = toledo_model_fit( traindata );
            push!( parameter_df_toledo, model_parameters_toledo );

            # We are validate the model.
            # Testing zero-parameter RTx.
            (RTx0_perfm_df, _) = model_test( validatedata, model_parameters_rtx0 );
            # Update only when the best f1-score so far is obtained.
            if RTx0_perfm_df[1, :f1_score] > best_f1_rtx0
                best_f1_rtx0 = RTx0_perfm_df[1,:f1_score];
                best_para_rtx0 = model_parameters_rtx0;
                println("        best_f1_rtx0=", best_f1_rtx0);
            end

            # Testing one-parameter RTx.
            (RTx1_perfm_df, RTx1_roc) = model_test( validatedata, model_parameters_rtx1 );
            # Update only when the best f1-score so far is obtained.
            if RTx1_perfm_df[1, :f1_score] > best_f1_rtx1
                best_f1_rtx1 = RTx1_perfm_df[1, :f1_score];
                best_para_rtx1 = model_parameters_rtx1;
                println("        best_f1_rtx1=", best_f1_rtx1);
            end

            # Testing seven-parameter RTx.
            (RTx7_perfm_df, RTx7_roc) = model_test( validatedata, model_parameters_rtx7 );
            # Update only when the best f1-score so far is obtained.
            if RTx7_perfm_df[1, :f1_score] > best_f1_rtx7
                best_f1_rtx7 = RTx7_perfm_df[1, :f1_score];
                best_para_rtx7 = model_parameters_rtx7;
                println("        best_f1_rtx7=", best_f1_rtx7);
            end

            # Testing RTx
            (RTx_perfm_df, RTx_roc) = model_test( validatedata, model_parameters_rtx );
            # Update only when the best f1-score so far is obtained.
            if RTx_perfm_df[1,:f1_score] > best_f1_rtx
                best_f1_rtx = RTx_perfm_df[1, :f1_score];
                best_para_rtx = model_parameters_rtx;
                println("        best_f1_rtx=", best_f1_rtx);
            end

            # Testing Toledo gap acceptance model
            (Toledo_perfm_df, Toledo_roc) = toledo_model_test( validatedata, model_parameters_toledo );
            # Update only when the best f1-score so far is obtained.
            if Toledo_perfm_df[1, :f1_score] > best_f1_toledo
                best_f1_toledo = Toledo_perfm_df[1, :f1_score];
                best_para_toledo = model_parameters_toledo;
                println("        best_f1_toledo=", best_f1_toledo);
            end
        end

        # We are testing the model with the original data only (no synthesized).
        # Testing zero-parameter RTx.
        (RTx0_perfm_df_orig, RTx0_roc_orig) = model_test( test_df, best_para_rtx0 );
        # Testing one-parameter RTx.
        (RTx1_perfm_df_orig, RTx1_roc_orig) = model_test( test_df, best_para_rtx1 );
        # Testing seven-parameter RTx.
        (RTx7_perfm_df_orig, RTx7_roc_orig) = model_test( test_df, best_para_rtx7 );
        # Testing RTx
        (RTx_perfm_df_orig, RTx_roc_orig) = model_test( test_df, best_para_rtx );
        # Furthermore, we test the Toledo gap acceptance model
        (Toledo_perfm_df_orig, Toledo_roc_orig) = toledo_model_test( test_df, best_para_toledo );
        # Testing Time-to-collision model
        (TTC_perfm_df_orig, TTC_roc_orig) = ttc_model_test( test_df );

        # Store the result
        if rep == 1
            confusion_df_toledo_orig = Toledo_perfm_df_orig;
            confusion_df_rtx0_orig = RTx0_perfm_df_orig;
            confusion_df_rtx1_orig = RTx1_perfm_df_orig;
            confusion_df_rtx7_orig = RTx7_perfm_df_orig;
            confusion_df_rtx_orig = RTx_perfm_df_orig;
            confusion_df_ttc_orig = TTC_perfm_df_orig;

        else
            confusion_df_toledo_orig = vcat(confusion_df_toledo_orig, Toledo_perfm_df_orig);
            confusion_df_rtx0_orig = vcat(confusion_df_rtx0_orig, RTx0_perfm_df_orig);
            confusion_df_rtx1_orig = vcat(confusion_df_rtx1_orig, RTx1_perfm_df_orig);
            confusion_df_rtx7_orig = vcat(confusion_df_rtx7_orig, RTx7_perfm_df_orig);
            confusion_df_rtx_orig = vcat(confusion_df_rtx_orig, RTx_perfm_df_orig);
            confusion_df_ttc_orig = vcat(confusion_df_ttc_orig, TTC_perfm_df_orig);

        end

        # Store roc_curves
        curve_ind = rep;
        Toledo_roc_curves_orig[ :, :, curve_ind ] = Toledo_roc_orig;
        RTx0_roc_curves_orig[ :, :, curve_ind ] = RTx0_roc_orig;
        RTx1_roc_curves_orig[ :, :, curve_ind ] = RTx1_roc_orig;
        RTx7_roc_curves_orig[ :, :, curve_ind ] = RTx7_roc_orig;
        RTx_roc_curves_orig[ :, :, curve_ind ] = RTx_roc_orig;
        TTC_roc_curves_orig[ :, :, curve_ind ] = TTC_roc_orig;

    end

    # Save the ROC curves now.
    JLD.save( "./Results/Toledo_roc_orig.jld", "Toledo_roc_curves", Toledo_roc_curves_orig );
    JLD.save( "./Results/RTx0_roc_orig.jld", "RTx0_roc_curves", RTx0_roc_curves_orig );
    JLD.save( "./Results/RTx1_roc_orig.jld", "RTx1_roc_curves", RTx1_roc_curves_orig );
    JLD.save( "./Results/RTx7_roc_orig.jld", "RTx7_roc_curves", RTx7_roc_curves_orig );
    JLD.save( "./Results/RTx_roc_orig.jld", "RTx_roc_curves", RTx_roc_curves_orig );
    JLD.save( "./Results/TTC_roc_orig.jld", "TTC_roc_curves", TTC_roc_curves_orig );

    # Save the training progress curves.
    JLD.save( "./Results/train_progresses_rtx0.jld", "train_progresses_rtx0", train_progresses_rtx0 );
    JLD.save( "./Results/train_progresses_rtx1.jld", "train_progresses_rtx1", train_progresses_rtx1 );
    JLD.save( "./Results/train_progresses_rtx7.jld", "train_progresses_rtx7", train_progresses_rtx7 );
    JLD.save( "./Results/train_progresses_rtx.jld", "train_progresses_rtx", train_progresses_rtx );

    # Compute the mean and stds
    stat_df_toledo_orig = compute_mean_std(confusion_df_toledo_orig);
    stat_df_rtx0_orig = compute_mean_std(confusion_df_rtx0_orig);
    stat_df_rtx1_orig = compute_mean_std(confusion_df_rtx1_orig);
    stat_df_rtx7_orig = compute_mean_std(confusion_df_rtx7_orig);
    stat_df_rtx_orig = compute_mean_std(confusion_df_rtx_orig);
    stat_df_ttc_orig = compute_mean_std(confusion_df_ttc_orig);

    stat_df_para_toledo = compute_mean_std(parameter_df_toledo);
    stat_df_para_rtx0 = compute_mean_std(parameter_df_rtx0);
    stat_df_para_rtx1 = compute_mean_std(parameter_df_rtx1);
    stat_df_para_rtx7 = compute_mean_std(parameter_df_rtx7);
    stat_df_para_rtx = compute_mean_std(parameter_df_rtx);

    # Display and save
    println(" ")
    println("RTx0 performance on original data:")
    # println(confusion_df_rtx0_orig)
    println(stat_df_rtx0_orig);
    # Write this dataframe into a csv file.
    CSV.write("./Results/RTx0_performance_dataset_orig.csv", confusion_df_rtx0_orig);
    CSV.write("./Results/RTx0_performance_stats_orig.csv", stat_df_rtx0_orig);

    println(" ")
    println("RTx1 performance on original data:")
    # println(confusion_df_rtx1_orig)
    println(stat_df_rtx1_orig)
    # Write this dataframe into a csv file.
    CSV.write("./Results/RTx1_performance_dataset_orig.csv", confusion_df_rtx1_orig);
    CSV.write("./Results/RTx1_performance_stats_orig.csv", stat_df_rtx1_orig);

    println(" ")
    println("RTx7 performance on original data:")
    # println(confusion_df_rtx7_orig)
    println(stat_df_rtx7_orig)
    # Write this dataframe into a csv file.
    CSV.write("./Results/RTx7_performance_dataset_orig.csv", confusion_df_rtx7_orig);
    CSV.write("./Results/RTx7_performance_stats_orig.csv", stat_df_rtx7_orig);

    println(" ")
    println("RTx performance on original data:")
    # println(confusion_df_rtx_orig)
    println(stat_df_rtx_orig)
    # Write this dataframe into a csv file.
    CSV.write("./Results/RTx_performance_dataset_orig.csv", confusion_df_rtx_orig);
    CSV.write("./Results/RTx_performance_stats_orig.csv", stat_df_rtx_orig);

    println(" ")
    println("Toledo performance on original data:")
    # println(confusion_df_toledo_orig)
    println(stat_df_toledo_orig)
    # Write this dataframe into a csv file.
    CSV.write("./Results/Toledo_performance_dataset_orig.csv", confusion_df_toledo_orig);
    CSV.write("./Results/Toledo_performance_stats_orig.csv", stat_df_toledo_orig);

    println(" ")
    println("TTC performance on original data:")
    # println(confusion_df_ttc_orig)
    println(stat_df_ttc_orig)
    # Write this dataframe into a csv file.
    CSV.write("./Results/TTC_performance_dataset_orig.csv", confusion_df_ttc_orig);
    CSV.write("./Results/TTC_performance_stats_orig.csv", stat_df_ttc_orig);

    # Show the parameters
    println(" ")
    println("Model Toledo parameters")
    # println(parameter_df_toledo)
    println(stat_df_para_toledo)
    # Write this dataframe into a csv file.
    CSV.write("./Results/parameters_dataset_toledo.csv", parameter_df_toledo);
    CSV.write("./Results/parameters_stats_toledo.csv", stat_df_para_toledo);

    println(" ")
    println("Model RTx0 parameters")
    # println(parameter_df_rtx0)
    println(stat_df_para_rtx0)
    # Write this dataframe into a csv file.
    CSV.write("./Results/parameters_dataset_rtx0.csv", parameter_df_rtx0);
    CSV.write("./Results/parameters_stats_rtx0.csv", stat_df_para_rtx0);

    println(" ")
    println("Model RTx1 parameters")
    # println(parameter_df_rtx1)
    println(stat_df_para_rtx1)
    # Write this dataframe into a csv file.
    CSV.write("./Results/parameters_dataset_rtx1.csv", parameter_df_rtx1);
    CSV.write("./Results/parameters_stats_rtx1.csv", stat_df_para_rtx1);

    println(" ")
    println("Model RTx7 parameters")
    # println(parameter_df_rtx7)
    println(stat_df_para_rtx7)
    # Write this dataframe into a csv file.
    CSV.write("./Results/parameters_dataset_rtx7.csv", parameter_df_rtx7);
    CSV.write("./Results/parameters_stats_rtx7.csv", stat_df_para_rtx7);

    println(" ")
    println("Model RTx parameters")
    # println(parameter_df_rtx)
    println(stat_df_para_rtx)
    # Write this dataframe into a csv file.
    CSV.write("./Results/parameters_dataset_rtx.csv", parameter_df_rtx);
    CSV.write("./Results/parameters_stats_rtx.csv", stat_df_para_rtx);

end

"""
Use logistic regression to fit RTx model
"""
function model_fit( traindata::DataFrame;
                    stepsize::Vector{Float64} = [1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-4, 1e-5, 1e-5],
                    model::String = "RTx",
                    max_iter::Int64 = 10000,
                    tolerance::Float64 = 0.001,
                    debug::Bool=false,
                    display::Bool = false )

    # ==========================================================================
    # Loading data
    # ==========================================================================
    # Use short notation
    data = traindata;
    # Feature data, these are vectors
    x1 = data[:c_change];
    # Convert the type
    x1 = Array{Float64, 1}(x1);
    x2 = data[:c_collision];
    x2 = Array{Float64, 1}(x2);
    x3 = data[:c_keep];
    x3 = Array{Float64, 1}(x3);
    x4 = data[:p_safe];
    x4 = [ minimum( [ maximum([ x4[i], 0.01 ]), 0.99 ] ) for i = 1:length(x4) ];
    x4 = Array{Float64, 1}(x4);
    # Label data, this is a vector.
    y = data[:decision];
    y = Array{Float64, 1}(y);

    # Pack the features X= [x1, x2, x3, x4] and y into a matrix with each row being [X, y]
    Xy_mat = hcat(x1, x2, x3, x4, y)

    # ==========================================================================
    # Gradient ascent algorithm
    # ==========================================================================
    # Compute the ∇l(γ), where l(γ) is the log-likelihood, and γ is the parameter vector.
    # Initialize the parameter vector γ randomly
    # γ = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    if model == "RTx0"
        γ = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    elseif model == "RTx1"
        γ = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    elseif model == "RTx3"
        γ = [0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    elseif model == "RTx4"
        γ = [1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    elseif model == "RTx7"
        γ = [0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    elseif model == "RTx"
        γ = [0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0];
    else
        error("Model can only be \"RTx0\", \"RTx1\", \"RTx3\", \"RTx4\", \"RTx7\", or \"RTx\"")
    end
    γ_init = γ;
    # Step size
    α = stepsize;
    @assert length(α) == 9 "length of α must be 9";
    # Each αi corresponds to one parameter γ.
    # Initialize a vector to store log-likelihood
    l_vec = Vector{Float64}();
    l = 0;
    push!(l_vec, -max_iter);
    push!(l_vec, 0);
    # Initialize an iteration count
    count = 0;
    # Initialize the hypothesis function
    h_γ = -1*ones( length(y) );
    e_rtx = -1*ones( length(y) )

    while count <= max_iter && abs( l_vec[end] - l_vec[end-1] ) > tolerance
        # The goal is to compute the following.
        #   ∂l      ( (                  ) ∂e_rtx  )
        # ----- = ∑i[ [ y_i - h_γ( x_i ) ] ------- ]
        #   ∂γ_j    ( (                  )  ∂γ_j   )
        # We will do this step by step by using some short notations.
        # The following short notations are vectors.
        γx1 = γ[3] .* ( x1 - x3 + γ[6] );
        γx2 = γ[3] .* ( γ[7] .* x2 - x3 );
        q1 = γ[1] .* ( x1 - x3 + γ[6] ) + γ[2] .* sinh.( γx1 );
        q2 = γ[1] .* ( γ[7] .* x2 - x3 ) + γ[2] .* sinh.( γx2 );
        w = γ[8] + γ[9] .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );
        # Use the above short notations, we have the RTx net advantage signal.
        e_rtx = w .* ( q1 - q2 ) + q2; # This is a vector.
        # The logistic function is
        h_γ =  1.0 ./ ( 1.0 + exp.( -e_rtx ) );
        h_γ = [ minimum( [ maximum( [ h_γ_i, 1e-10 ] ), 1.0-1e-10 ] ) for h_γ_i in h_γ];  # This is a vector.
        # Compute the log-likelihood.
        l = sum( y .* log.( h_γ ) + (1.0 - y) .* log.( 1.0-h_γ ) );
        push!(l_vec, l);
        # Show l
        if display
            println("Count: ", count, ", log_likilihood: ", l);
        end

        # We now compute ∂e_rtx / ∂γ_j for each γ_j.
        # Each partial derivative is a vector.
        if model == "RTx0"
            ∂e_∂γ1 = zeros(length(w));
            ∂e_∂γ2 = zeros(length(w));
            ∂e_∂γ3 = zeros(length(w));
            ∂e_∂γ4 = zeros(length(w));
            ∂e_∂γ5 = zeros(length(w));
            ∂e_∂γ6 = zeros(length(w));
            ∂e_∂γ7 = zeros(length(w));
            ∂e_∂γ8 = zeros(length(w));
            ∂e_∂γ9 = zeros(length(w));

        elseif model == "RTx1"
            ∂e_∂γ1 = zeros(length(w));
            ∂e_∂γ2 = zeros(length(w));
            ∂e_∂γ3 = zeros(length(w));
            ∂e_∂γ4 = zeros(length(w));
            ∂e_∂γ5 = zeros(length(w));
            ∂e_∂γ6 = zeros(length(w));
            ∂e_∂γ7 = ( γ[1] .* x2 + γ[2].*γ[3].*x2 .* cosh.( γx2 ) ).* ( 1 - w );
            ∂e_∂γ8 = zeros(length(w));
            ∂e_∂γ9 = zeros(length(w));

        elseif model == "RTx3"
            ∂e_∂γ1 = γ[7] .* x2 - x3 + ( γ[6] - γ[7] .* x2 + x1 ) .* w;

            ∂e_∂γ2 = sinh.( γx2 ) + ( sinh.( γx1 ) - sinh.( γx2 ) ) .* w;

            ∂e_∂γ3 = ( γ[2] .* ( γ[7] .* x2 - x3 ) .* cosh.( γx2 ) +
                       ( γ[2] .* (γ[6] + x1 - x3) .* cosh.( γx1 ) -
                         γ[2] .* (γ[7] .* x2 - x3) .* cosh.( γx2 ) ) .* w );

            ∂e_∂γ4 = zeros(length(w));

            ∂e_∂γ5 = zeros(length(w));

            ∂e_∂γ6 = zeros(length(w)); # We let γ6 ≡ 0.0.

            ∂e_∂γ7 = zeros(length(w));

            ∂e_∂γ8 = zeros(length(w));

            ∂e_∂γ9 = zeros(length(w));

        elseif model == "RTx4"

            ∂e_∂γ1 = zeros(length(w));
            ∂e_∂γ2 = zeros(length(w));
            ∂e_∂γ3 = zeros(length(w));

            ∂e_∂γ4 = -( -log.(x4) ) .^ γ[5] .* (q1 - q2) .* ( w - γ[8] );

            ∂e_∂γ5 = -γ[4] .* ( -log.(x4) ) .^ γ[5] .* log.( -log.( x4 ) ) .* ( q1 - q2 ) .* ( w - γ[8] );

            ∂e_∂γ6 = zeros(length(w));
            ∂e_∂γ7 = zeros(length(w));

            ∂e_∂γ8 = q1 - q2;

            ∂e_∂γ9 = (q1 - q2) .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );

        elseif model == "RTx7"
            ∂e_∂γ1 = γ[7] .* x2 - x3 + ( γ[6] - γ[7] .* x2 + x1 ) .* w;

            ∂e_∂γ2 = sinh.( γx2 ) + ( sinh.( γx1 ) - sinh.( γx2 ) ) .* w;

            ∂e_∂γ3 = ( γ[2] .* ( γ[7] .* x2 - x3 ) .* cosh.( γx2 ) +
                       ( γ[2] .* (γ[6] + x1 - x3) .* cosh.( γx1 ) -
                         γ[2] .* (γ[7] .* x2 - x3) .* cosh.( γx2 ) ) .* w );

            ∂e_∂γ4 = -( -log.(x4) ) .^ γ[5] .* (q1 - q2) .* ( w - γ[8] );

            ∂e_∂γ5 = -γ[4] .* ( -log.(x4) ) .^ γ[5] .* log.( -log.( x4 ) ) .* ( q1 - q2 ) .* ( w - γ[8] );

            ∂e_∂γ6 = zeros(length(w)); # We let γ6 ≡ 0.0.
            ∂e_∂γ7 = zeros(length(w));

            ∂e_∂γ8 = q1 - q2;

            ∂e_∂γ9 = (q1 - q2) .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );

        elseif model == "RTx"
            ∂e_∂γ1 = γ[7] .* x2 - x3 + ( γ[6] - γ[7] .* x2 + x1 ) .* w;

            ∂e_∂γ2 = sinh.( γx2 ) + ( sinh.( γx1 ) - sinh.( γx2 ) ) .* w;

            ∂e_∂γ3 = ( γ[2] .* ( γ[7] .* x2 - x3 ) .* cosh.( γx2 ) +
                       ( γ[2] .* (γ[6] + x1 - x3) .* cosh.( γx1 ) -
                         γ[2] .* (γ[7] .* x2 - x3) .* cosh.( γx2 ) ) .* w );

            ∂e_∂γ4 = -( -log.(x4) ) .^ γ[5] .* (q1 - q2) .* ( w - γ[8] );

            ∂e_∂γ5 = -γ[4] .* ( -log.(x4) ) .^ γ[5] .* log.( -log.( x4 ) ) .* ( q1 - q2 ) .* ( w - γ[8] );

            # obsolete{
            # ∂e_∂γ6 = ( γ[1] + γ[2].*γ[3] .* cosh.( γx1 ) ) .* w; }
            ∂e_∂γ6 = zeros(length(w)); # We let γ6 ≡ 0.0.

            ∂e_∂γ7 = ( γ[1] .* x2 + γ[2].*γ[3].*x2 .* cosh.( γx2 ) ).* ( 1 - w );

            ∂e_∂γ8 = q1 - q2;

            ∂e_∂γ9 = (q1 - q2) .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );

        else
            error("Model can only be \"RTx0\", \"RTx1\",\"RTx3\", \"RTx4\", \"RTx7\", or \"RTx\"")
        end

        # Put these vectors into an array.
        ∂e_∂γ_array = [ ∂e_∂γ1, ∂e_∂γ2, ∂e_∂γ3, ∂e_∂γ4, ∂e_∂γ5, ∂e_∂γ6, ∂e_∂γ7, ∂e_∂γ8, ∂e_∂γ9 ];

        # Now we are ready to compute α ⋅ ∇l(γ)
        # Initialize a vector to store ∇l(γ)
        stepping = Vector{Float64}(9);
        for j = 1:9
            onestep = α[j] * sum( ( y - h_γ ) .* ∂e_∂γ_array[j] );
            stepping[j] = minimum( [ maximum([onestep, -0.05 ]), 0.05 ] );
        end
        # Here we apply gradient ascent.
        γ += stepping;

        # Bound the γ to some constraints.
        γ[1] = minimum( [ maximum( [ γ[1], -10.0 ] ), 10.0] );
        γ[2] = minimum( [ maximum( [ γ[2], -10.0 ] ), 10.0] );
        γ[3] = minimum( [ maximum( [ γ[3], -10.0 ] ), 10.0] );
        γ[4] = minimum( [ maximum( [ γ[4], 0.01 ] ), 10.0] );
        γ[5] = minimum( [ maximum( [ γ[5], 0.01 ] ), 10.0] );
        γ[6] = minimum( [ maximum( [ γ[6], -10.0 ] ), 10.0] );
        γ[7] = minimum( [ maximum( [ γ[7], 0.0 ] ), 100.0] );
        γ[8] = minimum( [ maximum( [ γ[8], 0.0 ] ), 1.0] );
        γ[9] = minimum( [ maximum( [ γ[9], 0.0 ] ), 1.0-γ[8]] );
        # Update the iteration count
        count += 1;
    end

    # Show the total iteration
    println("        total iteration: ", count);

    # Pack the curve of log-likelihood vs. iteration
    iter = collect(1:count);
    l_iter   = l_vec[3:end];
    itercurve = (iter, l_iter);

    # Return
    ( γ, itercurve )

end

"""
Test the model of logistic regression with RTx model
"""
function model_test( testdata::DataFrame, model_parameters::Vector{Float64}; modelname::String = "RTx", display::Bool = false )

    # Use short notation
    data = testdata;
    γ = model_parameters;

    # Feature data, these are vectors
    x1 = data[:c_change];
    # Convert the type
    x1 = Array{Float64, 1}(x1);
    x2 = data[:c_collision];
    x2 = Array{Float64, 1}(x2);
    x3 = data[:c_keep];
    x3 = Array{Float64, 1}(x3);
    x4 = data[:p_safe];
    x4 = [ minimum( [ maximum([ x4[i], 0.01 ]), 0.99 ] ) for i = 1:length(x4) ];
    x4 = Array{Float64, 1}(x4);
    # Label data, this is a vector.
    y = data[:decision];
    y = Array{Float64, 1}(y);

    # Compute the net advantage of RTx
    # The following short notations are vectors.
    γx1 = γ[3] .* ( x1 - x3 + γ[6] );
    γx2 = γ[3] .* ( γ[7] .* x2 - x3 );
    q1 = γ[1] .* ( x1 - x3 + γ[6] ) + γ[2] .* sinh.( γx1 );
    q2 = γ[1] .* ( γ[7] .* x2 - x3 ) + γ[2] .* sinh.( γx2 );
    w = γ[8] + γ[9] .* exp.( -γ[4] * ( -log.(x4) ) .^ γ[5] );
    # Use the above short notations, we have the RTx net advantage signal.
    netadv = w .* ( q1 - q2 ) + q2; # This is a vector.

    # Make confusion matrix for RTx and EV
    performance = get_confusion_metrics(modelname, y, netadv, display=display);

    # Get the points on the ROC curve.
    roc = get_roc_curve( y, netadv );
    # Compute the area under curve of ROC.
    roc_auc = get_roc_auc( roc );

    # Add the ROC_AUC to the performance dataframe.
    performance[:roc_auc] = [ roc_auc ];

    # Return
    (performance, roc)

end

function get_confusion_metrics(modelname::String, groundtruth::Vector{Float64}, netadv::Vector{Float64}; display::Bool=true )

    y = groundtruth;

    truechange_inds = find(d -> d == 1, y);
    tp = length( find( e-> e >= 0, netadv[ truechange_inds ] ) );
    fn = length( find( e-> e < 0, netadv[ truechange_inds ] ) );

    truekeep_inds = find(d -> d == 0, y);
    tn = length( find( e-> e < 0, netadv[ truekeep_inds ] ) );
    fp = length( find( e-> e >= 0, netadv[ truekeep_inds ] ) );

    # Display
    if display
        println( "Confusion matrix of " * modelname );
        cm = DataFrame( Predicted = ["True", "False"], True = [tp, fn], False = [fp, tn] );
        println(cm);
        println(" ");
    end

    # Compute the metrics
    sensitivity = tp / (tp + fn);
    precision = tp/ (tp + fp);
    specificity = tn / (tn + fp);

    accuracy = (tp + tn) / (tp + tn + fp + fn);
    prevalence = ( tp + fn ) / (tp + tn + fp + fn);

    f1_score = 2 * precision * sensitivity / ( precision + sensitivity );

    # Return
    metric = DataFrame( :sensitivity => sensitivity,
                        :specificity => specificity,
                        :precision => precision,
                        :accuracy => accuracy,
                        :prevalence => prevalence,
                        :f1_score => f1_score)
end

"""
Use maximal likelihood to fit Toledo gap acceptance model
"""
function toledo_model_fit(  traindata::DataFrame;
                            tolerance::Float64 = 0.001,
                            max_iter::Int64 = 10000,
                            init_para::Vector{Float64} = [ 1.706, -6.323, -0.155, 0.099, 0.939, 1.429, 0.512, 0.211, 0.775 ],
                            stepsize::Vector{Float64} = [1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3, 1e-3 ],
                            project_horizon::Float64 = 3.0,
                            display::Bool = false)

    # This is the implementation of the gap acceptance (GA) model in
    #   [1] Toledo, Tomer, and Romina Katz. "State dependence in lane-changing models."
    #       Transportation research record 2124.1 (2009): 81-88.
    # We will fit the parameters in this GA model.

    # Retrieve data
    v0 = Array{Float64,1}( traindata[:subj_v] );
    v2 = Array{Float64, 1}( traindata[:adj_lead_v] );
    Δd2 = Array{Float64, 1}( traindata[:adj_lead_rd] );
    v3 = Array{Float64, 1}( traindata[:adj_lag_v] );
    Δd3 = Array{Float64, 1}( traindata[:adj_lag_rd] );
    # Labels of the data
    y = Array{Int64, 1}( traindata[:decision] );     # data label 1:lane change, 0:lane keep

    # Use a short notation
    β = init_para;
    α = stepsize;

    # Compute the projected lead gap (ld_g) and lag gap (lg_g)
    # Relative speeds
    Δv20 = v2 - v0;
    Δv30 = v3 - v0;

    # Projected actual gaps ( preview based planning )
    proj_h = project_horizon;   # second

    ld_g = Δd2 + Δv20 * proj_h; # lead gap after proj_h
    ld_g = [ maximum([ gap, 1e-10]) for gap in ld_g ]; # lower bounded ld_g

    lg_g = Δd3 - Δv30 * proj_h; # lag gap after proj_h
    lg_g = [ maximum([ gap, 1e-10]) for gap in lg_g ]; # lower bounded lg_g

    # Prepare the explanatory variables in GA.
    x1 = [ maximum( [0, v] ) for v in Δv20 ]; # lower bounded by 0
    x2 = [ minimum( [0, v] ) for v in Δv20 ]; # upper bounded by 0
    x3 = [ maximum( [0, v] ) for v in Δv30 ]; # lower bounded by 0
    # Individual variable range, assume ν ∼ Normal(0, 1) [1].
    Δν = 1;
    ν = collect( -3:Δν:3 );

    # Gradient ascent
    # The expected log-likelihood function is
    #   Eν[l] =  ∑ₙ∑ᵢ( yᵢlog(pᵢ) + (1-yᵢ)log(1-pᵢ) ) * p(νₙ) * Δν
    # where yᵢ is the data label, pᵢ is the probability of yᵢ = 1. We let yᵢ = 1
    # being lane-change choice, hence,
    #   pᵢ = p( lead gap > critical lead gap || lag gap > critical lag gap )
    # at i-th data point. νₙ is the value of the individual specific variable
    # and Δν is the granularity of νₙ.
    #
    # We compute the gradient of Eν[l] with respect to parameters βⱼ
    #   ∂l/∂βⱼ = ∑ₙ∑ᵢ ( ( yᵢ-pᵢ ) / ( pᵢ( 1-pᵢ ) ) ) * ( ∂pᵢ / ∂βⱼ ) * p(νₙ) * Δν

    # Initialize a vector to store log-likelihood
    l_vec = Vector{Float64}();
    loglik = 0;
    push!(l_vec, -max_iter);
    push!(l_vec, 0);
    # Initialize the counter
    count = 0;

    while count <= max_iter && abs( l_vec[end] - l_vec[end-1] ) > tolerance


        # Compute the partial derivatives ∂l / ∂β_j, initialize
        ∂l_∂β = zeros( length( β ) );
        # Also compute the log-likelihood, initialize
        loglik = 0.0;

        for νₙ in ν

            # Compute the difference between log-gap and the mean critical log gap
            Δlog_ld = log.( ld_g ) - ( β[1] + β[2] .* x1 + β[3] .* x2 + β[4] * νₙ );
            Δlog_lg = log.( lg_g ) - ( β[6] + β[7] .* x3 + β[8] * νₙ );

            # Compute P( lead gap > critical lead gap )
            Φ_ld = cdf.( Normal(0, 1.0),  Δlog_ld ./ β[5] );
            # Compute the corresponding probability density
            φ_ld = pdf.( Normal(0, 1.0),  Δlog_ld ./ β[5] );

            # Compute P( lag gap > critical lag gap )
            Φ_lg = cdf.( Normal(0, 1.0),  Δlog_lg ./ β[9] );
            # Compute the corresponding probability density
            φ_lg = pdf.( Normal(0, 1.0),  Δlog_lg ./ β[9] );

            # Compute the probability of choosing lane change
            p = Φ_ld .* Φ_lg;
            # To avoid computation rounding error, we bound the probability in [0.01, 0.99]
            p = [  minimum( [ maximum( [p_i, 0.01] ), 0.99 ] ) for p_i in p ];
            # Compute loglik_inner = ∑ᵢ( yᵢlog(pᵢ) + (1-yᵢ)log(1-pᵢ) ) * p(νₙ) * Δν
            loglik_inner = sum( ( y .* log.( p ) + ( 1 - y ) .* log.( 1-p ) ) .* pdf.( Normal(0, 1.0), νₙ ) .* Δν );

            # Compute Eν[l] =  ∑ₙ loglik_inner
            loglik += loglik_inner;

            # Compute factor f_i_νₙ = ( yᵢ-pᵢ ) / ( pᵢ( 1-pᵢ ) )
            f_νₙ = ( y - p ) ./ ( p .* ( 1 - p ) );

            # Compute the partial derivatives ∂p/∂β_j where p = Φ_ld * Φ_lg
            ∂p_∂β1 =  Φ_lg .* φ_ld .* ( -1 / β[5] );
            ∂p_∂β2 =  Φ_lg .* φ_ld .* ( -x1 ./ β[5] );
            ∂p_∂β3 =  Φ_lg .* φ_ld .* ( -x2 ./ β[5] );
            ∂p_∂β4 =  Φ_lg .* φ_ld .* ( -νₙ ./ β[5] );
            ∂p_∂β5 =  Φ_lg .* φ_ld .* ( -Δlog_ld ./ ( β[5] ^ 2.0 ) );

            ∂p_∂β6 =  Φ_ld .* φ_lg .* ( -1 ./ β[9] );
            ∂p_∂β7 =  Φ_ld .* φ_lg .* ( -x3 ./ β[9] );
            ∂p_∂β8 =  Φ_ld .* φ_lg .* ( -νₙ ./ β[9] );
            ∂p_∂β9 =  Φ_ld .* φ_lg .* ( -Δlog_lg ./ ( β[9] ^ 2.0 ) );

            # Pack these derivative vectors into an array of vectors.
            ∂p_∂β = [ ∂p_∂β1, ∂p_∂β2, ∂p_∂β3, ∂p_∂β4, ∂p_∂β5, ∂p_∂β6, ∂p_∂β7, ∂p_∂β8, ∂p_∂β9 ];

            # Compute inner_νₙ_j = ∑ᵢ( ( yᵢ-pᵢ ) / ( pᵢ( 1-pᵢ ) ) ) * ( ∂pᵢ / ∂βⱼ ) * p(νₙ) * Δν
            # for each j.
            inner_νₙ = zeros( length( β ) );

            for j = 1: length( ∂p_∂β )
                inner_νₙ[j] =  sum( f_νₙ .* ∂p_∂β[j] .* pdf.( Normal(0, 1.0), νₙ ) .* Δν );
            end

            # Compute ∂l/∂βⱼ = ∑ₙ inner_νₙ
            ∂l_∂β += inner_νₙ;

        end

        # Store the log-likelihood of this iteration
        push!(l_vec, loglik);
        # Show l
        if display
            println("Count: ", count, ", log_likilihood: ", loglik);
        end

        # Now we are ready to compute α ⋅ ∇l(β)
        # Initialize a vector to store ∇l(β)
        stepping = Vector{Float64}( length( β ) );
        for j = 1:length( β )
            onestep = α[j] * ∂l_∂β[j];
            stepping[j] = minimum( [ maximum([onestep, -0.05 ]), 0.05 ] );
        end
        # Here we apply gradient ascent.
        β += stepping;
        # spin once
        count += 1;

    end

    # Show the total iteration
    println("        total iteration: ", count);

    # Return
    β

end

"""
Test Toledo gap acceptance model
"""
function toledo_model_test( testdata::DataFrame, para::Vector{Float64} )

    # Get the performance on standard metrics
    performance = toledo_confusion_metrics( testdata, para );
    # Get the roc curve based on Toledo gap acceptance model
    toledo_roc = toledo_roc_curve( testdata, para );
    # Get the area under the roc curve
    toledo_rocauc = get_roc_auc( toledo_roc );

    # Add the ROC_AUC to the performance dataframe.
    performance[:roc_auc] = [ toledo_rocauc ];

    # Return
    (performance, toledo_roc)

end

function toledo_roc_curve( testdata::DataFrame, para::Vector{Float64};
                           decision_ϵ_range::Vector{Float64} = [0.00, 1.0],
                           n_points::Int64 = 50 )

    # Define a grid of human characteristic variable in the hm_chara_range
    ϵ_range = linspace( decision_ϵ_range[1], decision_ϵ_range[2], n_points );

    # Create the roc curve
    # Initialize the points on the ROC curve.
    points = Matrix{Float64}(2, 0);

    for ϵ in ϵ_range

        ( tp, fn, tn, fp ) = toledo_confusionmatrix( testdata, para, decision_ϵ = ϵ );
        # Get true positive rate and false positive rate.
        tpr = tp / (tp + fn);
        fpr = fp / (tn + fp);

        # Add this point
        points = hcat(points, [fpr, tpr] );

    end

    # Sort the points hence fpr is ascending.
    sortedpoints = sortcols( points, lt=lexless )

    # Rectify the roc curve to have standard x-coordinates
    rec_fpr = collect( linspace( 0.0, 1.0, 100 ) );
    rec_tpr = linear_interpolation( sortedpoints[1,:], sortedpoints[2,:], rec_fpr );

    # Return
    rec_points = vcat( rec_fpr', rec_tpr' )

end

function toledo_confusionmatrix( testdata::DataFrame, para::Vector{Float64};
                                 decision_ϵ::Float64 = 0.5, project_horizon::Float64 = 3.0 )

    # This is the implementation of the gap acceptance model in
    #   [1] Toledo, Tomer, and Romina Katz. "State dependence in lane-changing models."
    #       Transportation research record 2124.1 (2009): 81-88.

    # Retrieve data
    v0 = Array{Float64,1}( testdata[:subj_v] );
    v2 = Array{Float64, 1}( testdata[:adj_lead_v] );
    Δd2 = Array{Float64, 1}( testdata[:adj_lead_rd] );
    v3 = Array{Float64, 1}( testdata[:adj_lag_v] );
    Δd3 = Array{Float64, 1}( testdata[:adj_lag_rd] );
    # Data label
    y = Array{Float64, 1}( testdata[:decision] );     # data label 1:lane change, 0:lane keep
    # Parameters
    β = para;
    νₙ = rand( Normal() );   # Individual specific term
    ϵ = decision_ϵ;

    # Compute the projected lead gap (ld_g) and lag gap (lg_g)
    # Relative speeds
    Δv20 = v2 - v0;
    Δv30 = v3 - v0;

    # Projected actual gaps ( preview based planning )
    proj_h = project_horizon;   # second

    ld_g = Δd2 + Δv20 * proj_h; # lead gap after proj_h
    ld_g = [ maximum([ gap, 1e-323 ]) for gap in ld_g ]; # lower bounded ld_g

    lg_g = Δd3 - Δv30 * proj_h; # lag gap after proj_h
    lg_g = [ maximum([ gap, 1e-323 ]) for gap in lg_g ]; # lower bounded lg_g

    # Prepare the explanatory variables in GA.
    x1 = [ maximum( [0, v] ) for v in Δv20 ]; # lower bounded by 0
    x2 = [ minimum( [0, v] ) for v in Δv20 ]; # upper bounded by 0
    x3 = [ maximum( [0, v] ) for v in Δv30 ]; # lower bounded by 0

    # ==========================================================================
    # The gap acceptance model
    # ==========================================================================

    # Compute the difference between log-gap and the mean critical log gap
    Δlog_ld = log.( ld_g ) - ( β[1] + β[2] .* x1 + β[3] .* x2 + β[4] * νₙ );
    Δlog_lg = log.( lg_g ) - ( β[6] + β[7] .* x3 + β[8] * νₙ );

    # Compute P( lead gap > critical lead gap )
    Φ_ld = cdf.( Normal(0, 1.0),  Δlog_ld ./ β[5] );
    # Compute P( lag gap > critical lag gap )
    Φ_lg = cdf.( Normal(0, 1.0),  Δlog_lg ./ β[9] );

    # Compute probability of choosing lane-change
    p = Φ_ld .* Φ_lg;
    # To avoid computation rounding error, we bound the probability in [0.01, 0.99]
    p = [  minimum( [ maximum( [p_i, 0.01] ), 0.99 ] ) for p_i in p ];

    # ==========================================================================
    # Test model performance
    # ==========================================================================
    # Lane change == ld_g > ld_cg and lg_g > lg_cg, otherwise, lane keep
    # true positive
    truechange_inds = find(d -> d == 1, y);
    tp = length( find( i -> p[i] >= ϵ, truechange_inds ) );
    # false negative
    fn = length( find( i -> p[i] < ϵ, truechange_inds ) );

    truekeep_inds = find(d -> d == 0, y);
    # true negative
    tn = length( find( i -> p[i] < ϵ, truekeep_inds ) );
    # false positive
    fp = length( find( i -> p[i] >= ϵ, truekeep_inds ) );

    # Return
    (tp, fn, tn, fp)

end


" Compute the performance for Toledo gap acceptance model"
function toledo_confusion_metrics( testdata::DataFrame, para::Vector{Float64}; decision_ϵ::Float64 = 0.5 )

    ( tp, fn, tn, fp ) = toledo_confusionmatrix( testdata, para, decision_ϵ = decision_ϵ );

    # Compute the metrics
    sensitivity = tp / (tp + fn);
    precision = tp/ (tp + fp);
    specificity = tn / (tn + fp);

    accuracy = (tp + tn) / (tp + tn + fp + fn);
    prevalence = ( tp + fn ) / (tp + tn + fp + fn);

    f1_score = 2 * precision * sensitivity / ( precision + sensitivity );

    # Return
    metric = DataFrame( :sensitivity => sensitivity,
                        :specificity => specificity,
                        :precision => precision,
                        :accuracy => accuracy,
                        :prevalence => prevalence,
                        :f1_score => f1_score)

end


"""
Test Time-to-collision model
"""
function ttc_model_test( testdata::DataFrame )

    # Get the performance on standard metrics
    performance = ttc_confusion_metrics( testdata );
    # Get the roc curve based on Toledo gap acceptance model
    ttc_roc = ttc_roc_curve( testdata );
    # Get the area under the roc curve
    ttc_rocauc = get_roc_auc( ttc_roc );

    # Add the ROC_AUC to the performance dataframe.
    performance[:roc_auc] = [ ttc_rocauc ];

    # Return
    (performance, ttc_roc)

end

function ttc_roc_curve( testdata::DataFrame; log_decision_ϵ_range::Vector{Float64} = [-10.0, 20.0],
                           n_points::Int64 = 50 )

    # Define a grid of human characteristic variable in the hm_chara_range
    log_ϵ_range = linspace( log_decision_ϵ_range[1], log_decision_ϵ_range[2], n_points );
    ϵ_range = 10.0 .^ log_ϵ_range;

    # Create the roc curve
    # Initialize the points on the ROC curve.
    points = Matrix{Float64}(2, 0);

    for ϵ in ϵ_range

        ( tp, fn, tn, fp ) = ttc_confusionmatrix( testdata, decision_ϵ = ϵ );
        # Get true positive rate and false positive rate.
        tpr = tp / (tp + fn);
        fpr = fp / (tn + fp);

        # Add this point
        points = hcat(points, [fpr, tpr] );

    end

    # Sort the points hence fpr is ascending.
    sortedpoints = sortcols( points, lt=lexless )

    # Rectify the roc curve to have standard x-coordinates
    rec_fpr = collect( linspace( 0.0, 1.0, 100 ) );
    rec_tpr = linear_interpolation( sortedpoints[1,:], sortedpoints[2,:], rec_fpr );

    # Return
    rec_points = vcat( rec_fpr', rec_tpr' )

end

"Compute the confusioin matrix for Time-to-collision model (TTC)"
function ttc_confusionmatrix( testdata::DataFrame; decision_ϵ::Float64 = 3.0, project_horizon::Float64 = 3.0)

    # Retrieve data
    v0 = Array{Float64,1}( testdata[:subj_v] );
    v2 = Array{Float64, 1}( testdata[:adj_lead_v] );
    Δd2 = Array{Float64, 1}( testdata[:adj_lead_rd] );
    v3 = Array{Float64, 1}( testdata[:adj_lag_v] );
    Δd3 = Array{Float64, 1}( testdata[:adj_lag_rd] );
    # Data label
    y = Array{Float64, 1}( testdata[:decision] );     # data label 1:lane change, 0:lane keep
    # Parameters
    ϵ = decision_ϵ;

    # Compute the projected lead gap (ld_g) and lag gap (lg_g)
    # Relative speeds
    Δv20 = v2 - v0;
    Δv30 = v3 - v0;
    # Projected actual gaps ( preview based planning )
    proj_h = project_horizon;   # second
    #
    ld_g = Δd2 + Δv20 * proj_h; # lead gap after proj_h
    ld_g = [ maximum([ gap, 1e-100 ]) for gap in ld_g ]; # lower bounded ld_g > 0
    #
    lg_g = Δd3 - Δv30 * proj_h; # lag gap after proj_h
    lg_g = [ maximum([ gap, 1e-100 ]) for gap in lg_g ]; # lower bounded lg_g > 0

    # Prepare the relative speed for TTC.
    Δv20_bnd = [ minimum( [v, -1e-10] ) for v in Δv20 ]; # upper bounded Δv20_bnd < 0
    Δv30_bnd = [ maximum( [v, 1e-10] ) for v in Δv30 ]; # lower bounded Δv30_bnd > 0

    # Compute the TTC for every instance
    ld_ttc = - ld_g ./ Δv20_bnd; # TTC to the lead vehicle
    lg_ttc = lg_g ./ Δv30_bnd; # TTC to the lag vehicle
    # The final TTC is the smaller one.
    ttc = [ minimum( [ ld_ttc[i], lg_ttc[i] ] ) for i in 1:length( ld_ttc ) ];

    # ==========================================================================
    # Test model performance
    # ==========================================================================
    # Lane change == ttc > ϵ, otherwise, lane keep
    # true positive
    truechange_inds = find(d -> d == 1, y);
    tp = length( find( i -> ttc[i] > ϵ, truechange_inds ) );
    # false negative
    fn = length( find( i -> ttc[i] <= ϵ, truechange_inds ) );

    truekeep_inds = find(d -> d == 0, y);
    # true negative
    tn = length( find( i -> ttc[i] <= ϵ, truekeep_inds ) );
    # false positive
    fp = length( find( i -> ttc[i] > ϵ, truekeep_inds ) );

    # Return
    (tp, fn, tn, fp)

end


" Compute the performance for Time-to-collision model"
function ttc_confusion_metrics( testdata::DataFrame, decision_ϵ::Float64 = 3.0 )

    ( tp, fn, tn, fp ) = ttc_confusionmatrix( testdata, decision_ϵ = decision_ϵ );

    # Compute the metrics
    sensitivity = tp / (tp + fn);
    precision = tp/ (tp + fp);
    specificity = tn / (tn + fp);

    accuracy = (tp + tn) / (tp + tn + fp + fn);
    prevalence = ( tp + fn ) / (tp + tn + fp + fn);

    f1_score = 2 * precision * sensitivity / ( precision + sensitivity );

    # Return
    metric = DataFrame( :sensitivity => sensitivity,
                        :specificity => specificity,
                        :precision => precision,
                        :accuracy => accuracy,
                        :prevalence => prevalence,
                        :f1_score => f1_score)

end


# If this file is run as the main file, execute the function main defined above.
if abspath(PROGRAM_FILE) == @__FILE__
    k_fold_cross_validation()
end
